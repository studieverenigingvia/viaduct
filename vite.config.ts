/// <reference types="vitest" />
import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import VueI18nPlugin from '@intlify/unplugin-vue-i18n/vite'
import checker from "vite-plugin-checker";

export default defineConfig({
    publicDir: false,
    base: "/static/",
    build: {
        manifest: true,
        emptyOutDir: false,
        sourcemap: true,
        outDir: "app/static",
        rollupOptions: {
            // overwrite default .html entry
            input: ["./frontend/index.ts", "./frontend/assets/scss/app.scss"],
        },
    },
    css: {
        preprocessorOptions: {
            less: {
                javascriptEnabled: true,
            },
            sass: {
                indentedSyntax: true,
            },
        },
    },
    resolve: {
        alias: {
            vue: "vue/dist/vue.esm-bundler.js",
            "@": "/frontend",
            "ant-design-vue/types/form/form": "node_modules/ant-design-vue/types/form/form",
            "ant-design-vue/types/transfer": "node_modules/ant-design-vue/types/transfer",
            "ant-design-vue/types/table/column": "node_modules/ant-design-vue/types/table/column",
            "ant-design-vue/types/select/option": "node_modules/ant-design-vue/types/select/option",
        },
    },
    plugins: [
        VueI18nPlugin({ strictMessage: false }),
        vue({
            template: {
                compilerOptions: {
                    isCustomElement: (tag) => tag === "pretix-widget",
                }
            }
        }),
        checker({ vueTsc: true }),
    ],
    server: {
        origin: process.env.VITE_SERVER || "http://localhost:5173",
    },
    test: {
        setupFiles: ["frontend/mocks/vitest.setup.ts"],
        globals: true,
        environment: "jsdom",
        coverage: {
            provider: "v8",
            reporter: ["json", "html", "json-summary", "text"],
            reportsDirectory: "coverage",
        }
    }
});
