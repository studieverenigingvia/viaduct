import logging

from sqlalchemy import literal_column
from sqlalchemy.exc import SQLAlchemyError

from app import db

_logger = logging.getLogger(__name__)


def check_database_status():
    try:
        return db.session.query(literal_column("True")).one()[0]
    except SQLAlchemyError as e:
        _logger.error("Database health check failed: %s", e)
        return False
