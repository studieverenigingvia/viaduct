import json

import requests

from app.models.setting_model import Setting
from app.repository.setting_repository import find_by_key
from app.service.setting_service import get_setting_by_key, save_setting


def request_and_save_tokens(params: dict):
    secret = get_setting_by_key("PIPEDRIVE_SECRET").value
    client_id = get_setting_by_key("PIPEDRIVE_CLIENT_ID").value

    headers = {"content-type": "application/x-www-form-urlencoded"}

    res = requests.post(
        "https://oauth.pipedrive.com/oauth/token",
        data=params,
        headers=headers,
        auth=(client_id, secret),
    ).json()

    rt = res.get("refresh_token", None)
    at = res.get("access_token", None)
    exp = res.get("expires_in", None)

    if at and rt and exp:
        if not (s := find_by_key("PIPEDRIVE_TOKEN")):
            s = Setting()
            s.key = "PIPEDRIVE_TOKEN"

        s.value = json.dumps({"access_token": at, "refresh_token": rt, "expiry": exp})
        save_setting(s)

        return True
    return False


def tokens_exists() -> bool:
    return bool(find_by_key("PIPEDRIVE_TOKEN"))


def req_authorization_url() -> str:
    base_url = "https://oauth.pipedrive.com/oauth/authorize"
    client_id = get_setting_by_key("PIPEDRIVE_CLIENT_ID").value
    redirect_uri = get_setting_by_key("PIPEDRIVE_CALLBACK_URL").value
    return f"{base_url}?client_id={client_id}&redirect_uri={redirect_uri}"


def handle_auth_callback(code: str):
    auth_url = get_setting_by_key("PIPEDRIVE_CALLBACK_URL").value
    params = {
        "code": code,
        "grant_type": "authorization_code",
        "redirect_uri": auth_url,
    }

    return request_and_save_tokens(params)


def refresh_access_token():
    if not (tokens := find_by_key("PIPEDRIVE_TOKEN")):
        return False

    rt = json.loads(tokens.value).get("refresh_token")
    params = {
        "refresh_token": rt,
        "grant_type": "refresh_token",
    }

    return request_and_save_tokens(params)


def _make_api_call(api_url):
    def fetch():
        if not (tokens := json.loads(find_by_key("PIPEDRIVE_TOKEN").value)):
            raise Exception(
                "Invalid Pipedrive tokens; please reauthorise using the marketplace."
            )

        if not (base_url := find_by_key("PIPEDRIVE_HOST")):
            raise Exception("Setting PIPEDRIVE_HOST needs to be set.")

        url = f"{base_url.value}{api_url}"
        headers = {"Authorization": f"Bearer {tokens['access_token']}"}

        return requests.get(url, headers=headers).json()

    if (req := fetch()) and req.get("success", False):
        return req

    # Try again with new access tokens.
    refresh_access_token()
    if (req := fetch()) and req.get("success", None):
        return req

    raise Exception(req.get("error", req))


def get_all_deals_raw():
    return _make_api_call("/api/v1/deals?stage_id=4&status=open")


def fetch_org_info(org_id):
    return _make_api_call(f"/api/v1/organizations/{org_id}")


def fetch_single_deal(deal_id: int = 0):
    return _make_api_call(f"/api/v1/deals/{deal_id}")


def fetch_products_for_deal(deal_id: int = 0):
    return _make_api_call(f"/api/v1/deals/{deal_id}/products?include_product_data=1")
