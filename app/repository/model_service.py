from typing import TypeVar

from app import db
from app.exceptions.base import ResourceNotFoundException
from app.models.base_model import BaseEntity

Model = TypeVar("Model", bound=BaseEntity, covariant=True)


def find_by_id(model: type[Model], model_id: int) -> Model | None:
    obj: Model = db.session.get(model, model_id)
    if not obj or getattr(obj, "deleted", None):
        return None
    return obj


def get_by_id(model: type[Model], model_id: int) -> Model:
    obj: Model = db.session.get(model, model_id)
    if not obj or getattr(obj, "deleted", None):
        raise ResourceNotFoundException(model.__name__.lower(), model_id)
    return obj


def get_all(model: type[Model], order_by=None) -> list[Model]:
    """
    Generic function to get all data from a model. Also has the option to order the
    data by a model's attribute.

    :param model: The model you would like to query the data of. E.g. Redirect
    :param order_by: The model's attribute you would like to order the output with.
    E.g. Redirect.fro

    :return: a list of the model's data
    """
    obj_list: list[Model]

    if not order_by:
        obj_list = db.session.query(model).all()
    else:
        obj_list = db.session.query(model).order_by(order_by).all()

    return obj_list


def save(obj) -> type[Model]:
    db.session.add(obj)
    db.session.commit()

    return obj


def delete(obj) -> None:
    db.session.delete(obj)
    db.session.commit()
