import datetime

from app import db
from app.models.group import Group, UserGroup
from app.models.meeting import Meeting
from app.models.user import User


def add_meeting(meeting: Meeting) -> None:
    db.session.add(meeting)
    db.session.commit()


def get_meeting_by_id(meeting_id: int) -> Meeting:
    return db.session.query(Meeting).filter_by(id=meeting_id).one_or_none()


def get_meetings_by_group_ids(group_ids: list) -> list[Meeting]:
    return db.session.query(Meeting).filter(Meeting.group_id.in_(group_ids)).all()


def get_for_user(user: User):
    return (
        db.session.query(Meeting)
        .join(Group)
        .join(UserGroup)
        .filter(UserGroup.user_id == user.id)
        .filter(Meeting.group_id == UserGroup.group_id)
    )


def get_upcoming_for_user(user: User):
    current_datetime = datetime.datetime.now(datetime.timezone.utc)

    return (
        get_for_user(user)
        .filter(Meeting.end_time > current_datetime)
        .order_by(Meeting.start_time.asc())
        .all()
    )


def get_past_for_user(user: User):
    current_datetime = datetime.datetime.now(datetime.timezone.utc)

    return (
        get_for_user(user)
        .filter(Meeting.end_time < current_datetime)
        .order_by(Meeting.start_time.desc())
        .all()
    )


def get_all_for_user(user: User):
    return get_for_user(user).order_by(Meeting.start_time).all()


def update_meeting() -> None:
    db.session.commit()


def remove_meeting_by_id(meeting_id: int) -> None:
    db.session.query(Meeting).filter_by(id=meeting_id).delete(synchronize_session=False)
    db.session.commit()
