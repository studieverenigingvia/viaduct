from sqlalchemy import Column, Float, ForeignKey, Integer, String, Text
from sqlalchemy.orm import relationship

from app.extensions import mapper_registry
from app.models.base_model import BaseEntity
from app.models.user import User


@mapper_registry.mapped
class Declaration(BaseEntity):
    __tablename__ = "declaration"
    committee = Column(String(256), nullable=False)
    amount = Column(Float, nullable=False)
    reason = Column(Text, nullable=False)
    user_id = Column(Integer, ForeignKey("user.id"), nullable=False)
    user = relationship(User, uselist=False)
