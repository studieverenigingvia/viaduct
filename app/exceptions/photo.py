"""
This module contains a set of basic application errors.

Any custom extensions of the exceptions should be a subclass of any of these
classes.
"""
from app.exceptions.base import ApplicationException


class FlickrAPIException(ApplicationException):
    title = "Flickr API error"
    message = "Flickr API endpoint {endpoint} returned error code {error_code}"

    def __init__(self, endpoint, error_code):
        super().__init__(endpoint=endpoint, error_code=error_code)
