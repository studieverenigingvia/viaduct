from enum import Enum, unique

from flask_babel import lazy_gettext as _


@unique
class Scopes(Enum):
    alv = _("Access to ALVs")
    pimpy = _("Access to pimpy")
    user = _("Access to users")
    group = _("Access to groups")
    examination = _("Access to examinations")
    photos = _("Access to photos")
    company = _("Access to companies")
    form = _("Access to forms")
    mailinglist = _("Access to mailing lists")
    page = _("Access to pages")
    meeting = _("Access to meeting")
    pretix = _("Access to pretix")
    navigation = _("Access to navigation")
    activity = _("Access to activities")
    challenge = _("Access to challenges")
    newsletter = _("Access to newsletters")
    tutoring = _("Access to tutoring")
    declaration = _("Access to declaration")
    redirects = _("Change the redirects")
    domjudge = _("Change DOMjudge settings")

    # deleted: used as feature flag during development
    search = _("Access to search")

    @classmethod
    def choices(cls):
        return [(choice, f"{choice.name}: {choice.value}") for choice in cls]

    @classmethod
    def coerce(cls, item):
        return cls[item] if not isinstance(item, cls) else item

    def __str__(self):
        return str(self.name)
