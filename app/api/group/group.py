from http import HTTPStatus

from flask import Response, abort
from flask.views import MethodView
from flask_login import current_user
from marshmallow import ValidationError, fields, validates_schema
from marshmallow.validate import OneOf

from app.api.schema import (
    PageSearchParameters,
    PaginatedResponseSchema,
    RestSchema,
)
from app.decorators import (
    json_schema,
    query_parameter_schema,
    require_oauth,
    require_role,
)
from app.models.group import ILLEGAL_MAILLIST_PREFIXES, Group
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import group_service, role_service


class GroupSchema(RestSchema):
    # List view
    id = fields.Integer(dump_only=True)
    created = fields.Date(dump_only=True)
    modified = fields.Date(dump_only=True)

    name = fields.String(required=True)
    mailtype = fields.String(
        required=True, validate=[OneOf(["none", "mailinglist", "mailbox"])]
    )
    maillist = fields.String()

    @classmethod
    def get_list_schema(cls):
        return cls(many=True, only=("id", "name", "maillist", "mailtype"))

    @validates_schema
    def validate_maillist(self, data, **_):
        errors = {}
        if data.get("mailtype") == "none":
            return

        if not data.get("mailtype"):
            errors["maillist"] = "Missing data for required field."
        elif any(data.get("mailtype").startswith(p) for p in ILLEGAL_MAILLIST_PREFIXES):
            errors["maillist"] = (
                "E-mail address cannot start with any of "
                "the following: " + ", ".join(ILLEGAL_MAILLIST_PREFIXES)
            )
        if errors:
            raise ValidationError(errors)


class GroupResource(MethodView):
    schema = GroupSchema()

    @require_oauth(Scopes.group)
    def get(self, group: Group):
        in_group = group_service.user_member_of_group(current_user, group.id)
        has_role = role_service.user_has_role(current_user, Roles.GROUP_READ)

        if in_group or has_role:
            return self.schema.dump(group)
        return abort(403)

    @require_oauth(Scopes.group)
    @require_role(Roles.GROUP_WRITE)
    @json_schema(schema)
    def put(self, data: dict[str, str], group: Group):
        group_service.edit_group(
            group, data["name"], data["mailtype"], data["maillist"]
        )

        return Response(status=HTTPStatus.NO_CONTENT)

    @require_oauth(Scopes.group)
    @require_role(Roles.GROUP_WRITE)
    def delete(self, group: Group):
        group_service.delete_group(group)
        return Response(status=HTTPStatus.NO_CONTENT)


class GroupListResource(MethodView):
    schema_get = PaginatedResponseSchema(GroupSchema.get_list_schema())
    schema_post = GroupSchema()

    @require_oauth(Scopes.group)
    @require_role(Roles.GROUP_READ)
    @query_parameter_schema(PageSearchParameters)
    def get(self, pagination: PageSearchParameters):
        pagination = group_service.paginated_search_all_groups(pagination)

        return self.schema_get.dump(pagination)

    @require_oauth(Scopes.group)
    @require_role(Roles.GROUP_WRITE)
    @json_schema(schema_post)
    def post(self, data: dict[str, str]):
        group = group_service.create_group(
            data["name"], data["mailtype"], data.get("maillist")
        )

        return self.schema_post.dump(group), HTTPStatus.CREATED
