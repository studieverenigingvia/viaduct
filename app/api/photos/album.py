from flask.views import MethodView
from marshmallow import fields

from app.api.schema import RestSchema
from app.decorators import require_favourer_or_membership, require_oauth
from app.oauth_scopes import Scopes
from app.service import photo_service


class PhotoSchema(RestSchema):
    id = fields.String()
    title = fields.String()
    url = fields.URL()
    highres_url = fields.URL()
    origin_url = fields.URL()
    shorturl = fields.URL()


class AlbumInfoSchema(RestSchema):
    id = fields.String()
    photos = fields.Integer()
    date_created = fields.DateTime()
    title = fields.String()
    description = fields.String()
    primary_photo = fields.URL()


class AlbumSchema(RestSchema):
    id = fields.String()
    photos = fields.Nested(PhotoSchema, many=True)
    date_created = fields.DateTime()
    title = fields.String()
    description = fields.String()
    primary_photo = fields.URL()
    photo_count = fields.Integer()


class AlbumResource(MethodView):
    schema_get = AlbumSchema()

    @require_oauth(Scopes.photos)
    @require_favourer_or_membership
    def get(self, album_id):
        album = photo_service.get_album(album_id)
        return self.schema_get.dump(album)


class AlbumListResource(MethodView):
    schema_get = AlbumInfoSchema(many=True)

    @require_oauth(Scopes.photos)
    @require_favourer_or_membership
    def get(self):
        index = photo_service.get_album_index()
        return self.schema_get.dump(index)


class RandomPhotoResource(MethodView):
    schema_get = PhotoSchema()

    @require_oauth(Scopes.photos)
    @require_favourer_or_membership
    def get(self):
        photo = photo_service.get_random_photo()
        return self.schema_get.dump(photo)
