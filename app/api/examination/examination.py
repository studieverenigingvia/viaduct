from collections.abc import Callable
from http import HTTPStatus

from flask import Response, request
from flask.views import MethodView
from marshmallow import fields
from werkzeug.datastructures import FileStorage

from app.api.examination.course import CourseSchema
from app.api.schema import RestSchema
from app.decorators import json_schema, require_oauth, require_role
from app.models.course import Course
from app.models.examination import ExamFileType, Examination
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import examination_service


class ExaminationSchema(RestSchema):
    id = fields.Int(required=True, dump_only=True)
    comment = fields.Str(load_default="")
    date = fields.Date(required=True)

    examination_file_id = fields.Int(dump_only=True, required=True, allow_none=False)
    answers_file_id = fields.Int(dump_only=True, allow_none=True)
    summary_file_id = fields.Int(dump_only=True, allow_none=True)

    course = fields.Nested(CourseSchema, dump_only=True)
    course_id = fields.Int(load_only=True, required=True)

    test_type = fields.Str(load_default="Unknown")


class CourseExaminationListResource(MethodView):
    schema = ExaminationSchema(many=True)

    def get(self, course: Course):
        examinations = examination_service.find_all_examinations_by_course_id(course.id)
        return self.schema.dump(examinations)


class ExaminationResource(MethodView):
    schema = ExaminationSchema()

    @require_oauth(Scopes.examination)
    @require_role(Roles.EXAMINATION_WRITE)
    def get(self, examination):
        return self.schema.dump(examination)

    @require_oauth(Scopes.examination)
    @require_role(Roles.EXAMINATION_WRITE)
    @json_schema(schema)
    def put(self, data, examination):
        examination_service.update_examination(
            exam=examination,
            exam_date=data["date"],
            comment=data["comment"],
            test_type=data["test_type"],
            course_id=data["course_id"],
        )
        return self.schema.dump(examination)

    @require_oauth(Scopes.examination)
    @require_role(Roles.EXAMINATION_WRITE)
    def delete(self, examination):
        examination_service.delete_examination(examination)
        return Response(status=HTTPStatus.NO_CONTENT)


class ExaminationListResource(MethodView):
    schema = ExaminationSchema()

    @require_oauth(Scopes.examination)
    @require_role(Roles.EXAMINATION_WRITE)
    @json_schema(schema)
    def post(self, data):
        exam = examination_service.add_examination(
            exam_date=data["date"],
            comment=data["comment"],
            test_type=data["test_type"],
            course_id=data["course_id"],
        )
        return self.schema.dump(exam), 201


def create_examination_file_resource(
    save_func: Callable[[Examination, FileStorage, ExamFileType], None],
    file_type: ExamFileType,
):
    class ExaminationBaseUploadResource(MethodView):
        @require_oauth(Scopes.examination)
        @require_role(Roles.EXAMINATION_WRITE)
        def post(self, examination):
            try:
                exam_file = request.files["file"]
                save_func(examination, exam_file, file_type)
            except KeyError:
                return Response(status=HTTPStatus.BAD_REQUEST)

            return Response(status=HTTPStatus.OK)

    return ExaminationBaseUploadResource


ExaminationExamUploadResource = create_examination_file_resource(
    examination_service.save_exam_file,
    ExamFileType.EXAMINATION,
)

ExaminationAnswerUploadResource = create_examination_file_resource(
    examination_service.save_exam_file,
    ExamFileType.ANSWERS,
)

ExaminationSummaryUploadResource = create_examination_file_resource(
    examination_service.save_exam_file,
    ExamFileType.SUMMARY,
)
