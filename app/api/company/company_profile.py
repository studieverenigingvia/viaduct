import random
from datetime import date

from flask import abort
from flask.views import MethodView
from marshmallow import fields

from app.api.company.company import CompanyModuleSchemaMixin
from app.api.schema import RenderedMarkdownStringField, RestSchema
from app.decorators import json_schema, require_oauth, require_role
from app.models.company import Company
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import company_service


class CompanyProfileSchema(RestSchema, CompanyModuleSchemaMixin):
    description_nl = RenderedMarkdownStringField()
    description_en = RenderedMarkdownStringField()

    description_nl_raw = fields.String(dump_only=True, attribute="description_nl")
    description_en_raw = fields.String(dump_only=True, attribute="description_en")


class ExtendendCompanyProfileSchema(CompanyProfileSchema):
    name = fields.String()


class CompanyProfileListSchema(RestSchema):
    id = fields.Integer(dump_only=True)
    company_id = fields.Integer(dump_only=True)
    company_name = fields.String(dump_only=True)
    company_slug = fields.String(dump_only=True)


class CompanyProfileListResource(MethodView):
    """Public API, so no scope or role required."""

    schema = CompanyProfileListSchema(many=True)

    def get(self):
        profiles = company_service.find_all_profiles()
        profiles = [
            {
                "id": profile.id,
                "company_id": profile.company_id,
                "company_name": profile.company.name,
                "company_slug": profile.company.slug,
            }
            for profile in profiles
        ]

        # Shuffle in backend, randomized per day
        r = random.Random(str(date.today()))
        r.shuffle(profiles)

        return self.schema.dump(profiles)


class CompanyProfileSlugResource(MethodView):
    """Public API, so no scope or role required."""

    schema = ExtendendCompanyProfileSchema()

    def get(self, company):
        profile = company_service.get_company_profile_by_company_id(company.id)

        if not profile.active:
            abort(410)

        # To also display the name on the company profile via VueJS
        setattr(profile, "name", company.name)  # noqa: B010

        return self.schema.dump(profile)


class CompanyProfileIdResource(MethodView):
    schema = CompanyProfileSchema()

    @require_oauth(Scopes.company)
    @require_role(Roles.COMPANY_WRITE)
    def get(self, company: Company):
        profile = company_service.get_company_profile_by_company_id(company.id)
        return self.schema.dump(profile)

    @require_oauth(Scopes.company)
    @require_role(Roles.COMPANY_WRITE)
    @json_schema(schema)
    def post(self, company_profile: dict, company: Company):
        profile = company_service.create_company_profile(
            company_id=company.id, **company_profile
        )
        return self.schema.dump(profile)

    @require_oauth(Scopes.company)
    @require_role(Roles.COMPANY_WRITE)
    @json_schema(schema)
    def put(self, company_profile: dict, company: Company):
        profile = company_service.edit_company_profile(
            company_id=company.id, **company_profile
        )
        return self.schema.dump(profile)
