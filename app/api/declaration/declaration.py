from http import HTTPStatus

from flask import Response, request
from flask.views import MethodView
from marshmallow import fields

from app.api.schema import RestSchema
from app.decorators import form_schema, require_oauth
from app.oauth_scopes import Scopes
from app.service import declaration_service


class DeclarationSchema(RestSchema):
    reason = fields.String()
    committee = fields.String()
    amount = fields.Float()
    iban = fields.String()


class DeclarationResource(MethodView):
    schema = DeclarationSchema()

    @require_oauth(Scopes.declaration)
    @form_schema(schema)
    def post(self, data):
        if request.files is None or len(request.files) == 0:
            return Response(status=HTTPStatus.BAD_REQUEST, response="No files provided")

        declaration_service.send_declaration(request.files, **data)
        return Response(status=HTTPStatus.NO_CONTENT)
