from collections.abc import Callable, Generator
from typing import Any, TypedDict, TypeVar

import pydantic
import requests
from flask.views import MethodView
from marshmallow import (
    RAISE,
    Schema,
    SchemaOpts,
    ValidationError,
    fields,
    pre_dump,
)
from marshmallow import utils as marshmallow_utils
from marshmallow.fields import Field, String
from marshmallow.utils import missing as missing_
from pydantic import PydanticValueError

from app import app
from app.exceptions.base import ResourceNotFoundException
from app.models.company_job import CONTRACT_OF_SERVICE_TYPES
from app.service.markdown_service import (
    render_markdown_safely,
    render_markdown_summarized_safely,
)
from app.utils.markdown_summary import SummaryOptions


class RestSchemaOpts(SchemaOpts):
    """
    Overwrite the defaults for the meta class.

    Raise ValidationError if there are any unknown fields in deserialization.
    Always maintain field ordering of serialized output.
    """

    def __init__(self, meta, ordered):
        super().__init__(meta, ordered=ordered)
        self.unknown = getattr(meta, "unknown", RAISE)
        self.ordered = getattr(meta, "ordered", True)


class RestSchema(Schema):
    OPTIONS_CLASS = RestSchemaOpts


class PaginatedResponseSchema(RestSchema):
    page = fields.Integer()
    page_size = fields.Integer()
    page_count = fields.Integer()
    total = fields.Integer()

    data = fields.Method("get_data", metadata={"many": True})

    def __init__(self, subschema: Schema, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.subschema = subschema

    @pre_dump
    def unpack_pagination(self, pagination_obj, **kwargs):
        return {
            "page": pagination_obj.page,
            "page_size": pagination_obj.per_page,
            "page_count": pagination_obj.pages,
            "total": pagination_obj.total,
            "data": pagination_obj.items,
        }

    def get_data(self, obj):
        return self.subschema.dump(obj["data"])


class PageNumberInt(pydantic.ConstrainedInt):
    gt = 0


class PageLimitInt(pydantic.ConstrainedInt):
    gt = 0
    le = 50


class PageSearchParameters(pydantic.BaseModel):
    search = ""
    page = PageNumberInt(1)
    limit = PageLimitInt(10)
    sort = "id"

    @pydantic.validator("sort", pre=True)
    def validate_sort(cls, sort: str):
        if not sort:
            return ""
        check_from = 0
        if sort.startswith("-"):
            check_from = 1
        if not sort[check_from:].isalpha():
            raise ValidationError("Sorting keys must be alphabetic")
        return sort


class CourseIdInt(pydantic.ConstrainedInt):
    gt = 0


class PageCourseSearchParameters(PageSearchParameters):
    dn_course_ids: list[CourseIdInt] = []
    user_educations: list[tuple[int, int]] = []

    @pydantic.validator("dn_course_ids", pre=True)
    def preprocess_dn_course_ids(cls, v):
        if not v:
            return []

        if isinstance(v, str):
            return v.split(",")
        return v

    @pydantic.validator("user_educations", pre=True)
    def preprocess_user_educations(cls, user_educations):
        if not user_educations:
            return []

        if isinstance(user_educations, str):
            user_educations = list(map(int, user_educations.split(",")))
            # educations are always send in "<edu_id>,<year>,<edu_id>,<year>,..."
            user_educations = [
                (user_educations[i], user_educations[i + 1])
                for i in range(0, len(user_educations), 2)
            ]
        return user_educations


class PageJobSearchParameters(PageSearchParameters):
    contract_of_service: list[str] = CONTRACT_OF_SERVICE_TYPES

    @pydantic.validator("contract_of_service", pre=True)
    def preprocess_contract_of_service(cls, contract_of_service):
        if not contract_of_service:
            return CONTRACT_OF_SERVICE_TYPES
        if isinstance(contract_of_service, str):
            contract_of_service = [
                t
                for t in contract_of_service.split(",")
                if t in CONTRACT_OF_SERVICE_TYPES
            ]
        return contract_of_service


class MultilangStringField(Field):
    default_error_messages = {
        "invalid": "Not a valid dict of languages.",
        "invalid_utf8": "Not a valid utf-8 string.",
        "missing_data": "Not all languages supplied",
    }

    langs = ["nl", "en"]

    def _serialize(self, value, attr, obj, **kwargs):
        return self._verify_dict(value)

    def _deserialize(self, value, attr, data, **kwargs):
        return self._verify_dict(value)

    def _validate(self, value):
        for lang in self.langs:
            super()._validate(value[lang])

    def _verify_dict(self, value):
        if not isinstance(value, dict):
            raise self.make_error("invalid")

        r = {}
        for lang in self.langs:
            if lang not in value or not isinstance(value[lang], str):
                r[lang] = ""
            else:
                try:
                    r[lang] = marshmallow_utils.ensure_text_type(value[lang])
                except UnicodeDecodeError as e:
                    raise self.make_error("invalid_utf8") from e
        return r


class AutoMultilangStringField(MultilangStringField):
    def __init__(self, *args, attribute_format="{lang}_{attr}", **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.attribute_format = attribute_format

    def get_value(self, obj, attr, accessor=None, default=missing_):
        return {
            lang: super(AutoMultilangStringField, self).get_value(
                obj,
                self.attribute_format.format(attr=attr, lang=lang),
                accessor,
                default,
            )
            for lang in self.langs
        }


class MultilangStringDict(TypedDict):
    en: str
    nl: str


class EnumField(Field):
    """Based on https://github.com/justanr/marshmallow_enum."""

    default_error_messages = {
        "by_name": "Invalid enum member {input}",
        "must_be_string": "Enum name must be string",
    }

    def __init__(self, enum, by_value=False, error="", *args, **kwargs):
        self.enum = enum
        self.by_value = by_value
        self.error = error

        super().__init__(*args, **kwargs)

    def _serialize(self, value, attr, obj):
        if value is None:
            return None
        return value.name

    def _deserialize(self, value, attr, data, **kwargs):
        if value is None:
            return None
        return self._deserialize_by_name(value, attr, data)

    def _deserialize_by_name(self, value, attr, data):
        if not isinstance(value, str):
            self.fail("must_be_string", input=value, name=value)

        try:
            return getattr(self.enum, value)
        except AttributeError:
            self.fail("by_name", input=value, name=value)

    def fail(self, key, **kwargs):
        kwargs["values"] = ", ".join([str(mem.value) for mem in self.enum])
        kwargs["names"] = ", ".join([mem.name for mem in self.enum])

        if self.error:
            if self.by_value:
                kwargs["choices"] = kwargs["values"]
            else:
                kwargs["choices"] = kwargs["names"]
            msg = self.error.format(**kwargs)
            raise ValidationError(msg)
        else:
            super().fail(key, **kwargs)


class TextAreaStr(pydantic.ConstrainedStr):
    @classmethod
    def __modify_schema__(cls, field_schema):
        super().__modify_schema__(field_schema)
        pydantic.types.update_not_none(field_schema, format="textarea")


class UserIdInt(pydantic.ConstrainedInt):
    gt = 0

    @classmethod
    def __modify_schema__(cls, field_schema):
        super().__modify_schema__(field_schema)
        pydantic.types.update_not_none(field_schema, format="user")


class StudentIdError(PydanticValueError):
    msg_template = "invalid student id format"


class StudentIdStr(pydantic.ConstrainedStr):
    @classmethod
    def validate_studentid(cls, value: str) -> str:
        mod11_sum = 0

        for i, digit in enumerate(reversed(str(value))):
            mod11_sum += (i + 1) * int(digit)

        if mod11_sum % 11 != 0:
            raise StudentIdError
        return value

    @classmethod
    def __get_validators__(cls) -> Generator[Callable[..., Any], None, None]:
        yield cls.validate
        yield cls.validate_studentid

    @classmethod
    def __modify_schema__(cls, field_schema):
        super().__modify_schema__(field_schema)
        pydantic.types.update_not_none(field_schema, format="studentid")


class RecaptchaError(pydantic.PydanticValueError):
    msg_template = "Recaptcha verification failed: {reason}"


class RecaptchaStr(pydantic.ConstrainedStr):
    # https://developers.google.com/recaptcha/docs/verify
    verify_url = "https://www.google.com/recaptcha/api/siteverify"
    errors = {
        "missing-input-secret": "The secret parameter is missing.",
        "invalid-input-secret": "The secret parameter is invalid or malformed.",
        "missing-input-response": "The response parameter is missing.",
        "invalid-input-response": "The response parameter is invalid or malformed.",
        "bad-request": "The request is invalid or malformed.",
        "timeout-or-duplicate": "The response is no longer valid: either is too old "
        "or has been used previously.",
        # These are not documented, however I saw them returned.
        "invalid-keys": "The secret parameter is invalid or malformed.",
        # This is a fallback for unknown situations.
        "unknown": "The failure is unknown.",
    }

    @classmethod
    def validate_recaptcha(cls, value: str) -> str:
        private_key = app.config["RECAPTCHA_PRIVATE_KEY"]

        r = requests.post(
            cls.verify_url, data={"response": value, "secret": private_key}
        )
        if not r.ok or not r.json().get("success", False):
            for error_code in r.json().get("error-codes", ["unknown"]):
                raise RecaptchaError(reason=cls.errors[error_code])

        return value

    @classmethod
    def __get_validators__(cls) -> Generator[Callable[..., Any], None, None]:
        yield cls.validate
        yield cls.validate_recaptcha

    @classmethod
    def __modify_schema__(cls, field_schema):
        super().__modify_schema__(field_schema)
        pydantic.types.update_not_none(field_schema, format="recaptcha")


S = TypeVar("S", bound=pydantic.BaseModel)


class SchemaRegistery:
    def __init__(self):
        self._registry: dict[str, type[S]] = {}

    def get(self, item):
        return self._registry.get(item)

    def register(self, cls: type[S]) -> type[S]:
        schema_name = cls.__name__
        if schema_name in self._registry:
            existing_schema = self._registry[schema_name]
            raise ValueError(
                f"Schema {schema_name} already registered: "
                f"{existing_schema.__qualname__}"
            )
        self._registry[schema_name] = cls
        return cls


schema_registry = SchemaRegistery()


class SchemaResource(MethodView):
    def get(self, schema_name: str):
        schema = schema_registry.get(schema_name)
        if not schema:
            raise ResourceNotFoundException("schema", schema_name)
        return schema.schema_json(), 200


class RenderedMarkdownStringField(String):
    def __init__(self, summary_options: SummaryOptions = None, *args, **kwargs):
        self.summary_options = summary_options

        super(String, self).__init__(*args, **kwargs)

    def _serialize(self, value, attr, obj, **kwargs):
        if value is None:
            return None

        text = marshmallow_utils.ensure_text_type(value)

        if self.summary_options is None:
            return render_markdown_safely(text)

        return render_markdown_summarized_safely(text, self.summary_options)
