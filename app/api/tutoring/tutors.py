from http import HTTPStatus

from flask import Response, jsonify
from flask.views import MethodView
from flask_login import current_user
from marshmallow import fields

from app.api.examination.course import CourseSchema
from app.api.schema import (
    PageSearchParameters,
    PaginatedResponseSchema,
    RestSchema,
)
from app.api.user.tutoring import TutoringSchema
from app.api.user.user import UserSchema
from app.decorators import (
    json_schema,
    query_parameter_schema,
    require_oauth,
    require_role,
)
from app.exceptions.base import ResourceNotFoundException
from app.models.tutoring import Tutoring
from app.models.user import User
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import role_service, tutoring_service


class TutorSchema(RestSchema):
    id = fields.Integer(dump_only=True)
    grade = fields.Float()
    course = fields.Nested(CourseSchema, dump_only=True)
    course_id = fields.Integer(load_only=True)
    user = fields.Nested(
        UserSchema(only=("id", "first_name", "last_name")), dump_only=True
    )
    approved = fields.Boolean()


class TutorCourseRegisterSchema(RestSchema):
    grade = fields.Float(required=True)
    course_id = fields.Integer(required=True)


class TutorRegisterSchema(RestSchema):
    courses = fields.Nested(TutorCourseRegisterSchema, many=True)


class TutorsResource(MethodView):
    schema = PaginatedResponseSchema(TutorSchema(many=True))

    @require_oauth(Scopes.tutoring)
    @require_role(Roles.EXAMINATION_WRITE)
    @query_parameter_schema(PageSearchParameters)
    def get(self, pagination: PageSearchParameters):
        return self.schema.dump(tutoring_service.find_tutors(pagination))


class TutorCoursesResource(MethodView):
    schema = TutorSchema()
    schema_post = TutorRegisterSchema()

    @require_oauth(Scopes.tutoring)
    def get(self, user):
        return self.schema.dump(
            tutoring_service.find_courses_for_tutor(user), many=True
        )

    @require_oauth(Scopes.tutoring)
    @json_schema(schema_post)
    def post(self, tutor_data, user):
        return (
            jsonify(
                self.schema.dump(
                    tutoring_service.register_tutor_courses(
                        user=user,
                        courses=tutor_data.get("courses"),
                    ),
                    many=True,
                )
            ),
            HTTPStatus.CREATED,
        )


class TutorCourseResource(MethodView):
    schema = TutorSchema(partial=True)

    @require_oauth(Scopes.tutoring)
    @json_schema(schema)
    def patch(self, tutor_data, user, tutor):
        if "approved" in tutor_data:
            role_service.check_user_has_role(current_user, Roles.EXAMINATION_WRITE)
            if tutor_data.get("approved"):
                tutoring_service.approve_tutor(tutor)

        # TODO Update other data (currently has no interface option)
        return self.schema.dump(tutor)

    @require_oauth(Scopes.tutoring)
    def delete(self, user, tutor):
        if tutor.user != user:
            role_service.check_user_has_role(current_user, Roles.EXAMINATION_WRITE)

        tutoring_service.delete_tutor(tutor)
        return Response(status=HTTPStatus.NO_CONTENT)


class TutoringResource(MethodView):
    schema = TutoringSchema(many=True)

    @require_oauth(Scopes.tutoring)
    def get(self):
        if role_service.user_has_role(current_user, Roles.EXAMINATION_WRITE):
            return self.schema.dump(tutoring_service.get_tutorings())

        return self.schema.dump(
            tutoring_service.get_tutorings(is_open=True, tutor=current_user)
        )


class TutorTutoringListResource(MethodView):
    schema = TutoringSchema(many=True)

    @require_oauth(Scopes.tutoring)
    def get(self, user):
        return self.schema.dump(tutoring_service.get_tutorings_for_tutor(user))


class TutorTutoringResource(MethodView):
    @require_oauth(Scopes.tutoring)
    def delete(self, user: User, tutoring: Tutoring):
        # TODO add support for admins to access all users.
        if tutoring.tutor is not user:
            raise ResourceNotFoundException("tutoring", tutoring.id)

        tutoring_service.withdraw_tutoring(tutoring)
        return Response(status=HTTPStatus.NO_CONTENT)


class TutorTutoringAcceptResource(MethodView):
    @require_oauth(Scopes.tutoring)
    def post(self, user, tutoring):
        if current_user != user:
            role_service.check_user_has_role(current_user, Roles.EXAMINATION_WRITE)
        tutoring_service.accept_tutoring(tutoring, user)
        return Response(status=HTTPStatus.NO_CONTENT)
