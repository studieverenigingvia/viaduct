from http import HTTPStatus
from zipfile import BadZipFile

from flask import Response, request
from flask.views import MethodView
from marshmallow import fields, validate

from app.api.activity import ActivitySchema
from app.api.schema import AutoMultilangStringField, EnumField, RestSchema
from app.api.user.user import UserSchema
from app.decorators import form_schema, json_schema, require_oauth, require_role
from app.exceptions.base import ValidationException
from app.oauth_scopes import Scopes
from app.roles import Roles
from app.service import alv_parse_service, alv_service
from app.service.alv_parse_service import AlvEvent, AlvParseError


def _alv_dict_from_data(data):
    alv_dict = {
        "nl_name": data["name"]["nl"],
        "en_name": data["name"]["en"],
        "date": data["date"],
    }

    if "chairman_user_id" in data:
        alv_dict["chairman_user_id"] = data["chairman_user_id"]

    if "secretary_user_id" in data:
        alv_dict["secretary_user_id"] = data["secretary_user_id"]

    if "activity_id" in data:
        alv_dict["activity_id"] = data["activity_id"]

    if "minutes_accepted" in data:
        alv_dict["minutes_accepted"] = data["minutes_accepted"]

    return alv_dict


class AlvMetaDataSchema(RestSchema):
    title = fields.String()
    chairman = fields.String()
    secretary = fields.String()
    start_time = fields.String()
    stop_time = fields.String()
    location = fields.String()

    present = fields.List(fields.String())
    proxies = fields.List(fields.Tuple((fields.String(), fields.String())))


class AlvDocumentSchema(RestSchema):
    def _newest_created(self, obj):
        """Finds the document version with the highest created property."""
        newest = None
        for doc_version in obj.versions:
            if newest is None or doc_version.created > newest:
                newest = doc_version.created

        if newest:
            return newest.strftime("%Y-%m-%dT%H:%M:%S%z")

    id = fields.Integer(dump_only=True)
    name = AutoMultilangStringField(required=True, validate=validate.Length(max=128))
    versions = fields.Function(lambda obj: len(obj.versions), dump_only=True)
    newest_created = fields.Method("_newest_created", dump_only=True)


class AlvDocumentUploadSchema(RestSchema):
    nl_name = fields.String(required=True, validate=validate.Length(max=128))
    en_name = fields.String(required=True, validate=validate.Length(max=128))


class AlvSchema(RestSchema):
    id = fields.Integer(dump_only=True)
    name = AutoMultilangStringField(required=True, validate=validate.Length(max=128))
    activity_id = fields.Integer(load_only=True)
    activity = fields.Nested(
        ActivitySchema(only=("id", "name", "start_time", "end_time")), dump_only=True
    )

    chairman_user_id = fields.Integer(load_only=True)
    chairman = fields.Nested(
        UserSchema(only=("id", "first_name", "last_name")), dump_only=True
    )
    secretary_user_id = fields.Integer(load_only=True)
    secretary = fields.Nested(
        UserSchema(only=("id", "first_name", "last_name")), dump_only=True
    )

    minutes_file_id = fields.Integer(dump_only=True)

    date = fields.Date(required=True)

    minutes_accepted = fields.Boolean(allow_none=True)
    meta = fields.Nested(
        AlvMetaDataSchema(exclude=("present", "proxies")), dump_only=True
    )

    documents = fields.List(fields.Nested(AlvDocumentSchema()), dump_only=True)


class AlvEventDataSchema(RestSchema):
    event_type = EnumField(AlvEvent)
    data = fields.Dict()


class AlvParseMinuteSchema(RestSchema):
    meta_data = fields.Nested(AlvMetaDataSchema())
    events = fields.List(fields.Nested(AlvEventDataSchema()))
    errors = fields.List(fields.String())


class AlvMinuteResource(MethodView):
    @require_oauth(Scopes.alv)
    @require_role(Roles.ALV_WRITE)
    def post(self, alv):
        alv_service.add_minutes(alv, request.files["file"])

        return {}, HTTPStatus.NO_CONTENT


class AlvDocumentListResource(MethodView):
    req_schema = AlvDocumentUploadSchema()
    resp_schema = AlvDocumentSchema()

    @require_oauth(Scopes.alv)
    @require_role(Roles.ALV_WRITE)
    @form_schema(req_schema)
    def post(self, data, alv):
        if "file" not in request.files:
            raise ValidationException("No file given.")

        document = alv_service.add_document(
            alv, request.files["file"], data["nl_name"], data["en_name"]
        )

        return self.resp_schema.dump(document), HTTPStatus.OK


class AlvDocumentResource(MethodView):
    schema = AlvDocumentSchema()

    @require_oauth(Scopes.alv)
    @require_role(Roles.ALV_WRITE)
    def post(self, doc_id):
        alv_document = alv_service.get_alv_document_by_id(doc_id)

        alv_service.update_document(
            alv_document,
            request.files["file"],
            alv_document.nl_name,
            alv_document.en_name,
        )

        return {}, HTTPStatus.NO_CONTENT

    @require_oauth(Scopes.alv)
    @require_role(Roles.ALV_WRITE)
    @json_schema(schema)
    def put(self, data, doc_id):
        alv_document = alv_service.get_alv_document_by_id(doc_id)

        alv_service.update_document(
            alv_document,
            None,
            data["name"]["nl"],
            data["name"]["en"],
        )

        return {}, HTTPStatus.NO_CONTENT


class AlvResource(MethodView):
    schema = AlvSchema()

    @require_oauth(Scopes.alv)
    def get(self, alv):
        return self.schema.dump(alv)

    @require_oauth(Scopes.alv)
    @require_role(Roles.ALV_WRITE)
    @json_schema(schema)
    def patch(self, data, alv):
        alv = alv_service.update(alv, **_alv_dict_from_data(data))
        return self.schema.dump(alv)


class AlvListResource(MethodView):
    schema = AlvSchema(exclude=["documents"])

    @require_oauth(Scopes.alv)
    def get(self):
        alvs = alv_service.find_all_alv()
        return self.schema.dump(alvs, many=True)

    @require_oauth(Scopes.alv)
    @require_role(Roles.ALV_WRITE)
    @json_schema(schema)
    def post(self, data):
        alv = alv_service.add(**_alv_dict_from_data(data))
        return self.schema.dump(alv), 201


class AlvParseMinuteResource(MethodView):
    schema = AlvParseMinuteSchema()

    @require_oauth(Scopes.alv)
    @require_role(Roles.ALV_WRITE)
    def post(self):
        try:
            meta_data, events, parse_errors = alv_parse_service.parse_minutes(
                request.files["file"]
            )
        except (AlvParseError, BadZipFile) as e:
            raise ValidationException(str(e))

        return (
            self.schema.dump(
                {
                    "meta_data": meta_data,
                    "events": events,
                    "errors": parse_errors,
                }
            ),
            HTTPStatus.OK,
        )


class AlvParsedMinuteResource(MethodView):
    schema = AlvParseMinuteSchema()

    @require_oauth(Scopes.alv)
    @require_role(Roles.ALV_WRITE)
    @json_schema(schema)
    def post(self, data, alv):
        alv_service.replace_parsed_alv_data(
            alv, data["meta_data"], data["events"], data["errors"]
        )

        return Response(status=HTTPStatus.NO_CONTENT)

    @require_oauth(Scopes.alv)
    def get(self, alv):
        parsed = alv_service.get_parsed_alv_data(alv)

        if parsed is None:
            return {}, HTTPStatus.NOT_FOUND

        meta_data, events, parse_errors = parsed

        return self.schema.dump(
            {
                "meta_data": meta_data,
                "events": events,
                "errors": parse_errors,
            }
        )
