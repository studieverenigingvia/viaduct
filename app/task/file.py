import logging
import re

from flask import url_for
from markdown.inlinepatterns import IMAGE_LINK_RE, LINK_RE

from app.models.file import File

_logger = logging.getLogger(__name__)
_re_link = re.compile(r"^(.*?)%s(.*)$" % LINK_RE, re.DOTALL | re.UNICODE)
_re_img = re.compile(r"^(.*?)%s(.*)$" % IMAGE_LINK_RE, re.DOTALL | re.UNICODE)


def replace_links_with_file(
    content: str | None, hashfs_file: File, query: str
) -> str | None:
    if content is None:
        return None

    new_lines = []

    current_url = url_for("static", filename=query)
    new_url = url_for("file.content", file=hashfs_file, file_hash=hashfs_file.hash)
    for line in content.splitlines():
        if current_url in line:
            new_lines.append(line.replace(current_url, new_url))
        else:
            new_lines.append(line)
    return "\n".join(new_lines)
