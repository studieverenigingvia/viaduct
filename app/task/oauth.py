import logging

from app import app, db, worker
from app.repository import oauth_repository

_logger = logging.getLogger(__name__)


@worker.task()
def delete_expire_oauth_tokens():
    with app.app_context():
        query = oauth_repository.get_expired_tokens_query()
        _logger.info(f"Trying to remove {query.count()} expired tokens")
        query.delete()
        db.session.commit()
        _logger.info("Deleted the expired tokens")
