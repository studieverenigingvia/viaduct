import datetime
from re import match
from urllib.parse import urlparse

import pytz

from app import app, worker
from app.models.activity import Activity
from app.repository import model_service
from app.service import redirect_service


@worker.task
def remove_expired_redirects():
    """
    This function checks if there are redirects to an activity. If the
    activity finished more than two weeks ago, it will be deleted.
    """
    with app.app_context():
        utc = pytz.UTC

        redirects = redirect_service.get_all()
        for redirect in redirects:
            r = urlparse(redirect.to).path

            # Check if the redirect is to an activity and get its 'id'.
            if m := match(r".*/?activities/(?P<id>[0-9]+)(/?|/.*)$", r):
                activity = model_service.find_by_id(Activity, m.groupdict()["id"])
                # Check if the activity is finished at least two weeks ago.
                if not activity or activity.end_time < utc.localize(
                    datetime.datetime.now() - datetime.timedelta(days=14)
                ):
                    # Remove the redirect
                    model_service.delete(redirect)
