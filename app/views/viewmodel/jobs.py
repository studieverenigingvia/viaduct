from flask import url_for

from app.models.company_job import CompanyJob


class JobsViewModel:
    def __init__(self, job: CompanyJob) -> None:
        self.job = job

    @property
    def id(self):
        return self.job.id

    @property
    def title(self):
        title, _ = self.job.get_localized_title_description()
        return title

    @property
    def description(self):
        _, description = self.job.get_localized_title_description()
        return description

    @property
    def contract_of_service(self):
        return self.job.contract_of_service

    @property
    def company(self):
        return self.job.company

    @property
    def url(self):
        return url_for("vue.view", job_id=self.job.id)

    @property
    def logo(self):
        return url_for("company.view_logo", company=self.company)
