from datetime import datetime

from flask import render_template

from app.models.meeting import Meeting
from app.service import group_service


class MeetingViewModel:
    def __init__(self, meeting: Meeting):
        self.m = meeting
        self.group = group_service.get_by_id(self.m.group_id)

    def render(self):
        return render_template("meeting/view_single.htm", view_model=self)

    def start_time_is_today(self):
        return self.m.start_time.date() == datetime.today().date()
