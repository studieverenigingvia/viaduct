from functools import wraps

from flask import request
from flask_login import current_user

REQUEST_ATTRIBUTE = "enable_membership_pending_banner"


def disable_membership_confirmation_pending_banner(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        MembershipBannerViewModel.show_membership_pending_banner(False)
        return f(*args, **kwargs)

    return wrapper


class MembershipBannerViewModel:
    def __init__(self):
        pass

    @staticmethod
    def show_membership_pending_banner(show: bool):
        setattr(request, REQUEST_ATTRIBUTE, show)

    @property
    def membership_confirmation_pending(self):
        enabled = getattr(request, REQUEST_ATTRIBUTE, True)
        if not enabled:
            return False

        user = current_user
        if user.is_anonymous or not user.is_authenticated:
            return False

        if user.favourer:
            return False

        return not user.has_paid and not user.alumnus
