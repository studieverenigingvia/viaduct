import logging

from flask import Blueprint, abort, redirect, render_template, request, url_for
from flask_babel import gettext as _

from app.decorators import require_role
from app.repository import contracts_repository
from app.repository.contracts_repository import handle_auth_callback
from app.roles import Roles
from app.service import contracts_service, setting_service

blueprint = Blueprint("contracts", __name__, url_prefix="/contracts")

_logger = logging.getLogger(__name__)


@blueprint.route("/callback/")
@require_role(Roles.COMPANY_WRITE)
def auth_callback():
    code = request.args.get("code", None)
    if not code:
        abort(400)

    # If the auth is successful, redirect to the overview. Else, redirect to the
    # Pipedrive site for reauthentication.
    if handle_auth_callback(code):
        return redirect(url_for("contracts.contracts_overview"))
    else:
        base_url = "https://oauth.pipedrive.com/oauth/authorize"
        client_id = setting_service.get_setting_by_key("PIPEDRIVE_CLIENT_ID").value
        auth_url = setting_service.get_setting_by_key("PIPEDRIVE_CALLBACK_URL").value

        return redirect(f"{base_url}?client_id={client_id}&redirect_uri={auth_url}")


@blueprint.route("/")
def root():
    return redirect(url_for("contracts.contracts_overview"))


@blueprint.route("/overview/")
@require_role(Roles.COMPANY_WRITE)
def contracts_overview():
    if not contracts_repository.tokens_exists():
        return redirect(contracts_repository.req_authorization_url())

    deals = contracts_service.get_all_deals()
    return render_template(
        "contracts/view.htm", deals=deals, title=_("Potential contracts")
    )


@blueprint.route("/create/")
@require_role(Roles.COMPANY_WRITE)
def create_contract():
    if "id" in request.args.keys():
        deal_id = request.args["id"]
    elif "selectedIds" in request.args.keys():
        deal_id = request.args["selectedIds"]
    else:
        return redirect(url_for("contracts.contracts_overview"))

    deal = contracts_service.get_single_deal(deal_id)
    prd = contracts_service.get_products_for_deal(deal_id)
    s = contracts_service.find_settings()
    return render_template("contracts/create.htm", deal=deal, prd=prd, s=s)
