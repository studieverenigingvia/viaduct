#!/usr/bin/env python

from flask import Blueprint, flash, redirect, session, url_for
from flask_babel import _, refresh
from flask_login import current_user

from app import constants, db
from app.views import get_safe_redirect_url

blueprint = Blueprint("lang", __name__, url_prefix="/lang")


@blueprint.route("/set/<path:lang>/", methods=["GET"])
def set_user_lang(lang=None):
    if lang not in constants.LANGUAGES.keys():
        flash(_("Language unsupported on this site") + ": " + lang, "warning")
        return redirect(get_safe_redirect_url())
    if current_user.is_anonymous:
        flash(_("You need to be logged in to set a permanent language."))
        return redirect(get_safe_redirect_url())

    current_user.locale = lang
    if "lang" in session:
        del session["lang"]
    db.session.add(current_user)
    db.session.commit()
    refresh()
    return redirect(get_safe_redirect_url())


@blueprint.route("/<path:lang>/", methods=["GET"])
def set_lang(lang=None):
    if lang not in constants.LANGUAGES.keys():
        flash(_("Language unsupported on this site") + ": " + lang, "warning")
        return redirect(get_safe_redirect_url())

    session["lang"] = lang

    # Show option to make language setting permanent if the user is logged in,
    # and the new language is not the user's default langauge.
    if current_user.is_authenticated and current_user.locale != lang:
        msg = _(
            "{} is now set as language for this session. To make this "
            "setting permanent, <a href='{}'>click here</a>"
        )
        flash(
            msg.format(
                constants.LANGUAGES[lang], url_for("lang.set_user_lang", lang=lang)
            ),
            "safe",
        )
    return redirect(get_safe_redirect_url())
