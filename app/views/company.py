from flask import Blueprint, abort, render_template
from flask_babel import gettext

from app.models.company import Company
from app.service import company_service, file_service

blueprint = Blueprint("company", __name__, url_prefix="/companies")


@blueprint.route("/", methods=["GET"])
def overview():
    return render_template("vue_content.htm", title=gettext("Companies"))


@blueprint.route("/<company_path:company>/", methods=["GET"])
def view(company: Company):
    """View a company."""

    profile = company_service.get_company_profile_by_company_id(company.id)
    if not profile.active:
        abort(410)
    return render_template("vue_content.htm", title=company.name)


@blueprint.route("/<company:company>/logo/", methods=["GET"])
@blueprint.route("/<company_path:company>/logo/", methods=["GET"])
def view_logo(company: Company):
    if company.logo_file_id is None:
        return abort(404)

    logo_file = file_service.get_file_by_id(company.logo_file_id)
    return file_service.serve_file(logo_file)
