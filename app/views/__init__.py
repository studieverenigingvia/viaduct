from urllib.parse import urljoin, urlparse

import flask


def is_safe_url(target):
    ref_url = urlparse(flask.request.host_url)
    test_url = urlparse(urljoin(flask.request.host_url, target))
    return test_url.scheme in ("http", "https") and ref_url.netloc == test_url.netloc


def set_redirect_location_if_not_set():
    if "denied_from" not in flask.session:
        flask.session["denied_from"] = flask.request.referrer


def get_safe_redirect_url(default="home.home"):
    for target in (
        flask.request.values.get("next"),
        flask.session.get("denied_from"),
        flask.request.referrer,
    ):
        if not target:
            continue
        # Avoid redirect loops by disallowing a target if it is equal to the request
        if target in [flask.request.url, flask.request.path, flask.request.full_path]:
            continue
        if is_safe_url(target):
            if flask.session.get("denied_from") is not None:
                del flask.session["denied_from"]
            return target
    return flask.url_for(default)
