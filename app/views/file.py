"""Views for the file module."""
import logging

from flask import (
    Blueprint,
    abort,
    flash,
    redirect,
    render_template,
    request,
    url_for,
)
from flask_login import current_user

from app.decorators import require_role
from app.enums import FileCategory
from app.forms.file import FileForm
from app.models.file import File
from app.roles import Roles
from app.service import file_service, role_service

blueprint = Blueprint("file", __name__, url_prefix="/files")
_logger = logging.getLogger(__name__)


@blueprint.route("/", methods=["GET"])
@require_role(Roles.FILE_READ)
def list():
    """List all files that are not assigned to a page."""

    search = request.args.get("search", None)
    try:
        page_nr = int(request.args.get("page", 1))
    except (TypeError, ValueError):
        page_nr = 1
    per_page = 30

    if search:
        files = file_service.search_files_in_uploads(search)[:per_page]
        files_paginated = None
    else:
        files_paginated = file_service.get_all_uploads(page_nr, per_page)
        files = files_paginated.items

    form = FileForm()

    can_write = role_service.user_has_role(current_user, Roles.FILE_WRITE)

    return render_template(
        "files/list.htm",
        files=files,
        form=form,
        files_paginated=files_paginated,
        search=search,
        can_write=can_write,
    )


@blueprint.route("/", methods=["POST"])
@require_role(Roles.FILE_WRITE)
def upload():
    """Upload a file."""
    try:
        page_nr = int(request.args.get("page", 1))
    except (TypeError, ValueError):
        page_nr = 1
    form = FileForm()
    new_files = []

    if form.validate_on_submit():
        category = FileCategory.UPLOADS
        if form.members_only.data:
            category = FileCategory.UPLOADS_MEMBER
        for new_file in request.files.getlist("file"):
            # File upload request, but no added files
            if new_file.filename == "":
                flash("No files selected", "danger")
                return redirect(url_for(".list", page=page_nr))

            f = file_service.add_file_to_s3(category, None, new_file)
            new_files.append(f)

    files_paginated = file_service.get_all_uploads(page_nr, 30)
    files = files_paginated.items

    can_write = role_service.user_has_role(current_user, Roles.FILE_WRITE)

    return render_template(
        "files/list.htm",
        files=files,
        form=form,
        files_paginated=files_paginated,
        search=None,
        new_files=new_files,
        can_write=can_write,
    )


@blueprint.route("/content/<file:file>/<string:file_hash>/", methods=["GET"])
def content(file: File, file_hash):
    if file.category not in FileCategory.all_uploads():
        return abort(404)
    if (
        (file.hash == f"uploads/{file_hash}" and file.category == FileCategory.UPLOADS)
        or (
            file.hash == f"uploads_member/{file_hash}"
            and file.category == FileCategory.UPLOADS_MEMBER
        )
        or file.hash == file_hash
    ):
        if file.require_membership and not current_user.has_paid:
            return abort(403)

        return file_service.serve_file(file)

    return abort(404)


@blueprint.route("/content/<file:file>/uploads/<string:file_hash>/", methods=["GET"])
@blueprint.route(
    "/content/<file:file>/uploads_member/<string:file_hash>/", methods=["GET"]
)
def s3_content(file: File, file_hash):
    if file.require_membership and not current_user.has_paid:
        return abort(403)

    if file.category == FileCategory.UPLOADS and file.hash == f"uploads/{file_hash}":
        return file_service.serve_file(file)

    if (
        file.category == FileCategory.UPLOADS_MEMBER
        and file.hash == f"uploads_member/{file_hash}"
    ):
        return file_service.serve_file(file)

    return abort(404)
