from flask import Blueprint, render_template
from flask_babel import gettext as _

from app.decorators import require_membership, require_role
from app.models.challenge import Challenge
from app.roles import Roles

blueprint = Blueprint("challenge", __name__, url_prefix="/challenge")


@blueprint.route("/", methods=["GET"])
@blueprint.route("/dashboard/", methods=["GET"])
@require_membership
def view_list():
    return render_template("vue_content.htm", title=_("Challenges"))


@blueprint.route("/create/", methods=["GET"])
@blueprint.route("/<challenge:challenge>/edit/", methods=["GET"])
@require_role(Roles.CHALLENGE_WRITE)
def edit(challenge: Challenge = None):
    title = _("Create challenge")
    if challenge:
        title = _("Edit challenge")
    return render_template("vue_content.htm", title=title)


@blueprint.route("/<challenge:challenge>/add-manual-submission/", methods=["GET"])
@require_role(Roles.CHALLENGE_WRITE)
def add_manual_submission(challenge: Challenge):
    return render_template(
        "vue_content.htm", title=_("Add manual submission: %s") % challenge.name
    )
