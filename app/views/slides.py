import logging

import flask
from flask import Blueprint

from app.service.activity_service import get_activities_for_slide, get_time_info
from app.service.slide_service import get_bus_train_departures

blueprint = Blueprint("slides", __name__, url_prefix="/slides")

_logger = logging.getLogger(__name__)


@blueprint.route("/ov/")
def slide_ov():
    bus_departures, train_departures = get_bus_train_departures()

    return flask.render_template(
        "slides/ovslide.html",
        departures_left=train_departures,
        departures_right=bus_departures,
    )


@blueprint.route("/weather/")
def slide_weather():
    return flask.render_template(
        "slides/weather.html",
    )


@blueprint.route("/activities/")
def slide_activity():
    start_hour = 9
    end_hour = 24

    single_day_activities, multi_day_activities = get_activities_for_slide()
    duration_days, days_description, week, hour_line_position = get_time_info(
        single_day_activities, multi_day_activities, start_hour, end_hour
    )

    number_of_hours = end_hour - start_hour

    return flask.render_template(
        "slides/activities.html",
        single_day_activities=single_day_activities,
        multi_day_activities=multi_day_activities,
        duration_days=duration_days,
        days_description=days_description,
        week=week,
        number_of_hours=number_of_hours,
        hour_line_position=hour_line_position,
        start_hour=start_hour,
    )
