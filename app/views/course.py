from flask import Blueprint, render_template
from flask_babel import _

from app.decorators import require_role
from app.roles import Roles

blueprint = Blueprint("course", __name__, url_prefix="/courses")


@blueprint.route("/", methods=["GET"])
@require_role(Roles.EXAMINATION_WRITE)
def view_courses():
    return render_template(
        "vue_content.htm", title=_("Courses"), keep_alive_include="course-overview"
    )


@blueprint.route("/create/", methods=["GET"])
@blueprint.route("/<course:course>/edit/", methods=["GET"])
@require_role(Roles.EXAMINATION_WRITE)
def edit_course(course=None):
    title = _("Courses")
    if course:
        title += f" {course.name}"
    return render_template(
        "vue_content.htm", title=title, keep_alive_include="course-overview"
    )
