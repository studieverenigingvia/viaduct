from flask_babel import lazy_gettext as _
from flask_wtf import FlaskForm
from wtforms import RadioField, SubmitField


class HistoryPageForm(FlaskForm):
    previous = RadioField(_("Previous"), coerce=int)
    current = RadioField(_("Current"), coerce=int)
    compare = SubmitField(_("Compare"))
