import json
import os

from flask import url_for

from app import app, static_url

MAIN_SCSS = "frontend/assets/scss/app.scss"
MAIN_JS = "frontend/index.ts"

if app.debug:
    vite_server = os.environ.get("VITE_SERVER", "http://localhost:5173/static")
    app.logger.debug(f"Connecting to Vite server at '{vite_server}'")

    def css_files():
        return [f"{vite_server}/{MAIN_SCSS}"]

    def js_files():
        return [
            f"{vite_server}/{js}"
            for js in [
                "@vite/client",
                MAIN_JS,
            ]
        ]

else:
    app.logger.debug("Loading compiled static JS/CSS server")

    manifest = {}
    manifest_path = "app/static/.vite/manifest.json"
    try:
        with open(manifest_path) as content:
            manifest = json.load(content)
    except OSError as exception:
        app.logger.error(f"Error loading manifest file: {exception}")
        app.logger.error("Continuing without manifest file")
        manifest = {}

    def get_manifest_for_file(file_path: str):
        return manifest[file_path]

    def get_file(file_path: str) -> str:
        return get_manifest_for_file(file_path)["file"]

    if not manifest:
        app.logger.error("Manifest file is empty, no CSS files will be loaded")
        css = []
    else:
        # Vite compiles CSS files from SCSS, so we need to include the SCSS file
        # in the list of CSS files. We also need to load css files that were compiled
        # from inside index.ts (e.g. ant design css)
        css = get_manifest_for_file(MAIN_JS)["css"]
        css.append(get_file(MAIN_SCSS))

    def css_files():
        return [
            static_url(url_for("static", filename=css_file), False) for css_file in css
        ]

    def js_files():
        if not manifest:
            app.logger.error("Manifest file is empty, no JS files will be loaded")
            return []

        return [static_url(url_for("static", filename=get_file(MAIN_JS)), False)]
