import logging
from collections import defaultdict
from functools import update_wrapper, wraps
from typing import TYPE_CHECKING

import pydantic
from authlib.oauth2.rfc6749 import grants
from celery.schedules import crontab
from redis import Redis
from werkzeug.urls import url_parse

from app.decorators import require_oauth
from app.extensions import (
    babel,
    cors,
    login_manager,
    migrate,
    oauth_server,
)
from app.models.company import Company
from app.routing import PageConverter

if TYPE_CHECKING:
    from app import ViaFlask

_logger = logging.getLogger(__name__)


def initializer(name):
    """A wrapper to make sure dependencies are not inialized twice."""

    def decorator(f):
        def wrapper(flask_initializer, *args, **kwargs):
            if not flask_initializer.init_status_dict[name]:
                f(flask_initializer, *args, **kwargs)
                flask_initializer.init_status_dict[name] = True

        return wrapper

    return decorator


class FlaskInitializer:
    """Handle the initialization of the flask application."""

    def __init__(self, app: "ViaFlask"):
        self.app = app
        self.init_status_dict: dict[str, bool] = defaultdict(lambda: False)

    @initializer("views")
    def init_views(self):
        self.init_babel()
        from app import api

        _logger.debug("Registered api '%s'", api.__name__)

        from app.views import (
            activity,
            admin,
            alv,
            calendar,
            challenge,
            company,
            contracts,
            copernica,
            course,
            declaration,
            domjudge,
            education,
            error_handlers,
            examination,
            file,
            group,
            home,
            lang,
            login,
            meeting,
            membership,
            mollie,
            navigation,
            news,
            newsletter,
            oauth,
            page,
            pages,
            pimpy,
            redirect,
            saml,
            seo,
            signup,
            slides,
            telegram,
            tutoring,
            user,
            vue,
        )

        _logger.debug("Registered error handler '%s'", error_handlers.__name__)
        for module in (
            activity,
            admin,
            contracts,
            alv,
            calendar,
            challenge,
            company,
            copernica,
            course,
            domjudge,
            declaration,
            education,
            examination,
            file,
            group,
            home,
            lang,
            meeting,
            mollie,
            navigation,
            news,
            newsletter,
            oauth,
            page,
            pages,
            pimpy,
            redirect,
            saml,
            seo,
            slides,
            telegram,
            tutoring,
            user,
            vue,
            signup,
            login,
            membership,
        ):
            self.app.register_blueprint(module.blueprint)
        _logger.debug("Blueprints: %s", str(self.app.blueprints.keys()))

    @initializer("worker")
    def init_worker(self, broker_url: pydantic.RedisDsn):
        from app import worker

        self.app.config["BROKER_URL"] = broker_url

        worker.config_from_object(
            {
                "result_backend": broker_url,
                # Without extended_results name of task is inaccessible from id.
                "result_extended": True,
                "task_serializer": "pickle",
                "accept_content": ["pickle", "json"],
                "result_serializer": "pickle",
                "broker_url": broker_url,
                "broker_transport_options": {"max_retries": 3},
            }
        )

        worker.conf.beat_schedule = {
            "daily-check-selection-profiles": {
                "task": "app.task.copernica.synchronize_copernica_disabling_selection_profiles",  # noqa: E501
                "schedule": crontab(hour=14, minute=35),
            },
            "hourly-user-educations-update": {
                "task": "app.task.user.update_most_historic_user_educations",
                "schedule": crontab(minute=15),
            },
            "daily-validate-copernica-structure": {
                "task": "app.task.copernica.validate_database_structure",
                "schedule": crontab(hour=11, minute=1),
            },
            "daily-check-expired-redirects": {
                "task": "app.task.redirect.remove_expired_redirects",
                "schedule": crontab(hour=11, minute=15),
            },
            "daily-check-expired-oauth-tokens": {
                "task": "app.task.oauth.delete_expire_oauth_tokens",
                "schedule": crontab(hour=11, minute=30),
            },
        }
        redis_host: str = url_parse(broker_url).host or "localhost"
        self.app.redis = Redis(redis_host)

    @initializer("babel")
    def init_babel(self):
        babel.init_app(self.app)

    @initializer("cors")
    def init_cors(self):
        cors.init_app(self.app, resources={r"/api/*": {"origins": "*"}})

    @initializer("migrate")
    def init_migrate(self, db):
        migrate.init_app(self.app, db)

    @initializer("login")
    def init_login(self):
        login_manager.init_app(self.app)

        login_manager.login_view = "login.sign_in"

        from app.models.user import AnonymousUser

        login_manager.anonymous_user = AnonymousUser

    @initializer("oauth")
    def init_oauth(self) -> None:
        from app.service import oauth_service

        oauth_server.init_app(
            self.app,
            query_client=oauth_service.get_client_by_id,
            save_token=oauth_service.create_token,
        )

        oauth_server.register_grant(oauth_service.AuthorizationCodeGrant)
        oauth_server.register_grant(oauth_service.RefreshTokenGrant)
        oauth_server.register_grant(oauth_service.ClientCredentialsGrant)
        oauth_server.register_grant(grants.ImplicitGrant)
        oauth_server.register_endpoint(oauth_service.RevocationEndpoint)
        oauth_server.register_endpoint(oauth_service.IntrospectionEndpoint)

        require_oauth.register_token_validator(oauth_service.BearerTokenValidator())

    @initializer("converters")
    def init_converters(self):
        from app import routing
        from app.models.activity import Activity
        from app.models.alv_model import Alv
        from app.models.challenge import Challenge
        from app.models.committee import Committee
        from app.models.course import Course
        from app.models.education import Education
        from app.models.examination import Examination
        from app.models.file import File
        from app.models.meeting import Meeting
        from app.models.mollie import Transaction
        from app.models.news import News
        from app.models.newsletter import Newsletter
        from app.models.pimpy import Minute
        from app.models.redirect import Redirect
        from app.models.tutoring import Tutor, Tutoring
        from app.models.user import User

        self.app.url_map.converters["activity"] = routing.create(Activity)
        self.app.url_map.converters["activity_slug"] = routing.create_slugable(
            Activity, Activity.to_slug
        )
        self.app.url_map.converters["alv"] = routing.create(Alv)
        self.app.url_map.converters["challenge"] = routing.create(Challenge)
        self.app.url_map.converters["company"] = routing.create(Company)
        self.app.url_map.converters["company_path"] = routing.CompanyConverter
        self.app.url_map.converters["course"] = routing.create(Course)
        self.app.url_map.converters["education"] = routing.create(Education)
        self.app.url_map.converters["examination"] = routing.create(Examination)
        self.app.url_map.converters["file"] = routing.create(File)
        self.app.url_map.converters["group"] = routing.GroupConverter
        self.app.url_map.converters["meeting"] = routing.create(Meeting)
        self.app.url_map.converters[
            "navigation_entry"
        ] = routing.NavigationEntryConverter
        self.app.url_map.converters["news"] = routing.create(News)
        self.app.url_map.converters["news_slug"] = routing.create_slugable(
            News, News.to_slug
        )
        self.app.url_map.converters["newsletter"] = routing.create(Newsletter)
        self.app.url_map.converters["transaction"] = routing.create(Transaction)
        self.app.url_map.converters["tutoring"] = routing.create(Tutoring)
        self.app.url_map.converters["tutor"] = routing.create(Tutor)
        self.app.url_map.converters["redirect"] = routing.create(Redirect)
        self.app.url_map.converters["minute"] = routing.create(Minute)
        self.app.url_map.converters["page"] = PageConverter
        self.app.url_map.converters["user"] = routing.create(User)
        self.app.url_map.converters["user_self"] = routing.UserSelfConverter
        self.app.url_map.converters["committee"] = routing.create(Committee)


def require_worker(f):
    @wraps
    def wrapper(*args, **kwargs):
        from app import app_init

        app_init.init_worker()
        return f(*args, **kwargs)

    return wrapper


def require_login_manager(f):
    def wrapper(*args, **kwargs):
        from app import app_init

        app_init.init_login()
        return f(*args, **kwargs)

    return update_wrapper(wrapper, f)
