import logging
from typing import Any, ClassVar, cast

import pydantic
from sqlalchemy import Column, MetaData, String, Table, func, inspect, select
from sqlalchemy.engine.reflection import Inspector
from sqlalchemy.orm import Session

from app.exceptions.base import ResourceNotFoundException
from app.repository import setting_repository

_logger = logging.getLogger(__name__)


class DatabaseSettingsSource:
    def __init__(self, db_session: Session) -> None:
        # We mirror app/models/setting.py here, since we cannot use the
        # ORM yet. Using the ORM requires all models (sqlalchemy mappers) to be
        # loaded. We load them, since that would require services to load and
        # the services are querying settings on startup. These settings on startup
        # might be something we'd want to deprecate?
        self.db_session = db_session
        self.table = Table(
            "setting",
            MetaData(),
            Column("key", String(128), unique=True, nullable=False),
            Column("value", String(4096), nullable=False),
        )

    def __call__(self, settings: "DatabaseSettingsMixin") -> dict[str, Any]:
        if not cast(Inspector, inspect(self.db_session.bind)).has_table("setting"):
            return {}

        keys = {k.lower() for k in set(settings.__fields__.keys())}
        stmt = select(self.table).where(func.lower(self.table.c.key).in_(keys))

        rows = self.db_session.execute(stmt).all()

        db_settings: dict[str, str] = {s.key.lower(): s.value for s in rows}

        # This is taken from pydantic.env_settings.EnvSettingsSource
        d: dict[str, str | None] = {}

        for field in settings.__fields__.values():
            env_val: str | None = None
            for env_name in field.field_info.extra["env_names"]:
                db_setting = db_settings.get(env_name)
                if db_setting is not None:
                    env_val = db_setting
                    break

            if env_val is None:
                continue

            if field.is_complex():
                try:
                    env_val = settings.__config__.json_loads(env_val)
                except ValueError as e:
                    raise pydantic.env_settings.SettingsError(
                        f'error parsing JSON for "{env_name}"'
                    ) from e
            d[field.alias] = env_val

        return d


class DatabaseSettingsMixin(pydantic.BaseSettings):
    db_session: Session | None = None

    def __init__(self, *, db_session: Session, **kwargs):
        self.__config__.db_session = db_session  # type: ignore[attr-defined]
        super().__init__(**kwargs)
        for key, value in self.dict().items():
            _logger.info("Loading database setting [key=%s, value=%s]", key, value)

    class Config:
        db_session: ClassVar

        @classmethod
        def customise_sources(
            cls,
            init_settings,
            env_settings,
            file_secret_settings,
        ):
            """
            Add database settings as a settings source.

            New setting priorities becomes:

            1. Arguments passed to the Settings class initialiser.
            2. Environment variables
            3. Database variables
            4. Variables loaded from a dotenv (.env) file.
            5. Variables loaded from the secrets directory.
            6. The default field values for the Settings model.

            See https://pydantic-docs.helpmanual.io/usage/settings/#field-value-priority
            """
            return (
                init_settings,
                env_settings,
                DatabaseSettingsSource(cls.db_session),
                file_secret_settings,
            )


def save_setting(setting):
    setting_repository.save(setting)


def get_setting_by_key(key):
    setting = setting_repository.find_by_key(key)
    if not setting:
        raise ResourceNotFoundException("setting", key)
    return setting


def delete_setting(setting):
    setting_repository.delete_setting(setting)
