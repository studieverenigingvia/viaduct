import json
import logging
from typing import Any

import requests
from pydantic import BaseSettings

from app.models.user import User
from app.roles import Roles
from app.service.group_service import get_groups_for_user
from app.service.role_service import user_get_roles

_logger = logging.getLogger(__name__)

_HAS_PAID = "has_paid"

_indices_settings = [
    ("page", None),
    ("news", _HAS_PAID),
    ("activity", None),
    ("pimpy_minute", Roles.PIMPY_READ),
    ("pimpy_task", Roles.PIMPY_READ),
    ("examination", _HAS_PAID),
    ("alv", _HAS_PAID),
]


class SearchConnectionUrl(BaseSettings):
    # AnyUrl is really weird, calling str() on it will only
    manticore_url: str = "http://manticore:9308/json/search"


def execute_search_request(
    user, query: str, index: str, limit: int, offset: int
) -> list[dict[str, Any]]:
    filters = (
        get_permissions_filter(user)
        + get_has_paid_filters(user)
        + get_group_filter(user)
    )

    if not index:
        index = ",".join(get_allowed_indices(user))

    # docs.manticoresearch.com/latest/html/searching/escaping_in_queries.html
    for c in ["\\", "!", '"', "$", "'", "(", ")", "-", "/", "<", "@", "^", "|", "~"]:
        query = query.replace(c, f"\\{c}")

    json_request = {
        "query": {"bool": {"must": [{"query_string": query}], "should": filters}},
        "index": index,
        "limit": limit,
        "offset": offset,
    }

    url = SearchConnectionUrl()
    if not url.manticore_url.endswith("/json/search"):
        _logger.warning(
            "Manticore search path is not '/json/search': %s", url.manticore_url
        )

    result = requests.post(
        SearchConnectionUrl().manticore_url,
        headers={"Accept": "application/json"},
        json=json_request,
    )
    if not result.ok:
        _logger.error(result.content)
    result.raise_for_status()

    return json.loads(result.content, strict=False)


def get_permissions_filter(user: User) -> list[dict[str, dict[str, Any]]]:
    permissions = user_get_roles(user)

    return [
        {"equals": {"permission": str(permission.name)}} for permission in permissions
    ]


def get_group_filter(user: User) -> list[dict[str, dict[str, Any]]]:
    groups = get_groups_for_user(user)

    return [{"equals": {"group_id": group.id}} for group in groups]


def get_has_paid_filters(user: User) -> list[dict[str, dict[str, Any]]]:
    # If a user has paid, it should see both the has_paid and non-has_paid
    # content
    if user.has_paid:
        has_paid_states = [0, 1]
    else:
        has_paid_states = [0]

    return [{"equals": {"needs_paid": has_paid}} for has_paid in has_paid_states]


def get_allowed_indices(user: User) -> list[str]:
    """
    Create a json dict that is used to display the available indices to the
    user using vue.js.

    :param user: User to fetch the indices for.
    :return: dict with a key for each index that the user is allowed to display
    together with a description and a
    font-awesome icon.
    """
    user_permissions = user_get_roles(user)
    user_has_paid = bool(user.has_paid)

    indices = []
    for index, index_permissions in _indices_settings:
        if user_can_see_index(index_permissions, user_permissions, user_has_paid):
            indices.append(index)

    return indices


def user_can_see_index(
    index_permission: Any, user_permissions: list[Roles], user_has_paid: bool
) -> bool:
    if not index_permission:
        return True

    if index_permission in user_permissions:
        return True

    if index_permission is _HAS_PAID and user_has_paid:
        return True

    return False
