from datetime import date

from werkzeug.datastructures import FileStorage

from app.enums import FileCategory
from app.exceptions.base import BusinessRuleException
from app.models.course import Course
from app.models.examination import ExamFileType, Examination
from app.repository import examination_repository, model_service
from app.service import course_service, file_service


def find_all_examinations_by_course_id(course_id: int) -> list[Examination]:
    course = course_service.get_courses_by_ids([course_id])[0]
    exams: list[int] = [course.id]
    if course.old_codes:
        exams.extend(course.old_codes)  # type: ignore
    return examination_repository.get_all_examinations_by_course_ids(exams)


def add_examination(
    exam_date: date,
    comment: str,
    course_id: int,
    test_type,
) -> Examination:
    course = model_service.get_by_id(Course, course_id)

    if not course_service.course_available(course, exam_date):
        raise BusinessRuleException(f"Course unavailable on {exam_date.isoformat()}")

    exam = examination_repository.create_examination()
    exam.date = exam_date
    exam.comment = comment
    exam.course = course
    exam.test_type = test_type

    model_service.save(exam)

    return exam


def update_examination(
    exam: Examination,
    exam_date: date,
    comment: str,
    course_id: int,
    test_type: str,
) -> Examination:
    course = model_service.get_by_id(Course, course_id)
    if not course_service.course_available(course, exam_date):
        raise BusinessRuleException(f"Course unavailable on {exam_date.isoformat()}")

    exam.date = exam_date
    exam.comment = comment
    exam.course = course
    exam.test_type = test_type

    model_service.save(exam)

    return exam


def delete_examination(examination: Examination):
    model_service.delete(examination)

    if examination.examination_file is not None:
        file_service.delete_file(examination.examination_file)
    if examination.answers_file is not None:
        file_service.delete_file(examination.answers_file)


def count_examinations_by_course(course: Course) -> int:
    exams = examination_repository.get_all_examinations_by_course_id(course.id)
    return len(exams)


def save_exam_file(
    exam: Examination, exam_filestorage: FileStorage, file_type: ExamFileType
):
    # Fetch old file.
    old_exam_file = None

    if file_type is ExamFileType.EXAMINATION:
        old_exam_file = exam.examination_file
    elif file_type is ExamFileType.ANSWERS:
        old_exam_file = exam.answers_file
    elif file_type is ExamFileType.SUMMARY:
        old_exam_file = exam.summary_file

    exam_file = file_service.add_file_to_s3(
        FileCategory.EXAMINATION, exam.id, exam_filestorage
    )

    if file_type is ExamFileType.EXAMINATION:
        exam.examination_file = exam_file
    elif file_type is ExamFileType.ANSWERS:
        exam.answers_file = exam_file
    elif file_type is ExamFileType.SUMMARY:
        exam.summary_file = exam_file

    model_service.save(exam)

    if old_exam_file is not None:
        file_service.delete_file(old_exam_file)
