from collections.abc import Sequence

import bleach
from bleach_allowlist import all_styles, generally_xss_safe
from markdown import markdown
from markdown.extensions import Extension

from app.utils.markdown_blockquotes import MarkdownBlockquotesExtension
from app.utils.markdown_pretix import MarkdownPretixWidgetExtension
from app.utils.markdown_strikethrough import MarkdownStrikethroughExtension
from app.utils.markdown_summary import MarkdownSummaryExtension, SummaryOptions
from app.utils.markdown_youtube import MarkdownYoutubeExtension

markdown_extensions: Sequence[str | Extension] = [
    "toc",
    "tables",
    MarkdownYoutubeExtension(),
    MarkdownBlockquotesExtension(),
    MarkdownPretixWidgetExtension(),
    MarkdownStrikethroughExtension(),
]


# This is a combination of bleach's print_attrs and markdown_attrs. In addition,
# data-target and data-toggle are allowed for urls and buttons to facilitate a
# tool to improve the book sale. We combine these dictionaries manually because
# they contain lists that have to be merged.
all_allowed_attrs = {
    "*": ["id", "class", "style"],
    "img": ["src", "width", "height", "alt", "title"],
    "a": ["href", "alt", "title", "data-target", "data-toggle"],
    "button": ["data-target", "data-toggle"],
}


def render_markdown_safely(markdown_content: str):
    """
    Renders markdown as escaped HTML.

    E.g. **via** --> <strong>via</strong>
    <script> ... </script> --> &lt;script&gt; ... &lt;/script&gt;
    """
    return markdown(_clean_html(markdown_content), extensions=markdown_extensions)


def render_markdown_summarized_safely(
    markdown_content: str,
    options: SummaryOptions,
):
    return markdown(
        _clean_html(markdown_content), extensions=[MarkdownSummaryExtension(options)]
    )


def _clean_html(data: str):
    return bleach.clean(
        data,
        tags=generally_xss_safe,
        attributes=all_allowed_attrs,
        styles=all_styles,
    )
