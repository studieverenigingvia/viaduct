import functools
import logging
from datetime import datetime
from typing import Any, TypedDict
from urllib.parse import quote_plus, urljoin

import requests
from requests.structures import CaseInsensitiveDict

from app import app
from app.enums import ProgrammeType
from app.exceptions.base import ResourceNotFoundException
from app.models.course import Course
from app.networking import DefaultResponseAdapter
from app.service import course_service
from app.utils.marshmallow import create_schema_from_named_tuple

_logger = logging.getLogger(__name__)

BASE_URL = "https://api.datanose.nl"
DATANOSE_SESSION = requests.Session()
DATANOSE_SESSION.mount(BASE_URL, DefaultResponseAdapter("DataNose"))
DATANOSE_SESSION.headers = CaseInsensitiveDict({"Accept": "application/json"})


class DataNoseProgramme:
    def __init__(self, code: str, name: str, _type: ProgrammeType) -> None:
        self.code = code
        self.name = name
        self.programme_type = _type

    def __repr__(self) -> str:
        return f"<{self.__class__.__name__} '{self.name}'>"


class DataNoseCourse(TypedDict):
    NameDutch: str
    NameEnglish: str
    EC: int
    CatalogNumber: str
    AcademicYear: int
    CatalogCourseURL: str
    # Owner is the programme code.
    Owner: str


DataNoseCourseSchema = create_schema_from_named_tuple(DataNoseCourse)


def mock_on_debug(func):
    """Decorator for mocking responses to functions with API calls."""

    @functools.wraps(func)
    def wrapper(*args):
        if app.debug:
            if func.__name__ == "get_enrolled_courses":
                # Fetch courses linked in the createdb cli.
                return course_service.get_courses_by_datanose_codes(
                    ["52041MAL6Y", "5204DLNL6Y", "5314INTH6Y"]
                )
            elif func.__name__ == "get_study_programmes":
                return [
                    DataNoseProgramme(
                        "MSc AI", "Master Artificial Intelligence", ProgrammeType.MASTER
                    ),
                    DataNoseProgramme(
                        "MSc Logic", "Master Logic", ProgrammeType.MASTER
                    ),
                ]
        else:
            return func(*args)

    return wrapper


def _execute_get_request(relative_url: str) -> list[dict[str, Any]]:
    result = DATANOSE_SESSION.get(BASE_URL + relative_url)

    if result.status_code != 200:
        return []
    return result.json()


def get_current_academic_year() -> int:
    """Return the starting year this college year."""
    today = datetime.today()

    if today.month >= 9:
        return today.year
    else:
        return today.year - 1


_api_programme_type_map = {
    "None": ProgrammeType.OTHER,
    "Bachelor": ProgrammeType.BACHELOR,
    "Major": ProgrammeType.MAJOR,
    "Minor": ProgrammeType.MINOR,
    "Premaster": ProgrammeType.PRE_MASTER,
    "Master": ProgrammeType.MASTER,
}


@mock_on_debug
def get_study_programmes(student_id: str) -> list[DataNoseProgramme]:
    result = _execute_get_request(urljoin("/Programmes/", quote_plus(student_id)))

    # Either a failed request or an empty result
    if not result:
        return []

    programmes = []

    for programme in result:
        try:
            programme_type = programme["Type"]

            if programme_type in _api_programme_type_map:
                _type = _api_programme_type_map[programme_type]
            else:
                _logger.error(
                    "Got unknown programme type from DataNose: " + str(programme_type)
                )
                _type = ProgrammeType.OTHER

            p = DataNoseProgramme(programme["Code"], programme["Name"], _type)
            programmes.append(p)

        except KeyError:
            _logger.error(f"Got invalid programme entry from DataNose: {programme}")
            continue

    return programmes


def _parse_course_programmes(course: dict) -> dict:
    """
    Combines tracks of educations into one main education.
    Transforms year and period strings to ints, and adds periods to every programme.
    """
    parsed_course = {
        "CatalogNumber": course["CatalogNumber"],
        "Name": course["Name"],
        "Programmes": [],
    }

    if not course["Programmes"]:
        return parsed_course

    for programme in course["Programmes"]:
        # Combine tracks of educations into one education.
        # Add programme to parsed_course if not yet added.
        dash_idx = programme["ProgrammeCode"].find("-")
        if dash_idx != -1:
            main_programme_code = programme["ProgrammeCode"][:dash_idx]

            if main_programme_code not in parsed_course["Programmes"]:
                programme["ProgrammeCode"] = main_programme_code
                parsed_course["Programmes"].append(programme)
            else:
                continue
        else:
            parsed_course["Programmes"].append(programme)

        # Transform strings into ints.
        programme["Year"] = int(programme["Year"])
        programme["Periods"] = [int(c) for c in course["Periods"].split(",")]

    return parsed_course


def get_course_programmes_by_codes(
    course_dn_code: str,
    education_dn_code: str,
    courses_response: list[dict[str, Any]] | None = None,
) -> dict | None:
    """
    :courses_response: a list of courses that are (partially) owned by the given
    education in Datanose.
    """
    year = get_current_academic_year()

    # Fetch list of courses if no previously fetched list was passed.
    if not courses_response:
        courses = _execute_get_request(
            f"/{year}/Faculty/FNWI/Programmes/{education_dn_code}/Courses"
        )
    else:
        courses = courses_response

    # Find correct course in response.
    for course in courses:
        if course["CatalogNumber"] == course_dn_code:
            # Check if course has any bounded programmes.
            if not course["Programmes"]:
                break

            # Combine tracks, add periods to all programmes, and transform data types.
            course = _parse_course_programmes(course)
            return course["Programmes"]

    return None


def get_course_by_code(datanose_code: str) -> list[DataNoseCourse]:
    return _execute_get_request(urljoin("/Courses/", datanose_code))  # type: ignore


@mock_on_debug
def get_enrolled_courses(student_id: str) -> list[Course]:
    """
    Returns a list of courses which the given student_id is enrolled for.
    Make sure the student_id is confirmed when using this function!
    """
    enrolment_info = _execute_get_request(
        urljoin("/Enrolments/", quote_plus(student_id))
    )

    # Either a failed request or an empty result.
    if not enrolment_info:
        return []

    course_ids = []

    # Get the course names of the current study year.
    for enrolment in enrolment_info:
        try:
            course = course_service.get_course(datanose_code=enrolment["CatalogNumber"])
            course_ids.append(course.id)
        except ResourceNotFoundException:
            # Enrolled course is not in our database.
            pass
        except KeyError:
            _logger.warning(
                f"User has invalid student_id '{student_id}' "
                + "for fetching Datanose courses."
            )
            break

    return course_service.get_courses_by_ids(course_ids)
