import logging
import mimetypes
from typing import IO, Literal

import boto3
from botocore.exceptions import ClientError
from pydantic import BaseSettings
from werkzeug.utils import secure_filename

from app.enums import FileCategory
from app.exceptions.base import ResourceNotFoundException, ServiceUnavailableException
from app.models.file import File

logger = logging.getLogger(__name__)


class S3Config(BaseSettings):
    s3_endpoint = "s3.eu-central-003.backblazeb2.com"
    s3_bucket = "viaduct-files"
    s3_private_bucket = "viaduct-private-files"
    aws_access_key_id = "backblaze_keyID"
    aws_secret_access_key = "backblaze_applicationKey"
    aws_private_access_key_id = "backblaze_private_keyID"
    aws_private_secret_access_key = "backblaze_private_applicationKey"
    use_https = True


def http_string(use_https: bool) -> Literal["http", "https"]:
    return "https" if use_https else "http"


boto3_public_client = boto3.client(
    "s3",
    endpoint_url=f"{http_string(S3Config().use_https)}://{S3Config().s3_endpoint}",
    aws_access_key_id=S3Config().aws_access_key_id,
    aws_secret_access_key=S3Config().aws_secret_access_key,
    region_name="eu-central-003",
)

boto3_private_client = boto3.client(
    "s3",
    endpoint_url=f"{http_string(S3Config().use_https)}://{S3Config().s3_endpoint}",
    aws_access_key_id=S3Config().aws_private_access_key_id,
    aws_secret_access_key=S3Config().aws_private_secret_access_key,
    region_name="eu-central-003",
)


def get_bucket_for_category(category: FileCategory) -> str:
    if category.is_public():
        return S3Config().s3_bucket
    else:
        return S3Config().s3_private_bucket


def get_public_url_for_file(key: str) -> str:
    """This only works for public files (BucketType.PUBLIC)."""
    config = S3Config()
    return (
        f"{http_string(config.use_https)}://{config.s3_endpoint}/"
        f"{config.s3_bucket}/{key}"
    )


def boto3_client_for_bucket(bucket: str) -> boto3.client:
    if bucket == S3Config().s3_bucket:
        return boto3_public_client
    elif bucket == S3Config().s3_private_bucket:
        return boto3_private_client
    else:
        raise ValueError(f"Unknown bucket: {bucket}")


def get_presigned_url_for_file(
    file_: File, override_filename: str | None = None
) -> str:
    """
    This works for both public and private files.

    Override filename is used to set the filename for ALV's
    """
    s3 = boto3_client_for_bucket(file_.s3_bucket_id)

    try:
        s3.head_object(Bucket=file_.s3_bucket_id, Key=file_.hash)
        logger.info(f'Found "{file_.hash}" in the bucket "{file_.s3_bucket_id}".')

        if override_filename is not None:
            fn = secure_filename(override_filename)
        elif file_.display_name is not None:
            fn = secure_filename(f"{file_.display_name}.{file_.extension}")
        else:
            fn = secure_filename(file_.hash)

        return s3.generate_presigned_url(
            "get_object",
            Params={
                "Bucket": file_.s3_bucket_id,
                "Key": file_.hash,
                "ResponseContentDisposition": f'inline; filename="{fn}"',
            },
            ExpiresIn=3600,
        )
    except s3.exceptions.NoSuchKey:
        logger.info(f'Not found "{file_.hash}" in the bucket "{file_.s3_bucket_id}".')
        raise ResourceNotFoundException("file", file_.display_name)
    except Exception:
        logger.exception(f'Unable to find file "{file_.hash}"')
        raise ServiceUnavailableException("Generating file url failed", "s3")


def add_file_to_s3(
    key: str, blob: IO[bytes], bucket: str, friendly_filename: str | None = None
) -> str:
    s3 = boto3_client_for_bucket(bucket)

    content_disposition = "inline"
    if friendly_filename is not None:
        content_disposition += f'; filename="{secure_filename(friendly_filename)}"'

    try:
        s3.put_object(
            Body=blob,
            Bucket=bucket,
            Key=key,
            ContentDisposition=content_disposition,
            ContentType=mimetypes.guess_type(key)[0] or "application/octet-stream",
        )
        logger.info(f"Uploaded in-memory file to S3: {bucket}/{key}")
        return bucket
    except ClientError as e:
        logger.exception(f"Failed uploading file to S3: {bucket}/{key}")
        raise ServiceUnavailableException("Failed to upload file to S3.", "s3") from e


def read_file_from_s3(file: File) -> IO[bytes]:
    s3 = boto3_client_for_bucket(file.s3_bucket_id)

    try:
        return s3.get_object(Bucket=file.s3_bucket_id, Key=file.hash)["Body"]
    except s3.exceptions.NoSuchKey as e:
        logger.exception(f"Cannot find file in S3: {file.s3_bucket_id}/{file.hash}")
        raise ResourceNotFoundException("s3", file.hash) from e


def delete_file(key: str, bucket: str) -> None:
    s3 = boto3_client_for_bucket(bucket)

    try:
        s3.delete_object(Bucket=bucket, Key=key)
    except s3.exceptions.NoSuchKey:
        logger.warning(f"Cannot find+delete file in S3: {bucket}/{key}")
