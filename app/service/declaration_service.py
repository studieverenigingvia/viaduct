import mimetypes

from babel.numbers import format_decimal
from flask_login import current_user
from werkzeug.datastructures import FileStorage, ImmutableMultiDict

from app.models.declaration import Declaration
from app.repository import declaration_repository
from app.service import mail_service
from app.task.mail import MailCommand


def send_declaration(
    files: ImmutableMultiDict[str, FileStorage],
    reason: str,
    committee: str,
    amount: float,
    iban: str,
):
    new_amount = format_decimal(amount, locale="nl_NL")
    user = current_user
    command = MailCommand("penningmeester@svia.nl")
    command.reply_to = user.email
    command.with_template(
        "declaration",
        locale="en",
        user=user,
        reason=reason,
        committee=committee,
        amount=new_amount,
        iban=iban,
    )

    for i, file_key in enumerate(files):
        file = files[file_key]
        extension = mimetypes.guess_extension(file.mimetype) or ""
        command.with_loaded_attachment(
            "File" + str(i + 1) + extension, file.read(), file.mimetype
        )

    mail_service.send_mail(command)
    declaration = Declaration(
        committee=committee, amount=amount, reason=reason, user_id=user.id
    )
    declaration_repository.save(declaration)


def get_all_declarations_by_user_id(user_id: int) -> list[Declaration]:
    return declaration_repository.find_all_declarations_by_user_id(user_id)
