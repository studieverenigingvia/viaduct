import json
import logging
from collections.abc import Callable
from functools import wraps
from typing import Any, NamedTuple

import requests
from pydantic import SecretStr
from requests import Session
from typing_extensions import ParamSpec

from app import db
from app.exceptions.base import BusinessRuleException, ResourceNotFoundException
from app.models.user import User
from app.repository import user_repository
from app.service import (
    education_service,
    mailinglist_service,
    setting_service,
    user_mailing_service,
    user_service,
)
from app.service.setting_service import DatabaseSettingsMixin

_logger = logging.getLogger(__name__)


class CopernicaApiSettings(DatabaseSettingsMixin):
    copernica_enabled: bool = False
    copernica_api_key = SecretStr("")
    copernica_database_id: str = ""
    copernica_webhook_token = SecretStr("")


def copernica_dict_for_user(user: User) -> dict[str, str]:
    """
    Create a POST body for the copernica API.

    Uses the following page for formatting dates.
    https://www.copernica.com/en/documentation/database-fields-and-collections
    """
    if not user.educations:
        education_str = "Other"
    else:
        # Most "important" programme is returned first.
        programmes = user_service.get_user_sorted_study_programmes(user)

        education = programmes[0] if len(programmes) > 0 else None
        if education:
            education_str = education_service.get_education_name(education, "en")
        else:
            education_str = "Other"

    data = {
        "Emailadres": user.email,
        "Voornaam": user.first_name,
        "Achternaam": user.last_name,
        "Studie": education_str,
        "Studienummer": user.student_id,
        "Lid": "Ja" if user.has_paid else "Nee",
        "Alumnus": "Ja" if user.alumnus else "Nee",
        "VVV": "Ja" if user.favourer else "Nee",
        "Geboortedatum": (
            user.birth_date.strftime("%Y-%m-%d") if user.birth_date else "0000-00-00"
        ),
        "Taal": user.locale,
        "WebsiteID": str(user.id),
    }

    subscribed_mailinglists = set(
        user_mailing_service.get_user_subscribed_mailing_lists(user)
    )

    for mailing_list in mailinglist_service.get_all_mailinglists():
        subscribed = mailing_list in subscribed_mailinglists
        if mailing_list.members_only:
            subscribed = subscribed and user.has_paid  # type: ignore

        data[mailing_list.copernica_column_name] = "Ja" if subscribed else "Nee"

    return data


def handle_webhook(data: dict[str, str]) -> bool:
    try:
        if data["type"] != "update":
            return False
        settings = CopernicaApiSettings(db_session=db.session)
        if data["database"] != settings.copernica_database_id:
            return False

        profile_id = int(data["profile"])
        user = user_service.find_by_copernica_id(profile_id)
        if not user:
            return False

        old_user_mailing_lists = frozenset(user.mailinglists.all())
        new_user_mailing_lists = set(user.mailinglists.all())

        for mailinglist in mailinglist_service.get_all_mailinglists():
            param_name = f"parameters[{mailinglist.copernica_column_name}]"

            value = data.get(param_name)
            if not value:
                continue

            if value == "Ja":
                new_user_mailing_lists.add(mailinglist)
            else:
                # Regard anything else as no, but log weird values
                if value != "Nee":
                    _logger.error(
                        "Received invalid value for "
                        f"Copernica mailing list column: {value}"
                    )

                if mailinglist in new_user_mailing_lists:
                    new_user_mailing_lists.remove(mailinglist)

        if new_user_mailing_lists != old_user_mailing_lists:
            # Unfortunately we cannot detect whether the webhook
            # call was caused by our own API call. Therefore we don't
            # execute another API call when updating the mailing lists
            # to prevent an endless webhook/API call loop.
            user_mailing_service.set_mailing_list_subscriptions(
                user, new_user_mailing_lists, update_copernica=False
            )

        return True

    except (KeyError, ValueError):
        _logger.exception(f"Received invalid content for Copernica webhook: {data}")

        return False


P = ParamSpec("P")


def copernica_enabled(default_return: Callable[..., Any]):
    def decorator(f: Callable[P, None]) -> Callable[P, None]:
        @wraps(f)
        def wrapped(*args: P.args, **kwargs: P.kwargs) -> None:
            settings = CopernicaApiSettings(db_session=db.session)

            _logger.info("COPERNICA_ENABLED=%s", settings.copernica_enabled)

            if not settings.copernica_enabled:
                return default_return()
            else:
                return f(*args, **kwargs)

        return wrapped

    return decorator


class CopernicaWelcomeMailSettings(DatabaseSettingsMixin):
    copernica_welcome_mail_en_doc_id: int = 483
    copernica_welcome_mail_nl_doc_id: int = 484


@copernica_enabled(default_return=lambda: None)
def send_welcome_mail(user: User):
    settings = CopernicaApiSettings(db_session=db.session)
    url = "https://api.copernica.com/v2/publisher/emailing?access_token={}"
    url = url.format(settings.copernica_api_key.get_secret_value())
    if not user.copernica_id:
        raise Exception(
            f"copernica_id not available for user {user.id} in task 'send_welcome_mail'"
        )

    mail_settings = CopernicaWelcomeMailSettings(db_session=db.session)
    if user.locale == "en":
        document = mail_settings.copernica_welcome_mail_en_doc_id
    else:
        document = mail_settings.copernica_welcome_mail_nl_doc_id

    r = requests.post(
        url,
        data={
            "target": user.copernica_id,
            "targettype": "profile",
            "document": document,
            "settings": {"start": False, "iterations": 1},
        },
    )

    if r.status_code != 201:
        raise Exception(
            f"Could not send welcome mail to profile {user.copernica_id}"
            f" from Copernica. status_code={r.status_code}, data={r.json()}"
        )


class CopernicaDBField(NamedTuple):
    ID: int
    name: str
    type: str
    value: str


class CopernicaProfileInfo(NamedTuple):
    ID: int
    database_id: int
    secret: str
    fields: dict[str, str]


def copernica_integration_enabled():
    settings = CopernicaApiSettings(db_session=db.session)
    return settings.copernica_enabled


@copernica_enabled(default_return=list)
def get_database_fields() -> list[CopernicaDBField]:
    settings = CopernicaApiSettings(db_session=db.session)
    url = "https://api.copernica.com/database/{}/fields?access_token={}"
    url = url.format(
        settings.copernica_database_id, settings.copernica_api_key.get_secret_value()
    )

    r = requests.get(url)
    if r.status_code != 200:
        _logger.error(
            "Got invalid response from Copernica. "
            f"status_code={r.status_code}, data={r.json()}"
        )
        return []

    return [
        CopernicaDBField(
            ID=int(field["ID"]),
            name=field["name"],
            type=field["type"],
            value=field["value"],
        )
        for field in r.json()["data"]
    ]


def get_disabling_selections() -> dict[str, int]:
    try:
        s = setting_service.get_setting_by_key("COPERNICA_DISABLING_SELECTIONS")
        return json.loads(s.value)
    except ResourceNotFoundException:
        _logger.warning("COPERNICA_DISABLING_SELECTIONS setting not found.")
        return {}


@copernica_enabled(default_return=list)
def get_selection_profiles(selection_id: int):
    settings = CopernicaApiSettings(db_session=db.session)
    url = "https://api.copernica.com/v2/view/{}/profileids?access_token={}"
    url = url.format(selection_id, settings.copernica_api_key.get_secret_value())

    r = requests.get(url)
    if r.status_code != 200:
        _logger.error(
            "Got invalid response from Copernica. "
            f"status_code={r.status_code}, data={r.json()}"
        )
        return []
    return list(map(int, r.json()))


@copernica_enabled(default_return=lambda: None)
def update_profile(user: User) -> None:
    data = copernica_dict_for_user(user)

    if not user.copernica_id:
        _logger.warning("%s missing copernica_id", user)
        return
    settings = CopernicaApiSettings(db_session=db.session)
    url = (
        "https://api.copernica.com/database/{}/profiles?"
        "fields[]=ID%3D%3D{}&access_token={}"
    )
    url = url.format(
        settings.copernica_database_id,
        user.copernica_id,
        settings.copernica_api_key.get_secret_value(),
    )
    requests.put(url, data)


@copernica_enabled(default_return=lambda: None)
def create_profile(user: User) -> None:
    data = copernica_dict_for_user(user)

    if user.copernica_id:
        _logger.error("%s has copernica_id", user)
        return

    settings = CopernicaApiSettings(db_session=db.session)
    url = "https://api.copernica.com/database/{}/profiles?access_token={}"
    url = url.format(
        settings.copernica_database_id, settings.copernica_api_key.get_secret_value()
    )
    r = requests.post(url, data)

    user.copernica_id = int(r.headers["X-Created"])
    user_repository.save(user)


@copernica_enabled(default_return=lambda: None)
def create_lecture_only_profile(
    db_session: Session, first_name: str, last_name: str, email: str, student_id: str
) -> None:
    settings = CopernicaApiSettings(db_session=db_session)
    data = {
        "Emailadres": email,
        "Voornaam": first_name,
        "Achternaam": last_name,
        "Studienummer": student_id,
        "WebsiteID": 0,
    }
    # This API creates a new profile
    url = "https://api.copernica.com/database/{db_id}/profiles?access_token={token}"
    url = url.format(
        db_id=settings.copernica_database_id,
        token=settings.copernica_api_key.get_secret_value(),
    )
    r = requests.post(url, data)
    _logger.debug(r.content, r.headers)
    r.raise_for_status()


@copernica_enabled(default_return=lambda: None)
def get_profile_info(profile_id: int):
    settings = CopernicaApiSettings(db_session=db.session)
    url = "https://api.copernica.com/profile/{}/?access_token={}"
    url = url.format(profile_id, settings.copernica_api_key.get_secret_value())

    r = requests.get(url)
    data = r.json()

    if r.status_code != 200:
        if data["error"]["message"] != "No entity with supplied ID":
            _logger.error(
                "Got invalid response from Copernica. "
                f"status_code={r.status_code}, data={data}"
            )

        return None

    return CopernicaProfileInfo(
        ID=int(data["ID"]),
        database_id=data["database"],
        secret=data["secret"],
        fields=data["fields"],
    )


@copernica_enabled(default_return=lambda: None)
def delete_profile(profile_id: int):
    settings = CopernicaApiSettings(db_session=db.session)
    url = "https://api.copernica.com/profile/{}/?access_token={}"
    url = url.format(profile_id, settings.copernica_api_key.get_secret_value())

    r = requests.delete(url)
    if r.status_code != 200:
        _logger.error(
            f"Could not delete profile {profile_id} from Copernica."
            f"status_code={r.status_code}, data={r.json()}"
        )


@copernica_enabled(default_return=lambda: None)
def validate_column(
    copernica_column_name: str, database_fields: list[CopernicaDBField]
) -> None:
    db_fields = {field.name: field for field in database_fields}
    field = db_fields.get(copernica_column_name)
    if not field:
        raise BusinessRuleException(
            f"The Copernica field '{copernica_column_name}' " f"does not exist"
        )
    elif field.type != "text":
        if field.type == "select":
            if set(field.value.strip().splitlines()) != {"Ja", "Nee"}:
                raise BusinessRuleException(
                    f"The Copernica field '{copernica_column_name}' "
                    f"has invalid options."
                )
        else:
            raise BusinessRuleException(
                f"The Copernica field '{copernica_column_name}' "
                f"is not of type 'text' or 'select'"
            )
