import io
import logging
import mimetypes
import re
from pathlib import Path
from typing import IO, Literal
from uuid import uuid4

from flask import send_file
from fuzzywuzzy import fuzz
from PIL import Image
from werkzeug.datastructures import FileStorage
from werkzeug.utils import redirect
from werkzeug.wrappers import Response

from app import hashfs
from app.enums import FileCategory
from app.exceptions.base import ResourceNotFoundException
from app.models.file import File
from app.repository import file_repository
from app.service import s3_service

FILENAME_REGEX = re.compile(r"(.+)\.([^\s.]+)")

_logger = logging.getLogger(__name__)


def add_file_to_s3(
    category: FileCategory, object_id: int | None, data: FileStorage
) -> File:
    """Create a File and save it to S3, preferred over add_file."""
    if data.filename and (m := FILENAME_REGEX.match(data.filename)) is not None:
        display_name = m.group(1)
        extension = m.group(2)
    else:
        display_name = data.filename or f"{category.name.lower()}-{object_id}"
        extension = get_extension_from_mimetype(data.mimetype)

    key = Path(category.name.lower())
    # Note: for files saved in S3, the hash contains both hash and extension.
    # This way we can generate direct urls to the bucket with the correct
    # extension.
    if object_id is not None:
        key = key / str(object_id) / f"{uuid4()}.{extension}"
    else:
        key = key / f"{uuid4()}.{extension}"
    key_hash = str(key)

    bucket = s3_service.get_bucket_for_category(category)
    bucket_id = s3_service.add_file_to_s3(
        key=key_hash,
        blob=data.stream,
        bucket=bucket,
        friendly_filename=f"{display_name}.{extension}",
    )

    f = file_repository.create_file()
    f.s3_bucket_id = bucket_id
    if category in FileCategory.all_uploads():
        f.display_name = display_name

    f.hash = key_hash
    # Not used, but saved for consistency for now. See comment on key-variable.
    f.extension = extension
    f.category = category

    file_repository.save(f)
    return f


def delete_file(_file):
    _hash = _file.hash
    bucket_id = _file.s3_bucket_id
    file_repository.delete(_file)
    if not file_repository.find_all_files_by_hash(_hash):
        hashfs.delete(_hash)

        if bucket_id:
            s3_service.delete_file(key=_hash, bucket=bucket_id)


def get_file_by_id(file_id: int | None) -> File:
    f = file_repository.find_file_by_id(file_id)
    if not f:
        raise ResourceNotFoundException("file", file_id)
    return f


def get_thumbnail_of_hashfs_file(_file: File, thumbnail_size=None) -> io.BytesIO:
    if not thumbnail_size:
        thumbnail_size = (400, 400)

    try:
        with hashfs.open(_file.hash) as f:
            return image_resize(f, _file.extension, thumbnail_size)
    except OSError as e:
        _logger.error("Error processing thumbnail", exc_info=True)
        raise ResourceNotFoundException("file content", _file.hash) from e


def image_resize(f: FileStorage | IO[bytes], extension, to_size=None) -> io.BytesIO:
    if not to_size:
        to_size = (400, 400)

    im = Image.open(f)
    im.thumbnail(to_size)

    out = io.BytesIO()
    im.save(out, format=im.format or extension)
    out.seek(0)
    return out


def get_file_mimetype(_file, add_http_text_charset="utf-8"):
    try:
        mimetype = mimetypes.types_map["." + _file.extension]
        if mimetype.startswith("text/") and add_http_text_charset is not None:
            mimetype += "; charset=" + add_http_text_charset

        return mimetype
    except KeyError:
        return "application/octet-stream"


def get_extension_from_mimetype(mimetype: str):
    ext = mimetypes.guess_extension(mimetype)
    if ext:
        return ext[1:]
    return ""


def get_all_uploads(page_nr=None, per_page=None):
    return file_repository.find_all_files_by_categories(
        FileCategory.all_uploads(), page_nr, per_page
    )


def get_all_files(page_nr=None, per_page=None):
    return file_repository.find_all_files(page_nr, per_page)


def search_files_in_uploads(search):
    search = search.lower()
    all_files = get_all_uploads()
    file_scores = {}

    for f in all_files:
        score = fuzz.partial_ratio(search, f.full_display_name.lower())
        if score > 75:
            file_scores[f] = score

    files = sorted(file_scores, key=file_scores.get, reverse=True)
    return files


def ensure_thumbnail(file_: File, size: tuple[int, int] = (400, 400)) -> None:
    data = s3_service.read_file_from_s3(file_)
    stream = image_resize(data, file_.extension, size)

    bucket = s3_service.get_bucket_for_category(file_.category)
    s3_service.add_file_to_s3(f"thumbnails/{file_.hash}", stream, bucket)
    _logger.info("File thumbnailed")


def send_file_inline(_file: File, filename=None):
    address = hashfs.get(_file.hash)
    if not address:
        raise ResourceNotFoundException("file content", _file.hash)

    # Pass the attachment_filename argument, as it is used for mimetype.
    r: Response = send_file(address.abspath, download_name=filename)
    if filename:
        r.headers["Content-Disposition"] = f'inline; filename="{filename}"'

    return r


def serve_file(
    file: File, filename_for_hashfs_or_private: str | None = None
) -> Response:
    """
    Serve a file from either hashfs or s3, depending on the file.

    We cannot override the filename for public files on S3, as that has to be done
    on upload.
    The override is only used for hashfs files and private S3 files.
    """
    if file.s3_bucket_id:
        if file.category.is_public():
            key = file.hash
            return redirect(s3_service.get_public_url_for_file(key))

        return redirect(
            s3_service.get_presigned_url_for_file(file, filename_for_hashfs_or_private)
        )

    return send_file_inline(
        file, filename_for_hashfs_or_private or file.full_display_name
    )


def send_picture_generic(
    file_id: int | None,
    filename: str,
    picture_type: Literal["normal", "thumbnail"],
    thumbnail_size: tuple[int, int] | None = None,
) -> Response:
    """
    Serve a picture file from either hashfs or s3, depending on the file.

    Hashfs doesn't have pregenerated thumbnails, so we need to create them on the fly.
    Thumbnail_size and filename is only used for hashfs files.
    """
    if picture_type not in ("normal", "thumbnail"):
        picture_type = "normal"

    picture_file = get_file_by_id(file_id)

    filename = f"{filename}.{picture_file.extension}"
    if picture_type == "normal":
        return serve_file(picture_file)
    else:
        if picture_file.s3_bucket_id:
            return redirect(
                s3_service.get_public_url_for_file(f"thumbnails/{picture_file.hash}")
            )

        thumbnail = get_thumbnail_of_hashfs_file(picture_file, thumbnail_size)
        r = send_file(thumbnail, download_name=filename)
        r.headers["Content-Disposition"] = f'inline; filename="{filename}"'
        return r
