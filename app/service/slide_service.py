import logging
from datetime import datetime
from math import ceil, floor
from typing import Literal

import requests
from pydantic import BaseModel, ValidationError
from requests.exceptions import RequestException

from app import db
from app.exceptions.base import ServiceUnavailableException
from app.service.setting_service import DatabaseSettingsMixin

_logger = logging.getLogger(__name__)

OV_API_BASE_URL = "https://ovapi.nl/ovi"


class OvSlideSettings(DatabaseSettingsMixin):
    ov_bus_location: str = "NL:S:30000494"
    ov_train_location: str = "NL:S:assp"


class Station(BaseModel):
    uicCode: str
    longName: str
    stationCode: str
    stationType: str


class Notice(BaseModel):
    text: str
    alert: bool | None = None


class Departure(BaseModel):
    # Mixed
    delay: int | None
    tripStopStatus: Literal[
        "DRIVING", "ARRIVED", "CANCEL", "PLANNED", "PASSED", "UNKNOWN"
    ]
    operatorName: str

    # Train
    trainNumber: int | None
    trainId: int | None
    aimedDepartureTime: datetime | None
    plannedDestination: str | None
    platform: str | None
    platformChanged: bool | None
    operatorCode: str | None
    trainCategoryCode: str | None
    trainCategoryName: str | None
    notices: list[Notice] | None
    operationDate: int | None

    # Bus
    aimedDeparture: datetime | None
    vehicleJourneyId: str | None
    serviceDate: str | None
    vehicleJourneyNumber: str | None
    transportMode: str | None
    destination: str | None
    operatorId: str | None
    lineCode: str | None


class TextItem(BaseModel):
    text: str


class AlternativeTransportItem(BaseModel):
    departureLocation: str
    measure: str


class Alert(BaseModel):
    title: str | None = None
    subTitle: str | None = None
    message: str | None = None
    situation: str | None = None
    # measure: Optional[List[TextItem]] = None
    # reason: Optional[str] = None
    # id: str
    # timeStamp: Optional[str] = None
    # priorityNumber: int
    # source: str
    # alternativeTransport: Optional[List[AlternativeTransportItem]] = None
    # startTime: Optional[str] = None
    # prognosis: Optional[str] = None
    # unscheduledDisruption: Optional[bool] = None
    # advice: Optional[List[TextItem]] = None


class StopArea(BaseModel):
    name: str
    street: str
    town: str
    id: str
    type: str


class DepartureBoard(BaseModel):
    # Mixed
    departures: list[Departure]
    alerts: list[Alert] | None

    # Train
    station: Station | None

    # Bus
    stopArea: StopArea | None


class SlideDeparture:
    def __init__(self, departure: Departure):
        self.cancelled = departure.tripStopStatus == "CANCEL"

        if departure.aimedDeparture:
            self.time = datetime.strftime(departure.aimedDeparture, "%H:%M")
        elif departure.aimedDepartureTime:
            self.time = datetime.strftime(departure.aimedDepartureTime, "%H:%M")
        else:
            self.time = "?"

        self.realtime_delay = 0
        if departure.delay:
            self.realtime_delay = (
                ceil(departure.delay / 60.0)
                if departure.delay > 0
                else floor(departure.delay / 60.0)
            )

        self.platform = ""
        if departure.platform:
            self.platform = str(departure.platform)

        self.service = ""
        if departure.lineCode:
            self.service = departure.lineCode

        self.destination_name = ""
        if departure.plannedDestination:
            self.destination_name = departure.plannedDestination
        if departure.destination:
            self.destination_name = departure.destination

        self.remark: list[str] = []

        if departure.notices is not None:
            for notice in departure.notices:
                self._add_remark(notice.text)
        else:
            if self.cancelled:
                self._add_remark("Geannuleerd")

    def _add_remark(self, remark):
        self.remark.append(remark)


class SlideDepartures:
    def __init__(self, departure_board: DepartureBoard):
        if departure_board.stopArea:
            self.dep_type = "bus"
            self.name = departure_board.stopArea.name
        elif departure_board.station:
            self.dep_type = "train"
            self.name = departure_board.station.longName
        else:
            raise ValueError("No stopArea or station in departureboard")

        self.alerts = departure_board.alerts or []

        self.departures = [SlideDeparture(i) for i in departure_board.departures]


def get_bus_train_departures():
    slide_settings = OvSlideSettings(db_session=db.session)

    bus_departures = _fetch_departures(slide_settings.ov_bus_location)
    train_departures = _fetch_departures(slide_settings.ov_train_location, trains=True)

    return bus_departures, train_departures


def _fetch_departures(stop_area_id: str, trains=False):
    headers = {
        "User-Agent": "OVinfo 4.5.2/525 (Android 12 + SDK: 32 Device: Google Pixel 3a)",
        "Authorization": "Basic YW5kcm9pZDptdmR6aWc=",
    }

    params = {"stopAreaId": stop_area_id, "lang": "NL"}

    try:
        req = requests.get(
            OV_API_BASE_URL
            + ("/trains/departureboard" if trains else "/departureboard"),
            headers=headers,
            params=params,
        )
        req.raise_for_status()
        departure_board = DepartureBoard.parse_obj(req.json())
    except (RequestException, ValidationError) as e:
        _logger.error("OVapi error %s", e)

        raise ServiceUnavailableException(
            service="OVinfo", details="Unable to connect to the OVapi"
        ) from e

    slide_data = SlideDepartures(departure_board)
    return slide_data
