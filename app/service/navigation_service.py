import logging
from collections import defaultdict
from collections.abc import Callable
from typing import Literal

from flask import url_for
from flask_login import current_user

from app import db
from app.exceptions.base import (
    BusinessRuleException,
    ResourceNotFoundException,
    ValidationException,
)
from app.models.navigation import NavigationEntry
from app.models.page import Page
from app.models.user import User
from app.repository import (
    model_service,
    navigation_repository,
    page_repository,
    role_repository,
)
from app.roles import Roles
from app.service import page_service

_logger = logging.getLogger(__name__)


def get_stripped_path(input_path: str, depth: int) -> str:
    path = input_path.rstrip("0123456789")
    path = path.rstrip("/")
    path = path.lstrip("/")

    for _ in range(depth):
        try:
            # Try to parse a parent path
            path = path.rsplit("/", 1)[0]
        except IndexError:
            # Path does not contain any more slashes
            break
    _logger.debug(f"get_stripped_path {input_path=} {path=}")
    return path


def get_navigation_root_entries(children_depth) -> list[NavigationEntry]:
    return navigation_repository.get_root_entries(children_depth)


def get_complete_navigation_top_entries(
    user: User, user_roles: list[Roles] | None
) -> list[NavigationEntry]:
    """
    Function should:
    - Retrieve all navigation entries, and preload the children,
     so we have the entire nav structure.
    - Filter out unauthorized entries.
    """

    entries = get_navigation_root_entries(5)
    for entry in entries:
        db.session.expunge(entry)
        temp = [
            remove_unauthorized_recursive(child, user, user_roles, 4)
            for child in get_children(entry)
        ]
        entry.children = [child for child in temp if child]

    return entries


def remove_unauthorized_recursive(
    entry: NavigationEntry, user, user_roles=None, max_depth=2
) -> NavigationEntry | None:
    """
    Function should:
    - Remove unauthorized entries from the list.
    - Recursively call itself to remove unauthorized children from this entry.
    """
    # Remove object from session to prevent modification
    db.session.expunge(entry)

    if max_depth == 0:
        return None

    if not can_view_navigation_entry(entry, user, user_roles):
        return None

    temp = [
        remove_unauthorized_recursive(child, user, user_roles, max_depth - 1)
        for child in get_children(entry)
    ]
    entry.children = [child for child in temp if child]

    return entry


def find_entry_in_navigation_tree(
    comparator: Callable[[NavigationEntry], bool],
    navigation_entries: list[NavigationEntry],
) -> NavigationEntry | None:
    """
    Search for a specific entry in the navigation tree.

    It is expected that you pass in the root entries of the navigation tree. This
    function will search through the tree using a breath-first search.

    :param comparator: A function that takes a NavigationEntry and returns a boolean.
    :param navigation_entries: The list of NavigationEntries to search in.
    """

    stack = list(navigation_entries)
    while stack:
        entry = stack.pop()
        if comparator(entry):
            return entry
        stack.extend(entry.children)
    return None


def get_parent_by_searching_tree(
    entry: NavigationEntry, entries: list[NavigationEntry]
) -> NavigationEntry | None:
    """
    Find the parent (with a breadth first search) of the given entry in the list
    of entries. It is expected that you pass in the root entries of the navigation tree.

    This is a workaround, as the parent is not loaded into the NavEntry
    model by default. The issue is that parent and children create a
    circular reference, which is not supported by the ORM and causes
    an infinite loop when trying to load the query.
    """

    def is_parent(entry_to_find: NavigationEntry) -> bool:
        return entry_to_find.id == entry.parent_id

    return find_entry_in_navigation_tree(is_parent, entries)


def get_navigation_backtrack(
    path, entries: list[NavigationEntry], pages: list[Page]
) -> list[NavigationEntry]:
    """
    Return a list of entries to be used as backtrack menu.

    Can use entries and pages cached from the context processor.
    """
    _logger.debug("Loading navigation backtrack.")
    backtrack = []
    path = get_stripped_path(path, depth=0)
    entry = find_entry_by_path(path, entries, pages)

    while entry:
        backtrack.append(entry)
        entry = get_parent_by_searching_tree(entry, entries)

    backtrack.reverse()
    return backtrack


def get_navigation_side_entries(
    path: str,
    navigation_entries: list[NavigationEntry],
    pages: list[Page] | None = None,
):
    path = path.rstrip("/")

    entry = find_entry_by_path(
        get_stripped_path(path, depth=0), navigation_entries, pages
    )
    parent = None

    if entry:
        parent = get_parent_by_searching_tree(entry, navigation_entries)
    else:
        _logger.debug("Could not find entry for '%s', attempting parent path", path)
        entry = find_entry_by_path(
            get_stripped_path(path, depth=1), navigation_entries, pages
        )
        if entry and entry.parent_id:
            parent = get_parent_by_searching_tree(entry, navigation_entries)

    if parent:
        entries = get_children(parent)
    elif entry:
        entries = [entry]
    else:
        entries = []

    return entries, entry


# Dictionary of parent_id to children where parent_id == None are root entries.
NavigationChildren = dict[int | None, list[NavigationEntry]]


def get_navigation_ordering() -> tuple[list[NavigationEntry], NavigationChildren]:
    entries = model_service.get_all(NavigationEntry)
    entries_by_id = dict()
    root = navigation_repository.get_root_entries(children_depth=0)
    ordering = defaultdict(list)
    for entry in entries:
        entries_by_id[entry.id] = entry
        ordering[entry.parent_id].append(entry)

    # Apply the actual order of the subitems.
    for k, v in ordering.items():
        if k and entries_by_id[k].order_children_alphabetically:
            ordering[k] = sorted(v, key=lambda x: x.title)
        else:
            ordering[k] = sorted(v, key=lambda x: x.position)

    return root, ordering


def find_entry_by_path(
    path: str,
    navigation_entries: list[NavigationEntry] | None = None,
    pages: list[Page] | None = None,
) -> (NavigationEntry | None):
    if pages is None:
        pages = page_repository.get_all_not_deleted()

    if navigation_entries is None:
        navigation_entries = get_complete_navigation_top_entries(
            current_user, role_repository.load_user_roles(current_user.id)
        )

    def find_entry_with_page(page_to_find: Page) -> Callable[[NavigationEntry], bool]:
        def comparator(entry: NavigationEntry) -> bool:
            return entry.page_id == page_to_find.id

        return comparator

    for page in pages:
        if page.path == path:
            found = find_entry_in_navigation_tree(
                find_entry_with_page(page),
                navigation_entries,
            )
            if found:
                return found
            else:
                break

    return find_entry_in_navigation_tree(
        lambda entry: entry.url == path, navigation_entries
    )


def get_entry_by_id(entry_id: int) -> NavigationEntry:
    entry = navigation_repository.find_entry_by_id(entry_id, children_depth=1)
    if not entry:
        raise ResourceNotFoundException("navigation", entry_id)
    return entry


def get_children(
    entry: NavigationEntry,
) -> list[NavigationEntry]:
    """
    Load the children and filter/order them accordingly.

    Note: entry.children raises by default,
    """
    children: list[NavigationEntry] = entry.children

    if entry.order_children_alphabetically:
        children = sorted(children, key=lambda x: x.title)
    else:
        children = sorted(children, key=lambda x: x.position)

    return children


def can_view_navigation_entry(
    entry: NavigationEntry, user: User, user_roles: list[Roles] | None
) -> bool:
    """
    Check whether the current user can view the entry.

    Note: currently only works with pages.
    """
    if entry.external or entry.activity_list or not entry.page:
        return True
    return page_service.can_user_read_page(entry.page, user, user_roles)


def save_navigation_entries(entries: dict, parent: NavigationEntry | None):
    position = 1

    for dict_entry in entries:
        entry_id = dict_entry["id"]
        entry = model_service.find_by_id(NavigationEntry, entry_id)
        if entry is None:
            _logger.error("Could not save NavigationOrderEntry with id %d", entry_id)
            continue
        entry.parent_id = parent.id if parent else None
        entry.position = position

        save_navigation_entries(dict_entry["children_data"], entry)

        position += 1

        db.session.add(entry)
        db.session.commit()


def delete_navigation_entry(entry: NavigationEntry, remove_page: bool) -> None:
    if entry.children:
        raise BusinessRuleException("This entry still has children")

    if remove_page:
        if entry.external or entry.activity_list or not entry.page_id:
            raise BusinessRuleException("This entry does no refer to a page.")
        # When deleting the page, the navigation entry is also removed.
        page_service.delete_page(entry.page)
    else:
        navigation_repository.remove_by_id(entry.id)


def set_entry_type(
    entry: NavigationEntry,
    type: Literal["page", "url", "activities"],
    page_id: int | None,
    url: str | None,
    external: bool | None,
) -> None:
    if type == "page":
        if not page_id:
            raise ValidationException("Missing page id for navigation of type page")
        page = model_service.get_by_id(Page, page_id)
        entry.page_id = page.id
        entry.external = False
        entry.activity_list = False
    elif type == "url":
        if not url:
            raise ValidationException("Missing url for navigation of type url")
        proposed_url = get_stripped_path(url, depth=0)
        nav_entry = find_entry_by_path(proposed_url)
        if nav_entry and nav_entry.id != entry.id:
            raise ValidationException("This URL is already in use")
        entry.url = proposed_url
        entry.external = external
        entry.activity_list = False
        entry.page_id = None
    elif type == "activities":
        entry.external = False
        entry.activity_list = True
        entry.order_children_alphabetically = False
        entry.url = get_stripped_path(url_for("activity.view"), depth=0)
        entry.page_id = None


def create_entry(  # "page" is for site-created pages, "url" for programmed pages,
    # and "activities" for pages whose children are activities.
    type: Literal["page", "url", "activities"],
    nl_title: str,
    en_title: str,
    parent_id: int = None,
    page_id: int = None,
    url: str = None,
    external: bool = False,
    order_children_alphabetically: bool = False,
):
    entry = NavigationEntry()
    entry.parent_id = parent_id
    entry.nl_title = nl_title
    entry.en_title = en_title
    entry.order_children_alphabetically = order_children_alphabetically

    set_entry_type(entry, type, page_id, url, external)

    if parent_id:
        parent = get_entry_by_id(parent_id)

        # Do not use the get_children method, as we do not want to filter any entries.
        entry.position = len(parent.children) + 1
    else:
        entry.position = len(get_navigation_root_entries(children_depth=0)) + 1

    navigation_repository.save(entry)
    return entry


def update_entry(
    entry: NavigationEntry,
    type: Literal["page", "url", "activities"],
    nl_title: str,
    en_title: str,
    page_id: int = None,
    url: str = None,
    external: bool = False,
    order_children_alphabetically: bool = False,
):
    entry.nl_title = nl_title
    entry.en_title = en_title
    entry.order_children_alphabetically = order_children_alphabetically

    set_entry_type(entry, type, page_id, url, external)

    navigation_repository.save(entry)
    return entry
