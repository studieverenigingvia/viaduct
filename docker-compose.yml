services:
    frontend:
        user: ${UID:-1000}:${GID:-1000}
        build:
            context: .
            dockerfile: docker/Dockerfile
            target: frontend-dev
        ports:
            - "5173:5173"
        volumes:
            - .:/app
    backend:
        build:
            context: .
            dockerfile: docker/Dockerfile
            target: backend-dev
        user: ${UID:-1000}:${GID:-1000}
        command: bash /app/docker/entrypoint.development.sh
        volumes:
            - .:/app
        ports:
            - "5000:5000"
        environment:
            - SQLALCHEMY_DATABASE_URI=postgresql+psycopg2://viaduct:viaduct@db/viaduct
            - SQLALCHEMY_DATABASE_TEST_URI=postgresql+psycopg2://viaduct:viaduct@db/viaduct_test
            - BROKER_URL=redis://redis
            - AUTHLIB_INSECURE_TRANSPORT=true
            - PYTHONUNBUFFERED=1
            - S3_ENDPOINT=minio:9000
            - AWS_ACCESS_KEY_ID=viaduct
            - AWS_SECRET_ACCESS_KEY=viaduct123
            - AWS_PRIVATE_ACCESS_KEY_ID=viaduct
            - AWS_PRIVATE_SECRET_ACCESS_KEY=viaduct123
            - USE_HTTPS=false
        depends_on:
            - db
            - redis
            - minio

    db:
        image: postgres:10.6
        volumes:
            - db:/var/lib/postgresql/data
            - ./docker/postgresql/entrypoint.sh:/custom-entrypoint.sh
            - ./docker/postgresql/load-extensions.sh:/docker-entrypoint-initdb.d/load-extensions.sh

        entrypoint: /custom-entrypoint.sh
        command: ["postgres"]

        ports:
            - "5433:5432"
        environment:
            POSTGRES_USER: viaduct
            POSTGRES_DB: viaduct

    adminer:
        image: adminer:latest
        ports:
            - "8201:8080"
        volumes:
            - ./docker/adminer-uploads.ini:/usr/local/etc/php/conf.d/uploads.ini
            - ./docker/adminer-fill-login-form.php:/var/www/html/plugins-enabled/fill-login-form.php

        environment:
            - ADMINER_PLUGINS=tables-filter
            - ADMINER_LOGIN_FORM_SERVER=db
            - ADMINER_LOGIN_FORM_USER=viaduct
            - ADMINER_LOGIN_FORM_PASSWORD=viaduct
            - ADMINER_LOGIN_FORM_DB=viaduct

    redis:
        image: redis:5.0.3
        ports:
            - "6379:6379"

    worker:
        build:
            context: .
            dockerfile: docker/Dockerfile
            target: backend-dev
        command: "celery --app worker:worker worker -l info --beat --hostname=celery@%h"
        hostname: worker
        user: ${UID:-1000}:${GID:-1000}
        volumes:
            - .:/app
        environment:
            - SQLALCHEMY_DATABASE_URI=postgresql+psycopg2://viaduct:viaduct@db/viaduct
            - BROKER_URL=redis://redis
            - C_FORCE_ROOT=true
        depends_on:
            - redis
            - db

    minio:
        image: minio/minio
        entrypoint: sh
        command: -c 'mkdir -p /data/viaduct-files && mkdir -p /data/viaduct-private-files && /usr/bin/minio server /data'
        ports:
            - "9000:9000"
        # It is necessary to add this to your /etc/hosts or similar
        # 127.0.0.1 minio
        # Flask will use the docker container as an endpoint, but your browser will not recognize it
        environment:
            MINIO_ACCESS_KEY: viaduct
            MINIO_SECRET_KEY: viaduct123
        volumes:
            - minio_data:/data

    manticore:
        build:
            context: docker/manticore
            dockerfile: Dockerfile
        volumes:
            - manticore_search_data:/var/lib/manticore/data
            - manticore_search_logs:/var/lib/manticore/log
            - ./docker/manticore/manticore.conf:/etc/manticoresearch/manticore.conf
        ports:
            - "9306:9306"
            - "9308:9308"
        environment:
            POSTGRES_USER: viaduct
            POSTGRES_DB: viaduct
            POSTGRES_PASS: viaduct
            POSTGRES_HOST: db

volumes:
    db:
    manticore_search_data:
    manticore_search_logs:
    minio_data:
