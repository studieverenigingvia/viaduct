import "@/mocks/matchMedia";
import { vi } from "vitest";
import { config } from "@vue/test-utils";
import { i18n } from "../translations";
import { getCookie } from "../utils/axios";

vi.stubGlobal("vi", vi);

const Flask = {
    url_for: vi.fn(),
};

vi.stubGlobal("Flask", Flask);
vi.mock("../scripts/api/user.ts");

vi.mock("axios", () => ({
    default: {
        create: () => {
            return {
                get: () => ({
                    data: [],
                }),
                post: vi.fn(),
            };
        },
    },
    getCookie: () => "",
}));

if (!globalThis.defined) {
    config.global.plugins = [i18n];
    globalThis.defined = true;
}
