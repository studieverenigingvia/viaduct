import { render } from "@testing-library/vue";
import EditNews from "./edit_news.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(EditNews.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getAllByText } = render(EditNews, {
            global: {
                plugins: [router],
            },
            provide: { locale: "en" },
        });

        expect(getAllByText(/Create news/)).toHaveLength(2);
    });
});
