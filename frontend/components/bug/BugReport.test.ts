import { render } from "@testing-library/vue";
import BugReport from "./BugReport.vue";
import { router } from "../../router";
import flushPromises from "flush-promises";

vi.mock("../../utils/flask.ts");

vi.mock("../../scripts/api/schema.ts", () => {
    return {
        schemaApi: {
            getSchema: vi.fn(() =>
                Promise.resolve({
                    data: {
                        title: "BugReportRequest",
                        type: "object",
                        properties: {
                            project: {
                                title: "Project",
                                default: "viaduct",
                                enum: ["viaduct", "pretix", "pos"],
                                type: "string",
                            },
                            title: { title: "Title", type: "string" },
                            description: {
                                title: "Description",
                                format: "textarea",
                                type: "string",
                            },
                        },
                        required: ["title", "description"],
                    },
                })
            ),
        },
    };
});

const consoleSpy = vi.spyOn(console, "error");

describe(BugReport.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getAllByText } = render(BugReport, {
            global: {
                plugins: [router],
                provide: { locale: "en" },
            },
        });

        await flushPromises();
        expect(getAllByText(/Report a bug/)).toHaveLength(2);
        expect(getAllByText(/Project/)).toHaveLength(1);
        expect(getAllByText(/Title/)).toHaveLength(1);
        expect(getAllByText(/Description/)).toHaveLength(1);
    });
});
