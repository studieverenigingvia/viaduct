import { render } from "@testing-library/vue";
import EditExamination from "./edit_examination.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(EditExamination.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(EditExamination, {
            global: {
                plugins: [router],
            },
        });

        getByText("Create examination");
    });
});
