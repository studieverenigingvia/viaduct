import { render } from "@testing-library/vue";
import CreateCourse from "./create_course.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(CreateCourse.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(CreateCourse, {
            global: {
                plugins: [router],
            },
        });

        getByText("Load course using course catalog number");
    });
});
