import { render } from "@testing-library/vue";
import EducationOverview from "./education_overview.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(EducationOverview.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(EducationOverview, {
            global: {
                plugins: [router],
            },
        });

        getByText("Study programmes overview");
    });
});
