import { render } from "@testing-library/vue";
import CourseOverview from "./course_overview.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(CourseOverview.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getAllByText } = render(CourseOverview, {
            global: {
                plugins: [router],
            },
        });

        expect(getAllByText(/Course overview/)).toHaveLength(2);
    });
});
