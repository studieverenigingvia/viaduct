import { render } from "@testing-library/vue";
import EditMeeting from "./edit_meeting.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(EditMeeting.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(EditMeeting, {
            global: {
                plugins: [router],
            },
        });

        getByText("Create meeting");
    });
});
