import { render } from "@testing-library/vue";
import Declaration from "./declaration.vue";
import flushPromises from "flush-promises";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");
vi.mock("../../scripts/api/page.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(Declaration.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(Declaration, {
            global: {
                plugins: [router],
                provide: { locale: "en" },
            },
        });

        await flushPromises();

        getByText("declaration en title");
        getByText("declaration en content");
        getByText("declaration en title");
        getByText("declaration en content");
    });
});
