import { render } from "@testing-library/vue";
import GroupRoles from "./group_roles.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");
vi.mock("../../utils/axios.ts", () => ({
    default: {
        get: async () => {
            return {
                data: {
                    options: [],
                    roles: [
                        {
                            id: 1,
                            name: "admin",
                            permissions: [
                                {
                                    id: 1,
                                    name: "admin",
                                },
                            ],
                        },
                        {
                            id: 2,
                            name: "user",
                            permissions: [
                                {
                                    id: 2,
                                    name: "user",
                                },
                            ],
                        },
                    ],
                },
            };
        },
    },
    getCookie: () => "",
}));

const consoleSpy = vi.spyOn(console, "error");

describe(GroupRoles.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(GroupRoles, {
            global: {
                plugins: [router],
            },
        });

        getByText("Group roles");
    });
});
