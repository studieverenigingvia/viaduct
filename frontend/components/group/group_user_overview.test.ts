import { render } from "@testing-library/vue";
import GroupUserOverview from "./group_user_overview.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(GroupUserOverview.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getAllByText } = render(GroupUserOverview, {
            global: {
                plugins: [router],
            },
        });

        expect(getAllByText(/Users/)).toHaveLength(2);
    });
});
