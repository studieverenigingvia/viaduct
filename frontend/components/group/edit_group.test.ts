import { render } from "@testing-library/vue";
import EditGroup from "./edit_group.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(EditGroup.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(EditGroup, {
            global: {
                plugins: [router],
            },
        });

        getByText("Create group");
    });
});
