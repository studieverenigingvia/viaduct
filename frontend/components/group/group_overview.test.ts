import { render } from "@testing-library/vue";
import GroupOverview from "./group_overview.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(GroupOverview.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(GroupOverview, {
            global: {
                plugins: [router],
            },
        });

        getByText("Group overview");
    });
});
