import { render } from "@testing-library/vue";
import EditAlv from "./edit_alv.vue";

vi.mock("../../utils/flask.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(EditAlv.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(EditAlv, {
            provide: { locale: "en" },
        });

        getByText("Create general assembly");
    });
});
