import { render } from "@testing-library/vue";
import UserOAuthApplications from "./oauth_applications.vue";
import flushPromises from "flush-promises";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");
vi.mock("../../scripts/api/oauth.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(UserOAuthApplications.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(UserOAuthApplications, {
            global: {
                plugins: [router],
            },
            mocks: {
                $route: {
                    params: {},
                },
            },
        });
        await flushPromises();
        getByText("Connected applications");
    });
});
