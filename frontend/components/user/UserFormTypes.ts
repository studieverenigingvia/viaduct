import moment from "moment";

export type SignupFormValues = {
    email: string;
    first_name: string;
    last_name: string;
    birth_date: moment.Moment;
    address: string;
    zip: string;
    city: string;
    country: string;
    student_id: string;
    educations: number[];
    recaptcha: string;
    agree_with_privacy_policy: boolean;
};

export type SignupManualFormValues = {
    password: string;
} & SignupFormValues;

export type ProfileFormValues = Omit<
    SignupFormValues,
    "recaptcha" | "agree_with_privacy_policy"
> & {
    phone_nr: string;
    alumnus: boolean;
    locale: string;
};

export type UserFormValues =
    | SignupFormValues
    | SignupManualFormValues
    | ProfileFormValues;

export function convertToFormData(values: UserFormValues): FormData {
    const formData = new FormData();
    formData.append("csrf_token", window.viaduct.csrf_token);

    Object.entries(values).forEach(([key, value]) => {
        if (key === "birth_date") {
            // @ts-expect-error value will always be a moment object (or a string)
            formData.append(key, moment(value).format("YYYY-MM-DD"));
        } else if (key === "educations") {
            // @ts-expect-error value will always be an array
            value.forEach((education) => {
                formData.append(`educations`, education);
            });
        } else if (key === "agree_with_privacy_policy") {
            formData.append(key, value.toString());
        } else if (key === "recaptcha") {
            // @ts-expect-error value will always be a string
            formData.append("g-recaptcha-response", value);
        } else {
            // @ts-expect-error value will always be a string
            formData.append(key, value);
        }
    });
    return formData;
}
