import { render } from "@testing-library/vue";
import UserEditAdminDataForm from "./edit_admin_data.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(UserEditAdminDataForm.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(UserEditAdminDataForm, {
            global: {
                plugins: [router],
            },
        });

        getByText("User administrator properties");
    });
});
