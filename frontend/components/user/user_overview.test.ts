import { render } from "@testing-library/vue";
import UserOverview from "./user_overview.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");
vi.mock("../../scripts/api/user.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(UserOverview.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(UserOverview, {
            global: {
                plugins: [router],
            },
        });

        getByText("User overview");
    });
});
