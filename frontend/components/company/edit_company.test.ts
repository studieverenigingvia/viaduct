import { render } from "@testing-library/vue";
import EditCompanyForm from "./edit_company.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(EditCompanyForm.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(EditCompanyForm, {
            global: {
                plugins: [router],
            },
        });

        getByText("Create company");
    });
});
