import { render } from "@testing-library/vue";
import EditCompanyJobForm from "./edit_job.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(EditCompanyJobForm.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(EditCompanyJobForm, {
            global: {
                plugins: [router],
            },
        });

        getByText("Create company job");
    });
});
