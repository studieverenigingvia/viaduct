import { render } from "@testing-library/vue";
import EditBanner from "./edit_banner.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(EditBanner.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(EditBanner, {
            global: {
                plugins: [router],
            },
        });

        getByText("Create company banner");
    });
});
