import { render } from "@testing-library/vue";
import EditCompanyProfileForm from "./edit_profile.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(EditCompanyProfileForm.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(EditCompanyProfileForm, {
            global: {
                plugins: [router],
            },
        });

        getByText("Create company profile");
    });
});
