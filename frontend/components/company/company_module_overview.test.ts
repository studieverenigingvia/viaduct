import { render } from "@testing-library/vue";
import CompanyModuleOverview from "./company_module_overview.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(CompanyModuleOverview.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(CompanyModuleOverview, {
            global: {
                plugins: [router],
            },
        });

        getByText("Jobs");
        getByText("Banner");
        getByText("Page");
    });
});
