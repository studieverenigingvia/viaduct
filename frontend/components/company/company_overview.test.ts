import { render } from "@testing-library/vue";
import CompanyOverview from "./company_overview.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(CompanyOverview.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getAllByText } = render(CompanyOverview, {
            global: {
                plugins: [router],
            },
        });

        expect(getAllByText(/Partners/)).toHaveLength(2);
    });
});
