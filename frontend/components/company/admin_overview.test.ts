import { render } from "@testing-library/vue";
import AdminOverview from "./admin_overview.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(AdminOverview.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(AdminOverview, {
            global: {
                plugins: [router],
            },
        });

        getByText("Manage companies");
    });
});
