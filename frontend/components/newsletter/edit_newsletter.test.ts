import { render } from "@testing-library/vue";
import EditNewsletter from "./edit_newsletter.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");
vi.mock("../../scripts/api/news");

const consoleSpy = vi.spyOn(console, "error");

describe(EditNewsletter.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(EditNewsletter, {
            global: {
                plugins: [router],
            },
        });

        getByText("Create newsletter");
    });
});
