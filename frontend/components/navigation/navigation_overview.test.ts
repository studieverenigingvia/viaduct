import { render } from "@testing-library/vue";
import NavigationOverview from "./navigation_overview.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(NavigationOverview.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(NavigationOverview, {
            global: {
                plugins: [router],
                provide: { locale: "en" },
            },
        });

        getByText("Navigation overview");
    });
});
