import { render } from "@testing-library/vue";
import EditNavigation from "./edit_navigation.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(EditNavigation.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(EditNavigation, {
            global: {
                plugins: [router],
            },
        });

        getByText("Create navigation entry");
    });
});
