import { render } from "@testing-library/vue";
import TutorOverview from "./tutors_overview.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");
vi.mock("../../scripts/api/page.ts");
vi.mock("../../scripts/api/tutoring.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(TutorOverview.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(TutorOverview, {
            global: {
                plugins: [router],
            },
        });

        getByText("Tutor overview");
    });
});
