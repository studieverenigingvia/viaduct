import { render } from "@testing-library/vue";
import TutorCourseRegister from "./tutor_course_register.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(TutorCourseRegister.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(TutorCourseRegister, {
            global: {
                plugins: [router],
            },
        });

        getByText("Tutor registration");
    });
});
