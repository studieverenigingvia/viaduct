import { render } from "@testing-library/vue";
import TutorsProfile from "./tutors_profile.vue";
import { router } from "../../router";
import flushPromises from "flush-promises";

vi.mock("../../utils/flask.ts");
vi.mock("../../scripts/api/page.ts");
vi.mock("../../scripts/api/tutoring.ts");
vi.mock("../../scripts/api/course.ts");
vi.mock("../../scripts/api/user.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(TutorsProfile.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(TutorsProfile, {
            global: {
                plugins: [router],
                provide: { locale: "en" },
            },
        });
        await flushPromises();
        getByText("tutors/self en content");
        getByText("Advanced Networking tutor course");
    });
});
