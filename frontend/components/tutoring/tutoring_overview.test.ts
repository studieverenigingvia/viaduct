import { render } from "@testing-library/vue";
import TutoringOverview from "./tutoring_overview.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");
vi.mock("../../scripts/api/page.ts");

import flushPromises from "flush-promises";

const consoleSpy = vi.spyOn(console, "error");

describe(TutoringOverview.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(TutoringOverview, {
            global: {
                plugins: [router],
                provide: { locale: "en" },
            },
        });
        await flushPromises();
        getByText("tutoring en content");
    });
});
