import { render } from "@testing-library/vue";
import ContestEditForm from "./ContestEditForm.vue";
import { router } from "../../router";

const consoleSpy = vi.spyOn(console, "error");

describe(ContestEditForm.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(ContestEditForm, {
            global: {
                plugins: [router],
            },
        });

        getByText("DOMjudge");
    });
});
