import { render } from "@testing-library/vue";
import SearchIndex from "./search_index.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(SearchIndex.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(SearchIndex, {
            global: {
                plugins: [router],
                provide: { locale: "en" },
            },
        });

        getByText("Search");
    });
});
