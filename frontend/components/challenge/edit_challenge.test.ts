import { render } from "@testing-library/vue";
import EditChallenge from "./edit_challenge.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(EditChallenge.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getAllByText } = render(EditChallenge, {
            global: {
                plugins: [router],
            },
        });

        expect(getAllByText("Create challenge")).toHaveLength(2);
    });
});
