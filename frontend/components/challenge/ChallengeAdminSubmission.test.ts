import { render } from "@testing-library/vue";
import ChallengeAdminSubmission from "./ChallengeAdminSubmission.vue";
import { router } from "../../router";
import flushPromises from "flush-promises";

vi.mock("../../utils/flask.ts");

vi.mock("../../scripts/api/schema.ts", () => {
    return {
        schemaApi: {
            getSchema: vi.fn(() =>
                Promise.resolve({
                    data: {
                        title: "ChallengeAdminSubmission",
                        type: "object",
                        properties: {
                            user_id: {
                                title: "User Id",
                                exclusiveMinimum: 0,
                                format: "user",
                                type: "integer",
                            },
                        },
                        required: ["user_id"],
                    },
                })
            ),
        },
    };
});

const consoleSpy = vi.spyOn(console, "error");

describe(ChallengeAdminSubmission.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getAllByText } = render(ChallengeAdminSubmission, {
            global: {
                plugins: [router],
                provide: { locale: "en" },
            },
        });

        await flushPromises();
        expect(getAllByText(/Manual solution/)).toHaveLength(1);
        expect(getAllByText(/Add manual submission/)).toHaveLength(1);
        expect(getAllByText(/Select user/)).toHaveLength(1);
    });
});
