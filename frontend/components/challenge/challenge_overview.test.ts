import { render } from "@testing-library/vue";
import Challenges from "./challenge_overview.vue";
import { router } from "../../router";
import flushPromises from "flush-promises";

vi.mock("../../utils/flask.ts");
vi.mock("../../scripts/api/page.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(Challenges.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(Challenges, {
            global: {
                plugins: [router],
                provide: { locale: "en" },
            },
        });
        await flushPromises();

        getByText("Ranking");
        getByText("challenge en title");
        getByText("challenge en content");
    });
});
