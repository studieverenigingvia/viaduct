import WtformDateField from "./wtform_date_picker.vue";
import { fireEvent, render } from "@testing-library/vue";
import { createLocalVue } from "@vue/test-utils";
import Antd from "ant-design-vue";

Date.now = vi.fn(() => new Date(Date.UTC(2017, 7, 9)).valueOf());

describe(WtformDateField.name + " component", () => {
    test("timezone is UTC", () => {
        expect(process.env.TZ).toBe("UTC");
        expect(new Date().getTimezoneOffset()).toBe(0);
    });
    test("renders correctly", async () => {
        const { getByLabelText, getByText } = render(WtformDateField, {
            props: {
                errors: [],
                value: "2013-10-05",
                format: "YYYY-MM-DD",
                required: true,
                label: "label",
                name: "name",
                id: "id",
            },
        });
        const input = getByLabelText("label");
        expect(input).toBeInstanceOf(HTMLInputElement);
        await fireEvent.click(input);
        const button = getByText("Today");
        await fireEvent.click(button);
        expect((input as HTMLInputElement).value).toBe("2017-08-09");
    });
    test("renders value in different format correctly in input", async () => {
        const { getByLabelText } = render(WtformDateField, {
            props: {
                errors: [],
                value: "2013-10-05 00:00:00",
                format: "YYYY-MM-DD",
                required: true,
                label: "label",
                name: "name",
                id: "id",
            },
        });
        const input = getByLabelText("label");
        expect(input).toBeInstanceOf(HTMLInputElement);
        expect((input as HTMLInputElement).value).toBe("2013-10-05");
    });
});
