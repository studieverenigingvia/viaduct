import { render } from "@testing-library/vue";
import PimpyMinutes from "./pimpy_minutes.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(PimpyMinutes.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(PimpyMinutes, {
            global: {
                plugins: [router],
            },
        });

        getByText("Minutes");
    });
});
