import { render } from "@testing-library/vue";
import PimpyBase from "./pimpy_base.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(PimpyBase.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(PimpyBase, {
            global: {
                plugins: [router],
            },
            props: { type: "tasks" },
        });

        getByText("Show");
    });
});
