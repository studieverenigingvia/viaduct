import { render } from "@testing-library/vue";
import CreateTask from "./create_task.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(CreateTask.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(CreateTask, {
            global: {
                plugins: [router],
            },
        });

        getByText("Create task");
    });
});
