import { render } from "@testing-library/vue";
import { date } from "../../scripts/formatters";
import PimpySingleMinute from "./pimpy_single_minute.vue";

vi.mock("../../utils/flask.ts");
vi.mock("../../scripts/api/pimpy.ts");

import { i18n } from "../../translations";
import { router } from "../../router";

const consoleSpy = vi.spyOn(console, "error");

describe(PimpySingleMinute.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(PimpySingleMinute, {
            global: {
                plugins: [router],
            },
            props: { userGroups: [] },
        });

        getByText("Minute");
    });
});
