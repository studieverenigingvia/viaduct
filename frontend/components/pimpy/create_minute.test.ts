import { render } from "@testing-library/vue";
import CreateMinute from "./create_minute.vue";

import { router } from "../../router";

const consoleSpy = vi.spyOn(console, "error");

describe(CreateMinute.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(CreateMinute, {
            global: {
                plugins: [router],
            },
        });

        getByText("New minute");
    });
});
