import { render } from "@testing-library/vue";
import EditPage from "./edit_page.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(EditPage.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getAllByText } = render(EditPage, {
            global: {
                plugins: [router],
            },
        });

        expect(getAllByText(/Create page/)).toHaveLength(2);
    });
});
