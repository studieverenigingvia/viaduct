import { render } from "@testing-library/vue";
import PageOverview from "./page_overview.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(PageOverview.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(PageOverview, {
            global: {
                plugins: [router],
            },
        });

        getByText("Page overview");
    });
});
