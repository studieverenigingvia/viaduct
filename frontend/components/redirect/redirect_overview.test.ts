import { render } from "@testing-library/vue";
import RedirectOverview from "./redirect_overview.vue";
import { router } from "../../router";

vi.mock("../../utils/flask.ts");

const consoleSpy = vi.spyOn(console, "error");

describe(RedirectOverview.name + " component", () => {
    afterEach(() => {
        expect(consoleSpy).toBeCalledTimes(0);
    });
    test("renders correctly", async () => {
        const { getByText } = render(RedirectOverview, {
            global: {
                plugins: [router],
            },
        });

        getByText("Redirects");
    });
});
