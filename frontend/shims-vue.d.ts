// This is needed to make typescript not complain about the $refs property.
// Mainly a workaround for the image-cropper-modal.
import * as runtimeCore from "vue";

declare module "@vue/runtime-core" {
    interface ComponentCustomProperties {
        $refs: {
            [key: string]: HTMLElement | any;
        };
    }
}
