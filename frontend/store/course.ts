import { Course } from "../scripts/api/examination";
import { courseApi } from "../scripts/api/course";
import { Tutoring } from "../scripts/api/tutoring";

class CourseStore {
    private courses: { [courseId: number]: Course } = {};

    async getCourse(courseId: number) {
        if (this.courses[courseId]) {
            return this.courses[courseId];
        } else {
            const course = (await courseApi.getCourse(courseId)).data;
            this.courses[course.id] = course;
            return course;
        }
    }

    async loadCourses(tutorings: Tutoring[]) {
        const courseIds = new Set(
            tutorings.map((tutoring) => tutoring.course_id)
        );

        return await Promise.all(
            [...courseIds].reduce((result, value) => {
                result.push(this.getCourse(value));
                return result;
            }, [] as Promise<Course>[])
        );
    }
}

export const courseStore = new CourseStore();
