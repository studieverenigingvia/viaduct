import { Api, PaginatedResponse } from "./api";
import { AxiosPromise } from "axios";
import { MultilangString } from "../../types/page";
import { Activity } from "./activity";
import Flask from "../../utils/flask";

export class NewsItemBody {
    title: MultilangString;
    content: MultilangString;
    publish_date: string;
    sponsored: boolean;

    constructor(
        title_en: string,
        title_nl: string,
        content_en: string,
        content_nl: string,
        publish_date: string,
        sponsored: boolean
    ) {
        this.title = new MultilangString(title_en, title_nl);
        this.content = new MultilangString(content_en, content_nl);
        this.publish_date = publish_date;
        this.sponsored = sponsored;
    }
}

export class NewsItem extends NewsItemBody {
    id: number;
}

type NewsletterResponseEntry<K> = {
    obj: K;
    overwrite_text: MultilangString;
};

type NewsletterRequestEntry = {
    item_id: number;
    overwrite_text: MultilangString;
};

export type NewsletterResponse = {
    id: number;
    created?: string;
    news: NewsletterResponseEntry<NewsItem>[];
    activities: NewsletterResponseEntry<Activity>[];
    greetings: MultilangString[];
};

class NewsApi extends Api {
    getInitialNewsletter(): AxiosPromise<NewsletterResponse> {
        return this.get<NewsletterResponse>(
            Flask.url_for("api.newsletter.initial")
        );
    }

    getNewsletter(newsletterId: number): AxiosPromise<NewsletterResponse> {
        return this.get<NewsletterResponse>(
            Flask.url_for("api.newsletter", { newsletter: newsletterId })
        );
    }

    createNewsletter(
        news: NewsletterRequestEntry[],
        activities: NewsletterRequestEntry[],
        greetings: MultilangString[]
    ) {
        return this.post<NewsletterResponse>(Flask.url_for("api.newsletters"), {
            news,
            activities,
            greetings,
        });
    }

    async updateNewsletter(
        newsletterId: number,
        news: NewsletterRequestEntry[],
        activities: NewsletterRequestEntry[],
        greetings: MultilangString[]
    ) {
        return this.put<NewsletterResponse>(
            Flask.url_for("api.newsletter", {
                newsletter: newsletterId,
            }),
            {
                news: news,
                activities: activities,
                greetings,
            }
        );
    }

    searchNews(
        page: number,
        search: string
    ): AxiosPromise<PaginatedResponse<NewsItem>> {
        return this.get<PaginatedResponse<NewsItem>>(
            Flask.url_for("api.news", {
                page: page,
                limit: 15,
                search: search,
            })
        );
    }

    getNewsItem(id: number): AxiosPromise<NewsItem> {
        return this.get<NewsItem>(Flask.url_for("api.news_item", { news: id }));
    }

    createNewsItem(data: NewsItemBody): AxiosPromise<NewsItem> {
        return this.post<NewsItem>(Flask.url_for("api.news"), data);
    }

    updateNewsItem(id: number, data: NewsItemBody): AxiosPromise<NewsItem> {
        return this.put<NewsItem>(
            Flask.url_for("api.news_item", { news: id }),
            data
        );
    }

    deleteNewsItem(id: number) {
        return this.delete(Flask.url_for("api.news_item", { news: id }));
    }

    async deleteNewsletter(newsletterId: string) {
        return this.delete(
            Flask.url_for("api.newsletter", { newsletter: newsletterId })
        );
    }
}

export const newsApi = new NewsApi();
