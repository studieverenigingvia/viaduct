import { Api, PaginatedResponse } from "./api";
import { AxiosPromise } from "axios";
import Flask from "../../utils/flask";
import { Education } from "../../types/examination";

class EducationApi extends Api {
    getUserEducations(userId: number | null = null): AxiosPromise<Education[]> {
        return this.get<Education[]>(
            Flask.url_for("api.user.educations", {
                user: userId ? userId : "self",
            })
        );
    }
    getEducations(
        search: string,
        page: string
    ): AxiosPromise<PaginatedResponse<Education>> {
        return this.get<PaginatedResponse<Education>>(
            Flask.url_for("api.educations", {
                search: search,
                page: page,
                limit: 50,
            })
        );
    }
}

export const educationApi = new EducationApi();
