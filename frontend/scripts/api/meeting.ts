import { Api } from "./api";
import { AxiosPromise } from "axios";
import Flask from "../../utils/flask";

export type Meeting = {
    id?: number;
    name: string;
    description: string;
    group_id: number;
    start_time: string;
    end_time: string;
    location: string;
    cancelled: boolean;
};

class MeetingApi extends Api {
    getMeeting(meetingId: number): AxiosPromise<Meeting> {
        return this.get<Meeting>(
            Flask.url_for("api.meeting", { meeting: meetingId })
        );
    }

    createMeeting(values: Meeting): AxiosPromise<Meeting> {
        return this.post<Meeting>(Flask.url_for("api.meetings"), values);
    }

    updateMeeting(meetingId: number, values: Meeting): AxiosPromise<Meeting> {
        return this.put<Meeting>(
            Flask.url_for("api.meeting", { meeting: meetingId }),
            values
        );
    }

    deleteMeeting(meetingId: number): AxiosPromise<void> {
        return this.delete<void>(
            Flask.url_for("api.meeting", { meeting: meetingId })
        );
    }
}

export const meetingApi = new MeetingApi();
