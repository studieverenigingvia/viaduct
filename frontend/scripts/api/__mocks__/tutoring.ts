export const tutoringApi = {
    getTutorTutorings: vi.fn(() => ({ data: [] })),
    getOpenTutorings: vi.fn(() => ({
        data: [
            {
                id: 69,
                num_hours: 0,
                course_id: 74,
                notes: "",
                tutor: null,
                tutee: {
                    first_name: "dummy",
                    last_name: "van via",
                    email: "dummy@svia.nl",
                    phone_nr: "0612345678",
                },
            },
        ],
    })),
    getTutorCourses: vi.fn(() => ({
        data: [
            {
                id: 28,
                grade: 70.0,
                course: {
                    id: 74,
                    name: "Advanced Networking tutor course",
                    datanose_code: "5384ADNE6Y",
                },
                user: {
                    id: 69,
                    first_name: "John",
                    last_name: "Doe",
                },
                approved: true,
            },
        ],
    })),
    getTutors: vi.fn(() => ({
        data: {
            page: 1,
            page_size: 10,
            page_count: 1,
            total: 1,
            data: [
                {
                    id: 28,
                    grade: 70.0,
                    course: {
                        id: 74,
                        name: "Advanced Networking",
                        datanose_code: "5384ADNE6Y",
                    },
                    user: {
                        id: 69,
                        first_name: "John",
                        last_name: "Doe",
                    },
                    approved: true,
                },
            ],
        },
    })),
};
