export const oAuthApi = {
    getUserSelfApplications: vi.fn(() => ({
        data: [
            {
                client_id: "telegram",
                client_name: "Studievereniging via Telegram bot",
                user: {
                    id: 1,
                    first_name: "John",
                    last_name: "Doe",
                },
            },
        ],
    })),
};
