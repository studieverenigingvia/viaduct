import { vi } from "vitest";

export const pageApi = {
    renderPage: vi.fn((page, locale) => ({
        data: {
            title: `${page} ${locale} title`,
            content: `${page} ${locale} content `,
        },
    })),
};
