export const newsApi = {
    getNewsletter: vi.fn(() => ({
        data: {
            id: 210,
            created: "2024-06-24T09:01:21.292278+00:00",
            news: [
                {
                    obj: {
                        id: 586,
                        modified: "2024-06-27T13:52:59.959527+00:00",
                        title: {
                            nl: "Wordt studentenvoorlichter bij Informatica!",
                            en: "Become a student informer!",
                        },
                        content: {
                            nl: "asd",
                            en: "asd",
                        },
                        publish_date: "2024-06-24",
                        sponsored: false,
                    },
                    overwrite_text: {
                        modified: "2024-08-12T19:53:11.723670+00:00",
                        nl: "",
                        en: "",
                    },
                },
            ],
            activities: [
                {
                    obj: {
                        id: 1424,
                        modified: "2024-06-25T10:06:02.711973+00:00",
                        name: {
                            nl: "Pre-FLUX buitenborrel",
                            en: "Pre-FLUX drinks outside",
                        },
                        description: {
                            nl: "",
                            en: "",
                        },
                        start_time: "2024-06-27T15:00:00.779000+00:00",
                        end_time: "2024-06-27T19:30:11.779000+00:00",
                        location:
                            "https://maps.google.com/?q=Veldje%20naast%20de%20fietsenstalling",
                        price: "",
                        picture_file_id: 5273,
                        pretix_event_slug: "",
                    },
                    overwrite_text: {
                        modified: "2024-08-12T19:53:11.723670+00:00",
                        nl: "",
                        en: "",
                    },
                },
            ],
            greetings: [],
        },
    })),
    getInitialNewsletter: vi.fn(() => ({
        data: {
            news: [
                {
                    obj: {
                        id: 568,
                        modified: "2024-08-12T19:56:19.223481+00:00",
                        title: {
                            nl: "sad",
                            en: "asd",
                        },
                        content: {
                            nl: "asd",
                            en: "asd",
                        },
                        publish_date: "2024-08-12",
                        sponsored: false,
                    },
                    overwrite_text: {
                        nl: "",
                        en: "",
                    },
                },
            ],
            activities: [
                {
                    obj: {
                        id: 1377,
                        modified: "2024-08-12T18:05:04.966220+00:00",
                        name: {
                            nl: "asad",
                            en: "asd",
                        },
                        description: {
                            nl: "asd",
                            en: "asd",
                        },
                        start_time: "2024-09-17T17:59:11.123000+00:00",
                        end_time: "2024-09-19T17:59:11.123000+00:00",
                        location: "https://maps.google.com/?q=asd",
                        price: "",
                        picture_file_id: 5145,
                        pretix_event_slug: "99s3y",
                    },
                    overwrite_text: {
                        nl: "",
                        en: "",
                    },
                },
            ],
        },
    })),
};
