import { Api } from "./api";
import { AxiosResponse } from "axios";
import Flask from "../../utils/flask";
import { MultilangString } from "../../types/page";

export class PretixEvent {
    name: MultilangString;
    slug: string;
}

class PretixApi extends Api {
    public async searchEvents(
        search: string
    ): Promise<AxiosResponse<PretixEvent[]>> {
        return this.get<PretixEvent[]>(
            Flask.url_for("api.pretix.events", { search: search })
        );
    }
}

export const pretixApi = new PretixApi();
