import { Api, PaginatedResponse } from "./api";
import { AxiosResponse } from "axios";
import { Redirect } from "../../types/redirect";
import Flask from "../../utils/flask";
import instance from "../../utils/axios";

class RedirectApi extends Api {
    public async getAllRedirects(
        search,
        page,
        limit = 10
    ): Promise<AxiosResponse<PaginatedResponse<Redirect>>> {
        return this.get(
            Flask.url_for("api.redirects", {
                search: search,
                page: page,
                limit: limit,
            })
        );
    }

    public async upsertRedirect(
        body: Redirect
    ): Promise<AxiosResponse<Redirect>> {
        return this.put(Flask.url_for("api.redirects"), body);
    }

    public async getRedirect(
        redirectId: string
    ): Promise<AxiosResponse<Redirect>> {
        return this.get(
            Flask.url_for("api.redirect", { redirect: redirectId })
        );
    }

    public async deleteRedirect(redirectId: string) {
        return instance.delete(
            Flask.url_for("api.redirect", { redirect: redirectId })
        );
    }
}

export const redirectApi = new RedirectApi();
