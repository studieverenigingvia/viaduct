import { Api } from "./api";
import { AxiosResponse } from "axios";
import {
    DOMjudgeContest,
    DOMjudgeContestSettings,
    DOMjudgeContestUser,
} from "../../types/domjudge";
import Flask from "../../utils/flask";

class DOMjudgeApi extends Api {
    public async getContest(
        contestId: number
    ): Promise<AxiosResponse<DOMjudgeContest>> {
        return this.get(
            Flask.url_for("api.domjudge.contest", { contest_id: contestId })
        );
    }

    public async setContestBanner(
        contestId: number,
        banner: Blob
    ): Promise<AxiosResponse<void>> {
        const formData = new FormData();
        formData.append("file", banner);
        return this.put(
            Flask.url_for("api.domjudge.contest.banner", {
                contest_id: contestId,
            }),
            formData
        );
    }

    public async deleteContestBanner(
        contestId: number
    ): Promise<AxiosResponse<void>> {
        return this.delete(
            Flask.url_for("api.domjudge.contest.banner", {
                contest_id: contestId,
            })
        );
    }

    public async saveContestSettings(
        contestId: number,
        settings: DOMjudgeContestSettings
    ): Promise<AxiosResponse<DOMjudgeContest>> {
        return this.patch(
            Flask.url_for("api.domjudge.contest", { contest_id: contestId }),
            settings
        );
    }

    public async getContestUsers(
        contestId: number
    ): Promise<AxiosResponse<DOMjudgeContestUser[]>> {
        return this.get(
            Flask.url_for("api.domjudge.contest.users", {
                contest_id: contestId,
            })
        );
    }
}

export const domjudgeApi = new DOMjudgeApi();
