import { Api } from "./api";
import Flask from "../../utils/flask";

export class DeclarationOverviewResponse {
    id: number;
    created: string;
    committee: string;
    amount: number;
    reason: string;
}

class DeclarationApi extends Api {
    public async createDeclaration(data: FormData) {
        return this.post(Flask.url_for("api.declaration.send"), data);
    }

    public async getDeclarations() {
        return this.get<DeclarationOverviewResponse[]>(
            Flask.url_for("api.user.declarations", { user: "self" })
        );
    }
}

export const declarationApi = new DeclarationApi();
