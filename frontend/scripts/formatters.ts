export function date(
    value: string | Date,
    config: Intl.DateTimeFormatOptions = {
        year: "numeric",
        month: "long",
        day: "numeric",
    }
): string {
    let locale = "nl-NL";
    if (window.viaduct?.locale == "en") {
        locale = "en-GB";
    }
    return new Date(value).toLocaleDateString(locale, config);
}

export function datetime(value: string): string {
    let locale = "nl-NL";
    if (window.viaduct?.locale == "en") {
        locale = "en-GB";
    }
    const config: Intl.DateTimeFormatOptions = {
        year: "numeric",
        month: "short",
        day: "numeric",
        hour: "2-digit",
        minute: "2-digit",
    };
    return new Date(value).toLocaleString(locale, config);
}

export function euro(value: number): string {
    const formatter = new Intl.NumberFormat("nl-NL", {
        style: "currency",
        currency: "EUR",
        minimumFractionDigits: 2,
    });
    return formatter.format(value);
}

export function capitalize(value: string): string {
    //  This filter capitalizes the word. (i.e. 'via' becomes 'Via')
    if (value.length <= 1) return value;
    return value.charAt(0).toUpperCase() + value.slice(1);
}
