import { taskApi, TaskResponse } from "./api/task";

export async function poll({
    fn,
    validate,
    interval,
    maxAttempts,
}): Promise<TaskResponse> {
    let attempts = 0;

    const executePoll = async (resolve, reject) => {
        const result = await fn();
        attempts++;

        if (validate(result)) {
            return resolve(result);
        } else if (maxAttempts && attempts === maxAttempts) {
            return reject(new Error("Exceeded max attempts"));
        } else {
            setTimeout(executePoll, interval, resolve, reject);
        }
    };

    return new Promise(executePoll);
}

export async function checkPollTask(taskId: string): Promise<TaskResponse> {
    return await poll({
        fn: async () => await taskApi.getTask(taskId),
        validate: (result) => {
            return result.data.status === "SUCCESS";
        },
        interval: 500,
        maxAttempts: 10,
    });
}
