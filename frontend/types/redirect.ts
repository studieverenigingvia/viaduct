export class Redirect {
    public id?: string;
    public fro: string;
    public to: string;

    constructor(fro = "", to = "") {
        this.fro = fro;
        this.to = to;
    }
}
