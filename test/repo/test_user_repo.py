from datetime import timedelta

from sqlalchemy import func, select

from app.models.user import UserEducation
from app.repository import user_repository


def test_set_user_education_re_add_archived(admin_user, db_session, education_factory):
    stmt = select(func.count()).select_from(UserEducation)
    assert db_session.execute(stmt).scalar() == 0
    e1 = education_factory()

    # Set, remove and re-add study should result in two entries.
    user_repository.set_user_education(db_session, admin_user, [e1])
    user_repository.set_user_education(db_session, admin_user, [])
    user_repository.set_user_education(db_session, admin_user, [e1])

    educations = (
        db_session.execute(select(UserEducation).order_by(UserEducation.id))
        .scalars()
        .all()
    )
    assert len(educations) == 2
    for e in educations:
        assert e.education_id == e1.id
    assert educations[0].last_seen is not None
    assert educations[1].last_seen is None


def test_get_users_most_historic_educations(
    db_session, user_factory, education_factory
):
    user1 = user_factory()
    user2 = user_factory()

    education = education_factory()

    # Set user 1 first.
    user_repository.set_user_education(db_session, user1, [education])
    user_repository.set_user_education(db_session, user2, [education])
    db_session.commit()

    users = user_repository.get_users_with_educations_not_updated_for(
        db_session, 1, timedelta.resolution
    )
    assert len(users) == 1, users
    assert users[0].id == user1.id, users[0]

    # Re-set the user1's education, updating modified
    user_repository.set_user_education(db_session, user1, [education])
    db_session.commit()

    users = user_repository.get_users_with_educations_not_updated_for(
        db_session, 1, timedelta.resolution
    )
    assert len(users) == 1, users
    assert users[0].id == user2.id, users[0]

    users = user_repository.get_users_with_educations_not_updated_for(
        db_session, 25, timedelta(days=1)
    )
    assert not users, "all educations where updated today"
