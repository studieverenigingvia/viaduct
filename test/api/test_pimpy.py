from unittest import mock
from unittest.mock import Mock

from app.models.meeting import Meeting
from app.models.pimpy import Task


def test_minute(admin_client, admin_group):
    rv = admin_client.post(
        f"/api/groups/{admin_group.id}/minutes/",
        json={
            "date": "2020-09-01",
            "content": "content,",
            "group_id": admin_group.id,
        },
    )
    assert rv.status_code == 201, rv.data

    minute_id = rv.json["id"]
    rv = admin_client.get(f"/api/minutes/{minute_id}")
    assert rv.status_code == 200, rv.data

    rv = admin_client.get("/api/minutes")
    assert rv.status_code == 200, rv.data
    admin_group_minutes = rv.json[0]
    assert admin_group_minutes["group"]["id"] == admin_group.id
    assert admin_group_minutes["minutes"][0]["id"] == minute_id

    rv = admin_client.get(f"/api/groups/{admin_group.id}/minutes/")
    assert rv.status_code == 200, rv.data
    admin_group_minutes = rv.json[0]
    assert admin_group_minutes["group"]["id"] == admin_group.id
    assert admin_group_minutes["minutes"][0]["id"] == minute_id


@mock.patch("app.service.mail_service.send_mail_task.delay")
def test_minute_tasks_meeting_created(
    delay_mock: Mock, admin_client, admin_group, admin_user, member_user, db_session
):
    admin_user.first_name = "AdminFirstName"
    admin_user.last_name = "AdminLastName"

    member_user.first_name = "UserFirstName"
    member_user.last_name = "UserLastName"

    db_session.commit()

    rv = admin_client.post(
        f"/api/groups/{admin_group.id}/minutes/",
        json={
            "date": "2020-09-01",
            "content": f"ACTIE {admin_user.first_name}: Do something 1",
            "group_id": admin_group.id,
        },
    )
    assert rv.status_code == 201, rv.json

    delay_mock.assert_called_once()

    delay_mock.reset_mock()

    task = db_session.query(Task).one()

    rv = admin_client.post(
        f"/api/groups/{admin_group.id}/minutes/",
        json={
            "date": "2020-09-01",
            "content": f"""
ACTIE {admin_user.first_name}: Do something 2
DONE {task.b32_id}
ACTIES {admin_user.first_name}, {member_user.first_name}: Do something 3
MEETING 24-09-2020 1:00 test without length
MEETING 24-9-2020 15:00(9,9) test length as float with ,
MEETING 24-9-2020 21:01(9.9) test length as float with .
MEETING 25-07-2021 16:10(9) test length as int
            """,
            "group_id": admin_group.id,
        },
    )

    assert db_session.query(Task).count() == 4
    assert db_session.query(Meeting).count() == 4

    delay_mock.assert_called_once()


@mock.patch("app.service.mail_service.send_mail_task.delay")
def test_task(delay_mock: Mock, admin_client, admin_group, admin_user):
    rv = admin_client.post(
        "/api/tasks/",
        json={
            "title": "2020-09-01",
            "users": [admin_user.name],
            "group_id": admin_group.id,
            "status": "new",
        },
    )
    assert rv.status_code == 201, rv.data
    task_id = rv.json["b32_id"]

    rv = admin_client.get("/api/tasks/")
    assert rv.status_code == 200
    assert rv.json[0]["group"]["id"] == admin_group.id
    assert rv.json[0]["tasks"][0]["b32_id"] == task_id

    rv = admin_client.get("/api/users/self/tasks/")
    assert rv.status_code == 200
    assert rv.json[0]["group"]["id"] == admin_group.id
    assert rv.json[0]["tasks"][0]["b32_id"] == task_id

    rv = admin_client.get(f"/api/groups/{admin_group.id}/tasks")
    assert rv.status_code == 200
    assert rv.json[0]["group"]["id"] == admin_group.id
    assert rv.json[0]["tasks"][0]["b32_id"] == task_id

    delay_mock.assert_called_once()

    delay_mock.reset_mock()

    rv = admin_client.patch(
        f"/api/tasks/{task_id}/",
        json={
            "title": "new_title",
            "content": "content",
            "users": [admin_user.name],
            "status": "done",
        },
    )
    assert rv.status_code == 200

    delay_mock.assert_not_called()


@mock.patch("app.service.mail_service.send_mail_task.delay")
def test_autoremove_tasks_meeting_created(
    delay_mock: Mock, admin_client, admin_group, admin_user, member_user, db_session
):
    db_session.commit()

    # Add task with status new.
    rv = admin_client.post(
        "/api/tasks/",
        json={
            "title": "11",
            "users": [admin_user.name],
            "group_id": admin_group.id,
            "status": "new",
        },
    )
    assert rv.status_code == 201, rv.data

    # Add task with status done.
    rv = admin_client.post(
        "/api/tasks/",
        json={
            "title": "22",
            "users": [admin_user.name],
            "group_id": admin_group.id,
            "status": "done",
        },
    )
    assert rv.status_code == 201, rv.data

    # Add task with status not done
    rv = admin_client.post(
        "/api/tasks/",
        json={
            "title": "33",
            "users": [admin_user.name],
            "group_id": admin_group.id,
            "status": "remove",
        },
    )
    assert rv.status_code == 201, rv.data

    # Upload minutes without delete_finished_tasks
    rv = admin_client.post(
        f"/api/groups/{admin_group.id}/minutes/",
        json={
            "date": "2020-09-01",
            "content": "These are the minutes: there are no minutes.",
            "group_id": admin_group.id,
            "delete_finished_tasks": False,
        },
    )

    rv = admin_client.get("/api/tasks/")
    assert rv.status_code == 200
    assert len(rv.json[0]["tasks"]) == 3

    # Upload minutes with delete_finished_tasks
    rv = admin_client.post(
        f"/api/groups/{admin_group.id}/minutes/",
        json={
            "date": "2020-09-01",
            "content": "These are the minutes: there are no minutes once again",
            "group_id": admin_group.id,
            "delete_finished_tasks": True,
        },
    )

    rv = admin_client.get("/api/tasks/")
    assert rv.status_code == 200
    assert len(rv.json[0]["tasks"]) == 1


@mock.patch("app.service.mail_service.send_mail_task.delay")
def test_minute_tasks_meeting_created_with_keyword(
    delay_mock: Mock, admin_client, admin_group, admin_user, member_user, db_session
):
    admin_user.first_name = "AdminFirstName"
    admin_user.last_name = "AdminLastName"

    member_user.first_name = "UserFirstName"
    member_user.last_name = "UserLastName"

    db_session.commit()

    rv = admin_client.post(
        f"/api/groups/{admin_group.id}/minutes/",
        json={
            "date": "2020-09-01",
            "content": f"ACTIE {admin_user.first_name}: DOE JE ACTIEPUNTEN",
            "group_id": admin_group.id,
        },
    )
    assert rv.status_code == 201, rv.json

    delay_mock.assert_called_once()
