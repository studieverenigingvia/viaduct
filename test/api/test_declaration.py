from io import BytesIO
from unittest import mock

from conftest import CustomClient


@mock.patch("app.service.mail_service.send_mail_task.delay")
def test_declaration(mock, member_client: CustomClient):
    rv = member_client.post(
        "/api/declaration/send/",
        data={
            "reason": "reason",
            "committee": "Commissie",
            "amount": "10.50",
            "iban": "GB33BUKB20201555555555",
            "file 0": (BytesIO(b"file content"), "file 0.png"),
        },
    )
    assert rv.status_code == 204, rv.json
    assert mock.called_once

    rv = member_client.get("/api/users/self/declarations/")
    assert rv.status_code == 200, rv.json
