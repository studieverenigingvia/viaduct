from app.models.newsletter import Newsletter
from conftest import CustomClient


def test_get_put_newsletter(admin_client: CustomClient, newsletter: Newsletter) -> None:
    rv = admin_client.get(f"/api/newsletters/{newsletter.id}/")
    assert rv.status_code == 200, rv.json

    overwrite_text = {"overwrite_text": {"nl": "", "en": ""}}
    activity_item_id = rv.json["activities"][0]["obj"]["id"]
    news_item_id = rv.json["news"][0]["obj"]["id"]
    rv = admin_client.put(
        f"/api/newsletters/{newsletter.id}/",
        json={
            "activities": [
                {"item_id": activity_item_id, **overwrite_text},
            ],
            "news": [
                {"item_id": news_item_id, **overwrite_text},
            ],
            "greetings": [
                {"en": "Hi to Test!", "nl": "Groetjes aan Test!", "sender": "Test"}
            ],
        },
    )
    assert rv.status_code == 204, rv.json
