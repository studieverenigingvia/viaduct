import string

import pytest
from requests_mock import Mocker

from app.models.activity import Activity
from conftest import CustomClient


def test_activity_list(admin_client, activity):
    rv = admin_client.get("/api/activities")
    assert rv.status_code == 200
    assert rv.json["data"][0]["id"] == activity.id


def test_activity_get(admin_client, activity):
    rv = admin_client.get(f"/api/activities/{activity.id}")
    assert rv.status_code == 200
    assert rv.json["id"] == activity.id


@pytest.mark.usefixtures("activity")
def test_activity_search(admin_client: CustomClient):
    rv = admin_client.get(
        "/api/activities/",
        query_string={"page": 1, "limit": 10, "search": string.ascii_lowercase},
    )
    assert rv.status_code == 200
    assert len(rv.json["data"]) == 0


def test_activity_create(db, admin_client, admin_user, admin_group):
    rv = admin_client.post(
        "/api/activities/",
        json={
            "name": {"en": "asdf", "nl": "asdf"},
            "description": {"en": "adsf", "nl": "asdf"},
            "group_id": admin_group.id,
            "price": "1",
            "location": "Studievereniging VIA, Science Park 900, Amsterdam",
            "start_time": "2020-02-18T13:35:51.436Z",
            "end_time": "2020-02-18T13:35:51.436Z",
            "pretix_event_slug": "",
        },
    )
    assert rv.status_code == 200, rv.json
    activity = db.session.query(Activity).one_or_none()
    assert activity
    assert activity.name
    assert activity.owner_id == admin_user.id


def test_activity_create_too_long(db_session, admin_client, admin_group):
    rv = admin_client.post(
        "/api/activities/",
        json={
            "name": {"en": "a" * 257, "nl": "a" * 257},
            "description": {"en": "a" * 2049, "nl": "a" * 2049},
            "group_id": admin_group.id,
            "price": "1" * 257,
            "location": "L" * 1025,
            "start_time": "2020-02-18T13:35:51.436Z",
            "end_time": "2020-02-18T13:35:51.436Z",
            "pretix_event_slug": "s" * 65,
        },
    )
    assert rv.status_code == 400
    assert rv.json == {
        "code": 400,
        "data": {
            "details": "Schema validation failed.",
            "errors": {
                "location": ["Longer than maximum length 1024."],
                "name": ["Longer than maximum length 256."],
                "pretix_event_slug": ["Longer than maximum length 64."],
                "price": ["Longer than maximum length 256."],
            },
        },
        "detail": "The request was not according to semantic rules. Schema validation "
        "failed.",
        "title": "Validation Error",
        "type": "about:blank",
    }
    assert not db_session.query(Activity).first()


def test_list_pretix_events(
    admin_client: CustomClient, requests_mocker: Mocker
) -> None:
    requests_mocker.get(
        "https://pretix.svia.nl/api/v1/organizers/via/events/",
        json={
            # FIXME: actual response, these are currently used fields.
            "results": [
                {
                    "name": {"nl": "De Procam-pubquiz", "en": "De Procam-pubquiz"},
                    "slug": "20200604-procam",
                }
            ]
        },
    )
    rv = admin_client.get("/api/pretix/events/")
    assert rv.status_code == 200, rv.json
