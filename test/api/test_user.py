import datetime
import json
from datetime import date

from freezegun import freeze_time

from app.models.education import Education
from app.models.group import UserGroup, UserGroupHistory
from app.models.user import User
from app.repository import user_repository


def test_user_details_admin_id(user_factory, admin_client):
    user = user_factory()
    rv = admin_client.get(f"/api/users/{user.id}")
    assert rv.status_code == 200
    data = json.loads(rv.data)
    assert type(data["disabled"]) == bool


def test_user_details_user_id(member_user, member_client):
    rv = member_client.get(f"/api/users/{member_user.id}")
    assert rv.status_code == 403


def test_user_details_user_self(member_client):
    rv = member_client.get("/api/users/self")
    assert rv.status_code == 200


def test_user_educations_self(
    admin_client, db_session, education_factory, admin_user: User
):
    e1: Education = education_factory()
    # Make sure both education have same type, so they do not get filtered
    user_repository.set_user_education(
        db_session,
        admin_user,
        [
            e1,
            education_factory(programme_type=e1.programme_type.value),
        ],
    )
    db_session.commit()
    rv = admin_client.get("/api/users/self/educations/")
    assert rv.status_code == 200
    assert len(rv.json) == 2


def test_user_groups_admin_id(admin_client, member_user):
    rv = admin_client.get(f"/api/users/{member_user.id}/groups")
    assert rv.status_code == 200


def test_user_groups_user_id(member_client, member_user):
    rv = member_client.get(f"/api/users/{member_user.id}/groups")
    assert rv.status_code == 403


def test_user_groups_user_self(member_client):
    rv = member_client.get("/api/users/self/groups")
    assert rv.status_code == 200


def test_user_applications_admin_id(admin_client, member_user):
    rv = admin_client.get(f"/api/users/{member_user.id}/applications")
    assert rv.status_code == 200


def test_user_applications_user_id(member_client, member_user):
    rv = member_client.get(f"/api/users/{member_user.id}/applications")
    assert rv.status_code == 403


def test_user_applications_user_self(member_client):
    rv = member_client.get("/api/users/self/applications")
    assert rv.status_code == 200


def test_update_user_details_as_member(member_client, member_user, db):
    rv = member_client.patch(
        "/api/users/self/",
        json={
            "first_name": "Some_new_first_name",
            "last_name": "Some_new_last_name",
            "phone_nr": "06-somenumber",
        },
    )
    assert rv.status_code == 200

    u = db.session.get(User, member_user.id)
    assert u.first_name == "Some_new_first_name"
    assert u.last_name == "Some_new_last_name"
    assert u.phone_nr == "06-somenumber"

    rv = member_client.patch("/api/users/self/", json={"member": True})
    assert rv.status_code == 403
    rv = member_client.patch("/api/users/self/", json={"disabled": True})
    assert rv.status_code == 403
    rv = member_client.patch("/api/users/self/", json={"member_of_merit_date": None})
    assert rv.status_code == 403
    rv = member_client.patch("/api/users/self/", json={"favourer": True})
    assert rv.status_code == 403


def test_update_member_of_merit(db_session, admin_client, admin_user):
    assert admin_user.member_of_merit_date is None

    rv = admin_client.patch(
        "/api/users/self/", json={"member_of_merit_date": "2020-04-21"}
    )
    assert rv.status_code == 200, rv.json
    db_session.refresh(admin_user)
    assert admin_user.member_of_merit_date is not None

    # Update without member_of_merit, checks that it is not wiped.
    rv = admin_client.patch("/api/users/self/", json={})
    assert rv.status_code == 200, rv.json
    db_session.refresh(admin_user)
    assert admin_user.member_of_merit_date is not None

    rv = admin_client.patch("/api/users/self/", json={"member_of_merit_date": None})
    assert rv.status_code == 200, rv.json
    db_session.refresh(admin_user)
    assert admin_user.member_of_merit_date is None


def test_update_user_details_length_validation(member_client, member_user, db):
    rv = member_client.patch("/api/users/self/", json={"first_name": "a" * 255})
    assert rv.status_code == 200
    rv = member_client.patch("/api/users/self/", json={"first_name": "a" * 256})
    assert rv.status_code == 200
    rv = member_client.patch("/api/users/self/", json={"first_name": "a" * 257})
    assert rv.status_code == 400

    u = db.session.get(User, member_user.id)
    assert u.first_name == "a" * 256


def test_member_of_merit_list(db_session, anonymous_client, member_user: User):
    member_user.member_of_merit_date = date.today()
    db_session.commit()

    rv = anonymous_client.get("/api/users/merit/")
    assert rv.status_code == 200, rv.json
    assert len(rv.json) == 1, rv.json
    assert set(rv.json[0].keys()) == {
        "id",
        "first_name",
        "last_name",
        "name",
        "member_of_merit_date",
        "path",
    }, rv.json


def test_admin_make_user_member(db_session, admin_client, unpaid_user):
    rv = admin_client.patch(f"/api/users/{unpaid_user.id}/", json={"member": True})
    assert rv.status_code == 200, rv.json
    db_session.refresh(unpaid_user)
    assert unpaid_user.has_paid
    assert not unpaid_user.favourer


def test_admin_make_user_favourer(db_session, admin_client, unpaid_user):
    rv = admin_client.patch(f"/api/users/{unpaid_user.id}/", json={"favourer": True})
    assert rv.status_code == 200, rv.json
    db_session.refresh(unpaid_user)
    assert unpaid_user.favourer
    assert not unpaid_user.has_paid


def test_admin_make_user_favourer_and_member(db_session, admin_client, unpaid_user):
    rv = admin_client.patch(
        f"/api/users/{unpaid_user.id}/", json={"member": True, "favourer": True}
    )
    assert rv.status_code == 400, rv.json
    assert rv.json == {
        "code": 400,
        "data": {
            "details": "Schema validation failed.",
            "errors": {
                "favourer": "Member and favourer are mutually exclusive",
                "member": "Member and favourer are mutually exclusive",
            },
        },
        "detail": "The request was not according to semantic rules. Schema validation "
        "failed.",
        "title": "Validation Error",
        "type": "about:blank",
    }

    db_session.refresh(unpaid_user)
    assert not unpaid_user.has_paid
    assert not unpaid_user.favourer


def add_user_to_committee(db_session, admin_client, group_id, user_id, date):
    with freeze_time(date):
        rv = admin_client.post(
            f"/api/groups/{group_id}/users/", json={"user_ids": [user_id]}
        )
        assert rv.status_code == 204, rv.json

        user_group = db_session.query(UserGroup).filter_by(user_id=user_id).first()
        user_group.created = datetime.datetime.now()
        user_group.modified = datetime.datetime.now()

        db_session.commit()


def remove_user_from_committee(db_session, admin_client, group_id, user_id, date):
    with freeze_time(date):
        rv = admin_client.delete(
            f"/api/groups/{group_id}/users/", json={"user_ids": [user_id]}
        )
        assert rv.status_code == 204, rv.json

        user_group = (
            db_session.query(UserGroupHistory).filter_by(user_id=user_id).first()
        )
        user_group.deleted = datetime.datetime.now()


def test_active_user_currently_in_committee(
    db_session, admin_client, admin_user, admin_group, user_factory
):
    user = user_factory()

    add_user_to_committee(
        db_session, admin_client, admin_group.id, user.id, "2021-01-01"
    )

    rv = admin_client.get(f"/api/users/active/?groups={admin_group.id}")
    assert rv.status_code == 200, rv.json

    # Admin is ook lid van de groep
    assert len(rv.json) == 2, rv.json


def test_active_user_not_in_committee(
    db, admin_client, admin_user, admin_group, user_factory
):
    rv = admin_client.get(f"/api/users/active/?groups={admin_group.id}")
    assert rv.status_code == 200, rv.json
    assert len(rv.json) == 1, rv.json


def test_active_user_not_in_committee_anymore(
    db_session, admin_client, admin_user, admin_group, user_factory
):
    user = user_factory()

    add_user_to_committee(
        db_session, admin_client, admin_group.id, user.id, "2021-01-01"
    )
    remove_user_from_committee(
        db_session, admin_client, admin_group.id, user.id, "2021-02-01"
    )

    rv = admin_client.get(f"/api/users/active/?groups={admin_group.id}")
    assert rv.status_code == 200, rv.json
    # Only admin is in the group
    assert len(rv.json) == 1, rv.json


def test_active_user_empty_with_no_users(db, admin_client, admin_user, admin_group):
    millis_start = datetime.date(2021, 2, 1).strftime("%s")
    millis_end = datetime.date(2021, 2, 15).strftime("%s")

    rv = admin_client.get(
        f"/api/users/active/?groups={admin_group.id}&start_date={millis_start}&end_date={millis_end}"
    )
    assert rv.status_code == 200, rv.json
    # Admin only joined today (somewhere after 2021)
    assert len(rv.json) == 0, rv.json


def test_active_user_in_committee_crossing_time_span(
    db_session, admin_client, admin_user, admin_group, user_factory
):
    user = user_factory()

    add_user_to_committee(
        db_session, admin_client, admin_group.id, user.id, "2021-01-01"
    )
    remove_user_from_committee(
        db_session, admin_client, admin_group.id, user.id, "2021-03-01"
    )

    millis_start = datetime.date(2021, 2, 1).strftime("%s")
    millis_end = datetime.date(2021, 2, 15).strftime("%s")

    rv = admin_client.get(
        f"/api/users/active/?groups={admin_group.id}&start_date={millis_start}&end_date={millis_end}"
    )
    assert rv.status_code == 200, rv.json
    assert len(rv.json) == 1, rv.json


def test_active_user_in_committee_in_time_span(
    db_session, admin_client, admin_user, admin_group, user_factory
):
    user = user_factory()
    add_user_to_committee(
        db_session, admin_client, admin_group.id, user.id, "2021-02-05"
    )
    remove_user_from_committee(
        db_session, admin_client, admin_group.id, user.id, "2021-02-13"
    )

    millis_start = datetime.date(2021, 2, 1).strftime("%s")
    millis_end = datetime.date(2021, 2, 15).strftime("%s")

    rv = admin_client.get(
        f"/api/users/active/?groups={admin_group.id}&start_date={millis_start}&end_date={millis_end}"
    )
    assert rv.status_code == 200, rv.json
    assert len(rv.json) == 1, rv.json
