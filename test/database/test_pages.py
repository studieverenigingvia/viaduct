import pytest

from app.models.page import Page


def test_page_type_validation(db):
    with pytest.raises(ValueError):
        page = Page(path="path", type="someunknowntype")
        db.session.add(page)
        db.session.commit()
