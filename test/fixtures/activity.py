from pathlib import Path

import pytest
from sqlalchemy.orm import Session

from app.enums import FileCategory
from app.models.activity import Activity
from app.models.file import File


@pytest.fixture
def activity(db_session):
    activity = Activity()
    activity.nl_name = "nl_name"
    activity.en_name = "en_name"
    activity.nl_description = "nl_description"
    activity.en_description = "en_description"
    activity.start_time = "2019-03-23 23:33"
    activity.end_time = "2019-04-23 23:33"
    activity.location = "Studievereniging VIA"
    activity.price = 0
    activity.picture_file_id = None
    db_session.add(activity)
    db_session.commit()
    return activity


@pytest.fixture
def activity_picture_s3(db_session: Session, activity: Activity) -> File:
    f = File()
    key = Path(FileCategory.ACTIVITY_PICTURE.name.lower())
    f.hash = str(key / "uuid.jpg")
    f.extension = "jpg"
    f.category = FileCategory.ACTIVITY_PICTURE
    f.s3_bucket_id = "myfavbucket"

    db_session.add(f)
    db_session.flush()

    activity.picture_file_id = f.id
    db_session.commit()
    return f
