import unittest
from unittest.mock import MagicMock

from app.models.navigation import NavigationEntry
from app.models.page import Page
from app.service import navigation_service


def _create_mock_nav_entry(
    nav_id,
    parent_id=None,
    position=1,
    order=False,
    url=None,
    page_id=None,
    nl_title=None,
):
    m = MagicMock(spec=dir(NavigationEntry))
    m.id = nav_id
    m.page_id = page_id
    m.parent_id = parent_id
    m.nl_title = nl_title
    m.url = url
    m.position = position
    m.order_children_alphabetically = order

    return m


def _create_mock_nav_entry(
    nav_id,
    parent_id=None,
    position=1,
    order=False,
    url=None,
    page_id=None,
    nl_title=None,
):
    m = MagicMock(spec=dir(NavigationEntry))
    m.id = nav_id
    m.page_id = page_id
    m.parent_id = parent_id
    m.nl_title = nl_title
    m.url = url
    m.position = position
    m.order_children_alphabetically = order

    return m


class TestNavigationService(unittest.TestCase):
    def test_get_stripped_path(self):
        stripped_path1 = navigation_service.get_stripped_path("/bestuur12/", 0)
        self.assertEqual(stripped_path1, "bestuur12")

        stripped_path2 = navigation_service.get_stripped_path("/bestuur12", 0)
        self.assertEqual(stripped_path2, "bestuur")

        stripped_path3 = navigation_service.get_stripped_path("/activities/1337/", 0)
        self.assertEqual(stripped_path3, "activities/1337")

        stripped_path4 = navigation_service.get_stripped_path("/activities/1337", 0)
        self.assertEqual(stripped_path4, "activities")

        stripped_path5 = navigation_service.get_stripped_path("/user/avatar/1337/", 1)
        self.assertEqual(stripped_path5, "user/avatar")

        stripped_path6 = navigation_service.get_stripped_path("/user/avatar/1337", 1)
        self.assertEqual(stripped_path6, "user")

    def test_get_parent_by_searching_tree(self):
        # 1 (root)
        l1_entry = _create_mock_nav_entry(nav_id=1, parent_id=None)
        # 1 > 42
        l2_entry = _create_mock_nav_entry(nav_id=42, parent_id=1)
        # 1 > 42 > 96
        l3_entry = _create_mock_nav_entry(nav_id=96, parent_id=42)

        navigation_entries_mock = [l1_entry, l2_entry, l3_entry]

        l3_parent_id = navigation_service.get_parent_by_searching_tree(
            l3_entry, navigation_entries_mock
        ).id
        self.assertEqual(l3_parent_id, 42)

        l2_parent_id = navigation_service.get_parent_by_searching_tree(
            l2_entry, navigation_entries_mock
        ).id
        self.assertEqual(l2_parent_id, 1)

    def test_get_navigation_backtrack(self):
        member_of_merit_page = MagicMock(spec=dir(Page))
        member_of_merit_page.id = 1
        member_of_merit_page.path = "ereleden/Pietje+Puk"

        # Root > Ereleden (ereleden) > Pietje Puk (ereleden/Pietje+Puk; page_id=1)
        navigation_entries_mock = [
            _create_mock_nav_entry(
                nav_id=1,
                parent_id=None,
                url="ereleden",
                page_id=None,
                nl_title="Ereleden",
            ),
            _create_mock_nav_entry(
                nav_id=2,
                parent_id=1,
                url=None,
                page_id=member_of_merit_page.id,
                nl_title="Pietje Puk (NL)",
            ),
        ]
        pages_mock = [member_of_merit_page]

        backtrack_exists = navigation_service.get_navigation_backtrack(
            "ereleden/Pietje+Puk", entries=navigation_entries_mock, pages=pages_mock
        )
        self.assertEqual(
            [entry.nl_title for entry in backtrack_exists],
            ["Ereleden", "Pietje Puk (NL)"],
        )

        no_backtrack = navigation_service.get_navigation_backtrack(
            "ereleden/Fake+Page", entries=navigation_entries_mock, pages=pages_mock
        )
        self.assertEqual(no_backtrack, [])
