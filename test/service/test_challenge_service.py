from datetime import datetime, timedelta

import pytest
import pytz
from sqlalchemy.orm import Session

from app.exceptions.base import DuplicateResourceException
from app.models.challenge import Challenge, Competitor
from app.service import challenge_service


@pytest.fixture
def challenge(db_session):
    tz = pytz.timezone("Europe/Amsterdam")
    c = Challenge()
    c.name = "Challenge"
    c.description = "Description"
    c.hint = "Hint"
    c.start_date = datetime.now(tz=tz)
    c.end_date = datetime.now(tz=tz) + timedelta(days=1)
    c.weight = 10
    c.type = "Text"
    c.answer = "Answer"
    db_session.add(c)
    db_session.commit()
    return c


@pytest.fixture
def close_challenge(db_session: Session, challenge):
    challenge.start_date = datetime.now() - timedelta(days=2)
    challenge.end_date = datetime.now() - timedelta(days=1)
    db_session.commit()


@pytest.fixture
def change_challenge_type(db_session: Session, challenge):
    challenge.type = "Image"
    db_session.commit()


def test_create_challenge(db_session: Session):
    tz = pytz.timezone("Europe/Amsterdam")
    start = datetime.now(tz=tz)
    end = start + timedelta(days=1)
    c = challenge_service.create_challenge(
        "Name", "Description", "Hint", start, end, 10, "Text", "Answer"
    )

    result = db_session.query(Challenge).order_by(Challenge.id.desc()).first()

    assert result.name == c.name
    assert result.description == c.description
    assert result.hint == c.hint
    assert result.start_date == c.start_date
    assert result.end_date == c.end_date
    assert result.weight == c.weight
    assert result.type == c.type
    assert result.answer == c.answer


@pytest.mark.usefixtures("challenge")
def test_create_challenge_invalid_name(db_session: Session):
    tz = pytz.timezone("Europe/Amsterdam")
    start = datetime.now(tz=tz)
    end = start + timedelta(days=1)
    with pytest.raises(DuplicateResourceException):
        challenge_service.create_challenge(
            "Challenge", "Description", "Hint", start, end, 10, "Text", "Answer"
        )


@pytest.mark.usefixtures("challenge")
def test_is_open_challenge(db_session: Session):
    challenge = db_session.query(Challenge).first()

    assert challenge_service.is_open_challenge(challenge) is True


@pytest.mark.usefixtures("close_challenge")
def test_is_closed_challenge(db_session: Session):
    c = db_session.query(Challenge).first()

    assert challenge_service.is_open_challenge(c) is False


@pytest.mark.usefixtures("challenge")
def test_can_auto_validate(challenge):
    assert challenge_service.can_auto_validate(challenge) is True


@pytest.mark.usefixtures("change_challenge_type")
def test_cannot_auto_validate(challenge):
    assert challenge_service.can_auto_validate(challenge) is False


@pytest.mark.usefixtures("challenge")
def test_assign_points_to_user(db_session: Session, admin_user):
    challenge_service.assign_points_to_user(10, admin_user.id)

    result1 = (
        db_session.query(Competitor).filter(Competitor.user_id == admin_user.id).first()
    )

    assert result1.user_id == admin_user.id
    assert result1.points == 10
