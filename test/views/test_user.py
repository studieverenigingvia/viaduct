from collections import defaultdict
from datetime import datetime, timedelta, timezone
from unittest import mock
from unittest.mock import patch

import pytest
from flask_wtf.csrf import generate_csrf
from sqlalchemy import insert, inspect, select
from sqlalchemy.orm.dynamic import AppenderQuery

from app.models.mailinglist_model import MailingList
from app.models.request_ticket import PasswordTicket
from app.models.setting_model import Setting
from app.models.user import User, UserEducation
from app.repository import user_repository
from app.service import activity_service, pretix_service

pretix_service_mock = mock.MagicMock(pretix_service)
pretix_service_mock.get_user_orders.return_value = defaultdict(list)


@pytest.fixture
def default_mailinglist(db_session):
    m = MailingList()
    m.nl_name = "nl_name"
    m.en_name = "en_name"
    m.nl_description = "nl_description"
    m.en_description = "en_description"
    m.copernica_column_name = "copernica_column_name"
    m.members_only = True
    m.default = True
    db_session.add(m)
    db_session.commit()
    return m


def test_register_user(
    anonymous_client, education_factory, db_session, default_mailinglist
):
    education = education_factory()
    rv = anonymous_client.get("/sign-up/manual/")
    assert rv.status_code == 200

    rv = anonymous_client.post(
        "/sign-up/manual/",
        data={
            "first_name": "first_name",
            "last_name": "last_name",
            "birth_date": "1994-01-01",
            "address": "Street",
            "zip": "1111AA",
            "city": "Amsterdam",
            "country": "Nederland",
            "email": "maico.timmerman@gmail.com",
            "password": "test1234",
            "student_id": "123456789",
            "educations": [education.id],
            "agree_with_privacy_policy": True,
            "csrf_token": generate_csrf(),
        },
    )
    assert rv.status_code == 204
    user = db_session.query(User).one_or_none()

    assert user.mailinglists.all()
    q = select(UserEducation).where(UserEducation.user_id == user.id)
    educations = db_session.execute(q).scalars().all()
    assert len(educations) == 1
    assert educations[0].created - datetime.now(timezone.utc) < timedelta(seconds=5)
    assert educations[0].last_seen is None


@patch.object(activity_service, "pretix_service", pretix_service_mock)
@mock.patch("app.service.mail_service.send_mail_task.delay")
def test_edit_user(delay_mock, admin_client, db_session, admin_user, user_factory):
    user = user_factory()
    admin_client.login(admin_user)

    rv = admin_client.get(f"/users/{user.id}/edit/")
    assert rv.status_code == 200
    assert user.first_name != "first_name"

    rv = admin_client.post(
        f"/users/{user.id}/edit/",
        data={
            "first_name": "first_name",
            "last_name": "last_name",
            "birth_date": "1994-01-01",
            "address": "Street",
            "zip": "1111AA",
            "city": "Amsterdam",
            "country": "Nederland",
            "email": "maico.timmerman@gmail.com",
            "password": "test1234",
            "password_repeat": "test1234",
            "student_id": "123456789",
            "educations": [],
            "csrf_token": generate_csrf(),
        },
    )

    db_session.refresh(user)

    assert delay_mock.call_count == 2, "new and old e-mail should be notified"
    assert rv.status_code == 204
    assert user.first_name == "first_name"


@mock.patch("app.service.mail_service.send_mail_task.delay")
def test_edit_user_old_educations_perserved(
    delay_mock, admin_client, db_session, user_factory, education_factory
):
    user = user_factory(student_id_confirmed=False)

    e1 = education_factory()
    user_repository.set_user_education(db_session, user, [e1])
    db_session.commit()

    rv = admin_client.get(f"/users/{user.id}/edit/")
    assert rv.status_code == 200

    e2 = education_factory()
    rv = admin_client.post(
        f"/users/{user.id}/edit/",
        # Leave everything unchanged, except educations, change to newest.
        data={
            "first_name": user.first_name,
            "last_name": user.last_name,
            "birth_date": user.birth_date.isoformat(),
            "address": user.address,
            "zip": user.zip,
            "city": user.city,
            "country": user.country,
            "email": user.email,
            "password": "",
            "password_repeat": "",
            "student_id": user.student_id,
            "educations": [e2.id],
            "csrf_token": generate_csrf(),
        },
    )
    db_session.commit()
    assert rv.status_code == 204
    assert delay_mock.call_count == 0, "email has not change, should not mail"
    all_user_educations = db_session.execute(select(UserEducation)).scalars().all()
    assert len(all_user_educations) == 2, all_user_educations
    db_session.refresh(user)
    assert user.educations.all() == [
        e2
    ], "old education should not show in relationship"


def test_edit_user_existing_email(admin_client, admin_user, user_factory, db_session):
    user = user_factory()
    admin_client.login(admin_user)

    rv = admin_client.get(f"/users/{user.id}/edit/")
    assert rv.status_code == 200

    rv = admin_client.post(
        f"/users/{user.id}/edit/",
        data={
            "first_name": "first_name",
            "last_name": "last_name",
            "birth_date": "1994-01-01",
            "address": "Street",
            "zip": "1111AA",
            "city": "Amsterdam",
            "country": "Nederland",
            "email": admin_user.email,
            "password": "test1234",
            "password_repeat": "test1234",
            "student_id": "123456789",
            "educations": [],
            "csrf_token": generate_csrf(),
        },
    )
    assert rv.status_code == 409, rv.json
    assert rv.json == {"email": "A user with this e-mail address already exist."}
    db_session.refresh(user)
    db_session.refresh(admin_user)
    assert admin_user.email != user.email


@mock.patch("app.service.mail_service.send_mail_task.delay")
def test_password_reset(delay_mock, db, anonymous_client, user_factory, password):
    user = user_factory()

    rv = anonymous_client.get("/request_password/")
    assert rv.status_code == 200

    rv = anonymous_client.post(
        "/request_password/", data={"email": user.email, "csrf_token": generate_csrf()}
    )
    assert rv.status_code == 200
    assert delay_mock.call_count == 1

    ticket = db.session.query(PasswordTicket).one_or_none()
    assert ticket

    rv = anonymous_client.get(f"/reset_password/{ticket.hash}/")
    assert rv.status_code == 200

    rv = anonymous_client.post(
        f"/reset_password/{ticket.hash}/",
        data={
            "password": "new_password",
            "password_repeat": "new_password",
            "csrf_token": generate_csrf(),
        },
    )
    assert rv.status_code == 200
    assert b"Your password has been updated" in rv.data, "password not changed"

    # Login with the new user password, tests if password has changed.
    rv = anonymous_client.post(
        "/sign-in/",
        data={
            "email": user.email,
            # Raw password is only set in the pytest fixtures.
            "password": "new_password",
            "csrf_token": generate_csrf(),
        },
    )
    assert rv.status_code == 200


def test_set_confirmed_student_id(
    anonymous_client, db_session, admin_user, user_factory
):
    anonymous_client.login(admin_user)

    user1 = user_factory()
    user2 = user_factory()

    user1.student_id = "1234567890"
    user1.student_id_confirmed = False
    db_session.commit()

    rv = anonymous_client.get(f"/users/{user2.id}/student-id-linking/edit/")
    assert rv.status_code == 200

    rv = anonymous_client.post(
        f"/users/{user2.id}/student-id-linking/edit/",
        data={
            "csrf_token": generate_csrf(),
            "student_id": "1234567890",
            "student_id_confirmed": True,
        },
    )
    assert rv.status_code == 200
    db_session.refresh(user1)
    db_session.refresh(user2)
    assert not user1.student_id
    assert user2.student_id == "1234567890"
    assert user2.student_id_confirmed is True


def test_unset_student_id(anonymous_client, db_session, admin_user, user_factory):
    anonymous_client.login(admin_user)

    user = user_factory()
    user.student_id = "1234567890"
    user.student_id_confirmed = False
    db_session.commit()

    rv = anonymous_client.get(f"/users/{user.id}/student-id-linking/edit/")
    assert rv.status_code == 200

    rv = anonymous_client.post(
        f"/users/{user.id}/student-id-linking/edit/",
        data={
            "csrf_token": generate_csrf(),
            "student_id": "",
            "student_id_confirmed": True,
        },
    )
    assert rv.status_code == 200
    db_session.refresh(user)

    assert user.student_id == ""


@mock.patch("app.service.mail_service.send_mail_task.delay")
def test_anonymize(
    mock,
    anonymous_client,
    admin_user,
    member_user,
    db_session,
    db,
    app,
    requests_mocker,
):
    db_session.execute(
        insert(Setting).values([{"key": "COPERNICA_ENABLED", "value": str(True)}])
    )
    db_session.commit()

    requests_mocker.delete("https://api.copernica.com/profile/1/?access_token=")

    anonymous_client.login(admin_user)
    rv = anonymous_client.get(f"/users/{member_user.id}/anonymize/")
    assert rv.status_code == 200, rv.data

    rv = anonymous_client.post(
        f"/users/{member_user.id}/anonymize/", data={"csrf_token": generate_csrf()}
    )
    assert rv.status_code == 200
    assert requests_mocker.called_once

    db_session.refresh(member_user)

    assert mock.call_count == 1
    assert member_user.disabled
    assert member_user.first_name == "Anonymized"
    assert member_user.last_name == "User"
    assert "anonymized.svia.nl" in member_user.email

    inspector = inspect(db.engine)

    for attr, attr_value in inspect(member_user).attrs.items():
        # These are not going to be falsy, but explicitly tested.
        if attr in {
            "id",
            "created",
            "modified",
            "first_name",
            "last_name",
            "email",
            "disabled",
            "locale",
        }:
            continue

        if isinstance(attr_value.value, AppenderQuery):
            assert not attr_value.value.all(), f"{attr} is not falsy"
            continue

        assert not attr_value.value, f"{attr} is not falsy: {attr_value.value}"

    for name, table in db.metadata.tables.items():
        foreign_keys = inspector.get_foreign_keys(name)
        for fk_dict in foreign_keys:
            if fk_dict["referred_table"] == "user":
                assert fk_dict["referred_columns"][0] == "id"
                stmt = select(table).where(
                    table.c[fk_dict["constrained_columns"][0]] == member_user.id
                )
                assert not db_session.execute(stmt).one_or_none()


@pytest.mark.parametrize(
    "url",
    [
        "/users/self/tfa",
        "/users/self/edit",
        "/users/self/mailinglist-subscriptions",
        "/users/self/admin",
        "/users/self/applications",
        "/users/self/roles",
    ],
)
def test_user_vue_pages(url, anonymous_client, admin_user):
    anonymous_client.login(admin_user)

    rv = anonymous_client.get(url)
    assert rv.status_code == 200
