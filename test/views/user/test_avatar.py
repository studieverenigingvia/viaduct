import pytest
from werkzeug.datastructures import FileStorage

from app.service import user_service


@pytest.fixture
def avatar_file_storage():
    f = open("./app/static/img/user.png", "rb")
    fs = FileStorage(f, "avatar.png")
    yield fs
    f.close()


def test_random_user_without_avatar(anonymous_client, user_factory, db_session):
    user = user_factory()
    db_session.commit()

    rv = anonymous_client.get(f"/users/{user.id}/avatar/", follow_redirects=False)
    assert rv.status_code == 302
    assert rv.location.startswith("https://www.gravatar.com/avatar")


def test_avatar_based_on_email(anonymous_client, admin_user, member_user, db_session):
    admin_user.email = "john.doe@gmail.com"
    db_session.commit()

    rv = anonymous_client.get(f"/users/{admin_user.id}/avatar/", follow_redirects=False)

    assert rv.status_code == 302
    assert (
        rv.location == "https://www.gravatar.com/avatar/"
        "e13743a7f1db7f4246badd6fd6ff54ff?d=identicon&s=100"
    )


@pytest.mark.usefixtures("s3_client_with_bucket")
def test_anonymous_from_other_user(
    anonymous_client, news_factory, admin_user, avatar_file_storage, db_session
):
    db_session.commit()
    user_service.set_avatar(admin_user.id, avatar_file_storage)
    db_session.commit()

    with anonymous_client.get(
        f"/users/{admin_user.id}/avatar/", follow_redirects=False
    ) as rv:
        assert rv.status_code == 302
        assert "s3.eu-central-003.backblazeb2.com" in rv.location


def test_anonymous_self_avatar(anonymous_client):
    rv = anonymous_client.get("/users/self/avatar/", follow_redirects=False)
    assert rv.status_code == 302
    assert rv.location.startswith("https://www.gravatar.com/avatar")


@pytest.mark.usefixtures("s3_client_with_bucket")
def test_admin_self(anonymous_client, admin_user, avatar_file_storage, db_session):
    anonymous_client.login(admin_user)

    user_service.set_avatar(admin_user.id, avatar_file_storage)
    db_session.commit()

    with anonymous_client.get("/users/self/avatar/", follow_redirects=False) as rv:
        assert rv.status_code == 302
        assert "s3.eu-central-003.backblazeb2.com" in rv.location


@pytest.mark.usefixtures("s3_client_with_bucket")
def test_member_self(anonymous_client, member_user, avatar_file_storage, db_session):
    anonymous_client.login(member_user)

    user_service.set_avatar(member_user.id, avatar_file_storage)
    db_session.commit()

    with anonymous_client.get("/users/self/avatar/", follow_redirects=False) as rv:
        assert rv.status_code == 302
        assert "s3.eu-central-003.backblazeb2.com" in rv.location
