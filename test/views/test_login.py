import pyotp
from flask_wtf.csrf import generate_csrf


def test_login_tfa(admin_user, anonymous_client, password):
    assert admin_user.tfa_enabled

    # We need to do a GET before, as otherwise we cannot generate the csrf.
    rv = anonymous_client.get("/sign-in/")
    assert rv.status_code == 200

    rv = anonymous_client.post(
        "/sign-in/",
        data={
            "email": admin_user.email,
            # Raw password is only set in the pytest fixtures.
            "password": password["raw"],
            "csrf_token": generate_csrf(),
        },
    )
    assert rv.status_code == 200

    otp = pyotp.TOTP(admin_user.totp_secret).now()
    rv = anonymous_client.post(
        "/sign-in/otp/", data={"otp": otp, "csrf_token": generate_csrf()}
    )
    assert rv.status_code == 200

    # When login is successful, the log out button is visible.
    assert b"Log off" in rv.data
