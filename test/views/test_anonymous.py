import pytest

urls = ["/sitemap.xml", "/robots.txt"]


@pytest.mark.parametrize("url", urls)
def test_anonymous_pages(url, anonymous_client):
    rv = anonymous_client.get(url)
    assert rv.status_code == 200


def test_sitemap_with_pages(anonymous_client, page_revision):
    rv = anonymous_client.get("/sitemap.xml")
    data = rv.data.decode()
    assert rv.status_code == 200
    page, revision = page_revision
    assert page.path in data
