import io

import pytest
from werkzeug.datastructures import FileStorage

from app.enums import FileCategory
from app.service import file_service
from conftest import CustomClient

_FILE_CONTENT = b"filecontentbyes"


@pytest.fixture(params=[FileCategory.UPLOADS, FileCategory.UPLOADS_MEMBER])
def uploaded_file(request, db_session):
    fs = FileStorage(io.BytesIO(_FILE_CONTENT), "somelonguniquefilename.pdf")
    _file = file_service.add_file_to_s3(request.param, None, fs)
    return _file


@pytest.mark.usefixtures("s3_client_with_bucket")
def test_files_uploads(admin_client, admin_user, uploaded_file):
    admin_client.login(admin_user)

    rv = admin_client.get("/files/")
    assert rv.status_code == 200
    data = rv.data.decode()
    assert uploaded_file.full_display_name in data
    if uploaded_file.category == FileCategory.UPLOADS_MEMBER:
        assert "Members only" in data
    else:
        assert "Members only" not in data


@pytest.mark.usefixtures("s3_client_with_bucket")
def test_files_uploads_search(admin_client, admin_user, uploaded_file):
    admin_client.login(admin_user)

    rv = admin_client.get("/files/", query_string={"search": "someuniquefilename"})
    assert rv.status_code == 200
    data = rv.data.decode()
    assert uploaded_file.full_display_name in data
    if uploaded_file.category == FileCategory.UPLOADS_MEMBER:
        assert "Members only" in data
    else:
        assert "Members only" not in data


@pytest.mark.usefixtures("s3_client_with_bucket")
def test_files_uploads_search_not_found(admin_client, admin_user, uploaded_file):
    admin_client.login(admin_user)

    rv = admin_client.get("/files/", query_string={"search": "emanelifeuqinuemos"})
    assert rv.status_code == 200
    data = rv.data.decode()
    assert uploaded_file.full_display_name not in data


@pytest.mark.usefixtures("s3_client_with_bucket")
def test_files_content_anonymous(anonymous_client: CustomClient, uploaded_file):
    with anonymous_client.get(
        f"/files/content/{uploaded_file.id}/{uploaded_file.hash}/",
        follow_redirects=False,
    ) as rv:
        if uploaded_file.category == FileCategory.UPLOADS:
            assert rv.status_code == 302
            assert "s3.eu-central-003.backblazeb2.com" in rv.location
        elif uploaded_file.category == FileCategory.UPLOADS_MEMBER:
            assert rv.status_code == 302
            assert "/sign-in/" in rv.location
        else:
            raise AssertionError()


@pytest.mark.usefixtures("s3_client_with_bucket")
def test_files_content(admin_client, admin_user, uploaded_file):
    admin_client.login(admin_user)
    with admin_client.get(
        f"/files/content/{uploaded_file.id}/{uploaded_file.hash}/",
        follow_redirects=False,
    ) as rv:
        assert rv.status_code == 302
        assert "s3.eu-central-003.backblazeb2.com" in rv.location
        if uploaded_file.category == FileCategory.UPLOADS_MEMBER:
            assert "viaduct-private-files" in rv.location, rv.location
            assert "X-Amz-Signature" in rv.location, rv.location
