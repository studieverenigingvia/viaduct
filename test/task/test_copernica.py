import copy
import json
import logging
from collections.abc import Callable

import pytest
from _pytest.logging import LogCaptureFixture
from requests_mock import Mocker
from sqlalchemy.orm import Session

from app.exceptions.base import TaskFailure
from app.models.mailinglist_model import MailingList
from app.models.setting_model import Setting
from app.models.user import User
from app.task.copernica import (
    send_welcome_mail,
    synchronize_copernica_disabling_selection_profiles,
    validate_database_structure,
)

PRODUCT_DATABASE_FIELDS = [
    {
        "ID": "184",
        "name": "Emailadres",
        "type": "email",
        "value": "",
        "displayed": True,
        "ordered": False,
        "length": "50",
        "textlines": "0",
        "hidden": False,
        "index": True,
    },
    {
        "ID": "185",
        "name": "Voornaam",
        "type": "text",
        "value": "",
        "displayed": True,
        "ordered": False,
        "length": "50",
        "textlines": "0",
        "hidden": False,
        "index": True,
    },
    {
        "ID": "186",
        "name": "Achternaam",
        "type": "text",
        "value": "",
        "displayed": True,
        "ordered": False,
        "length": "50",
        "textlines": "0",
        "hidden": False,
        "index": False,
    },
    {
        "ID": "187",
        "name": "Studie",
        "type": "text",
        "value": "",
        "displayed": True,
        "ordered": False,
        "length": "50",
        "textlines": "0",
        "hidden": False,
        "index": True,
    },
    {
        "ID": "188",
        "name": "Studiejaar",
        "type": "text",
        "value": "",
        "displayed": False,
        "ordered": False,
        "length": "50",
        "textlines": "0",
        "hidden": False,
        "index": False,
    },
    {
        "ID": "189",
        "name": "Studienummer",
        "type": "text",
        "value": "",
        "displayed": True,
        "ordered": False,
        "length": "50",
        "textlines": "0",
        "hidden": False,
        "index": False,
    },
    {
        "ID": "190",
        "name": "Taal",
        "type": "select",
        "value": "\nnl\nen",
        "displayed": True,
        "ordered": False,
        "length": "50",
        "textlines": "1",
        "hidden": False,
        "index": False,
    },
    {
        "ID": "191",
        "name": "Ingeschreven",
        "type": "text",
        "value": "",
        "displayed": True,
        "ordered": False,
        "length": "50",
        "textlines": "0",
        "hidden": False,
        "index": True,
    },
    {
        "ID": "192",
        "name": "Commercieel",
        "type": "integer",
        "value": "1",
        "displayed": False,
        "ordered": False,
        "length": "50",
        "textlines": "0",
        "hidden": False,
        "index": True,
    },
    {
        "ID": "193",
        "name": "WebsiteID",
        "type": "integer",
        "value": "-1",
        "displayed": True,
        "ordered": False,
        "length": "50",
        "textlines": "0",
        "hidden": False,
        "index": True,
    },
    {
        "ID": "194",
        "name": "Bedrijfsinformatie",
        "type": "text",
        "value": "Nee",
        "displayed": True,
        "ordered": False,
        "length": "50",
        "textlines": "0",
        "hidden": False,
        "index": True,
    },
    {
        "ID": "195",
        "name": "Lid",
        "type": "select",
        "value": "Nee\nJa",
        "displayed": True,
        "ordered": False,
        "length": "50",
        "textlines": "0",
        "hidden": False,
        "index": True,
    },
    {
        "ID": "196",
        "name": "VVV",
        "type": "select",
        "value": "Nee\nJa",
        "displayed": True,
        "ordered": False,
        "length": "50",
        "textlines": "0",
        "hidden": False,
        "index": True,
    },
    {
        "ID": "200",
        "name": "Geboortedatum",
        "type": "empty_date",
        "value": "",
        "displayed": True,
        "ordered": False,
        "length": "50",
        "textlines": "1",
        "hidden": False,
        "index": True,
    },
    {
        "ID": "241",
        "name": "Alumnus",
        "type": "select",
        "value": "Nee\nJa\nJa_niet_mailen",
        "displayed": True,
        "ordered": False,
        "length": "50",
        "textlines": "1",
        "hidden": False,
        "index": False,
    },
    {
        "ID": "431",
        "name": "Test_Mailinglist",
        "type": "select",
        "value": "Nee\nJa",
        "displayed": True,
        "ordered": False,
        "length": "50",
        "textlines": "1",
        "hidden": False,
        "index": False,
    },
    {
        "ID": "434",
        "name": "LezingenNieuwsbrief",
        "type": "select",
        "value": "Nee\nJa",
        "displayed": True,
        "ordered": False,
        "length": "50",
        "textlines": "1",
        "hidden": False,
        "index": False,
    },
    {
        "ID": "540",
        "name": "Aankondigingen",
        "type": "select",
        "value": "Ja\nNee",
        "displayed": True,
        "ordered": False,
        "length": "50",
        "textlines": "1",
        "hidden": False,
        "index": True,
    },
]


@pytest.fixture
def copernica_mailinglist(db_session: Session) -> MailingList:
    mailinglist = MailingList(
        nl_name="Copernica nieuwsbrief",
        en_name="Copernica Newsletter",
        copernica_column_name="Ingeschreven",
        members_only=True,
    )

    db_session.add(mailinglist)
    db_session.commit()
    return mailinglist


@pytest.fixture
def copernica_mailinglist_select(db_session: Session) -> MailingList:
    mailinglist = MailingList(
        nl_name="Copernica nieuwsbrief",
        en_name="Copernica Newsletter",
        copernica_column_name="Aankondigingen",
        members_only=True,
    )

    db_session.add(mailinglist)
    db_session.commit()
    return mailinglist


@pytest.fixture
def copernica_subscribed_user(
    copernica_mailinglist: MailingList,
    db_session: Session,
    user_factory: Callable[[], User],
):
    user = user_factory()
    user.copernica_id = 1337
    user.mailinglists = [copernica_mailinglist]

    db_session.add(user)
    db_session.commit()
    return user


def test_disable_profiles(
    db_session, copernica_subscribed_user, requests_mocker, task_list
):
    db_session.add_all(
        [
            Setting(
                key="COPERNICA_DISABLING_SELECTIONS", value=json.dumps({"Bounces": 1})
            ),
            Setting(key="COPERNICA_ENABLED", value=str(True)),
        ]
    )
    db_session.commit()

    requests_mocker.get("https://api.copernica.com/v2/view/1/profileids", json=[1337])

    # Call the task-function directly, skipping worker communication.
    synchronize_copernica_disabling_selection_profiles()
    assert task_list[0]["sender"] == "app.task.copernica.update_user"
    assert task_list[0]["body"][0] == (copernica_subscribed_user.id,)
    assert len(task_list) == 1
    assert not copernica_subscribed_user.mailinglists.count()


def test_send_welcome_mail(db_session, copernica_subscribed_user, requests_mocker):
    db_session.add(
        Setting(key="COPERNICA_ENABLED", value=str(True)),
    )
    db_session.commit()
    requests_mocker.post(
        "https://api.copernica.com/v2/publisher/emailing", status_code=201
    )

    # Call the task-function directly, skipping worker communication
    send_welcome_mail(copernica_subscribed_user.id)

    assert {r.url for r in requests_mocker.request_history} == {
        "https://api.copernica.com/v2/publisher/emailing?access_token=",
    }


def test_validate_copernica_structure_correct(
    db_session: Session, requests_mocker: Mocker, copernica_mailinglist: MailingList
) -> None:
    db_session.add_all(
        [
            Setting(key="COPERNICA_ENABLED", value=str(True)),
            Setting(key="COPERNICA_DATABASE_ID", value=str(16)),
        ]
    )
    db_session.commit()

    requests_mocker.get(
        "https://api.copernica.com/database/16/fields?access_token=",
        json={
            "start": 0,
            "limit": 1,
            "count": 1,
            "data": [
                {
                    "ID": "191",
                    "name": "Ingeschreven",
                    "type": "text",
                    "value": "",
                    "displayed": True,
                    "ordered": False,
                    "length": "50",
                    "textlines": "0",
                    "hidden": False,
                    "index": True,
                },
            ],
            "total": 1,
        },
    )

    validate_database_structure()


def test_validate_copernica_structure_incorrect(
    db_session: Session,
    requests_mocker: Mocker,
    caplog: LogCaptureFixture,
    copernica_mailinglist: MailingList,
) -> None:
    db_session.add_all(
        [
            Setting(key="COPERNICA_ENABLED", value=str(True)),
            Setting(key="COPERNICA_DATABASE_ID", value=str(16)),
        ]
    )
    db_session.commit()

    data = copy.deepcopy(PRODUCT_DATABASE_FIELDS)
    assert data[7]["name"] == "Ingeschreven"
    data.pop(7)

    requests_mocker.get(
        "https://api.copernica.com/database/16/fields?access_token=",
        json={
            "start": 0,
            "limit": 1,
            "count": 1,
            "data": data,
            "total": 1,
        },
    )

    with pytest.raises(TaskFailure):
        with caplog.at_level(logging.ERROR):
            validate_database_structure()
    assert "The Copernica field 'Ingeschreven' does not exist" in caplog.text


def test_validate_copernica_select(
    db_session: Session,
    requests_mocker: Mocker,
    caplog: LogCaptureFixture,
    copernica_mailinglist_select: MailingList,
) -> None:
    db_session.add_all(
        [
            Setting(key="COPERNICA_ENABLED", value=str(True)),
            Setting(key="COPERNICA_DATABASE_ID", value=str(16)),
        ]
    )
    db_session.commit()

    requests_mocker.get(
        "https://api.copernica.com/database/16/fields?access_token=",
        json={
            "start": 0,
            "limit": 1,
            "count": 1,
            "data": PRODUCT_DATABASE_FIELDS,
            "total": 1,
        },
    )

    # Should not raise on the boolean based select field.
    validate_database_structure()
