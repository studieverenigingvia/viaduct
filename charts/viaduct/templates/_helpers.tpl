{{/*
Expand the name of the chart.
*/}}
{{- define "viaduct.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "viaduct.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "viaduct.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "viaduct.labels" -}}
helm.sh/chart: {{ include "viaduct.chart" . }}
{{ include "viaduct.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "viaduct.selectorLabels" -}}
app.kubernetes.io/name: {{ include "viaduct.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{- define "viaduct.lookupSecretKey" -}}
  {{- $namespace := index . 0 -}}
  {{- $secretName := index . 1 -}}
  {{- $key := index . 2 -}}
  {{- $secret := lookup "v1" "Secret" $namespace $secretName -}}
  {{- if $secret -}}
    {{- if hasKey $secret.data $key -}}
      {{- $secretValue := index $secret.data $key -}}
      {{ $secretValue | b64dec }}
    {{- else -}}
      {{- fail (printf "Secret '%s' does not have key '%s'" $secretName $key) -}}
    {{- end -}}
  {{- else -}}
    {{- fail (printf "Secret '%s' not found in namespace '%s'" $secretName $namespace) -}}
  {{- end -}}
{{- end -}}

{{- define "viaduct.databasePassword" -}}
  {{- if and .Values.postgresql.auth.existingSecret (not (empty .Values.postgresql.auth.existingSecret)) -}}
    {{ include "viaduct.lookupSecretKey" (list .Release.Namespace .Values.postgresql.auth.existingSecret "password") }}
  {{- else -}}
    {{ .Values.postgresql.auth.password }}
  {{- end -}}
{{- end -}}

{{- define "viaduct.databaseDsn" -}}
postgresql+psycopg2://{{ .Values.postgresql.auth.username }}:{{ (include "viaduct.databasePassword" .) }}@{{ (include "viaduct.fullname" .) }}-postgresql-hl/{{ .Values.postgresql.auth.database }}
{{- end -}}

{{- define "viaduct.brokerPassword" -}}
  {{- if and .Values.redis.auth.existingSecret (not (empty .Values.redis.auth.existingSecret)) -}}
    {{ include "viaduct.lookupSecretKey" (list .Release.Namespace .Values.redis.auth.existingSecret "redis-password") }}
  {{- else -}}
    {{ .Values.redis.auth.password }}
  {{- end -}}
{{- end -}}


{{- define "viaduct.brokerDsn" -}}
redis://:{{ (include "viaduct.brokerPassword" .) }}@{{ (include "viaduct.fullname" .) }}-redis-master
{{- end -}}

{{/*
Create the environment variables used by the Python app
*/}}
{{- define "viaduct.appEnvironment" }}
  - name: SQLALCHEMY_DATABASE_URI
    value: {{ include "viaduct.databaseDsn" . }}
  - name: POSTGRES_HOST
    value: {{ (include "viaduct.fullname" .) }}-postgresql-hl
  - name: POSTGRES_USER
    value: {{ .Values.postgresql.auth.username }}
  - name: POSTGRES_PASS
{{ if .Values.postgresql.auth.existingSecret }}
    valueFrom:
        secretKeyRef:
            name: {{ .Values.postgresql.auth.existingSecret }}
            key: password
{{ else }}
    value: {{ .Values.postgresql.auth.password }}
{{ end }}
  - name: POSTGRES_DB
    value: {{ .Values.postgresql.auth.database }}
  - name: BROKER_URL
    value: {{ include "viaduct.brokerDsn" . }}
  - name: S3_BUCKET
    value: {{ .Values.viaduct.s3.bucket  }}
  - name: S3_PRIVATE_BUCKET
    value: {{ .Values.viaduct.s3.private_bucket }}
  - name: AWS_ACCESS_KEY_ID
    value: {{ .Values.viaduct.s3.aws_access_key_id  }}
  - name: AWS_SECRET_ACCESS_KEY
{{ if .Values.viaduct.s3.aws_secret_access_key_secret_name }}
    valueFrom:
        secretKeyRef:
            name: {{ .Values.viaduct.s3.aws_secret_access_key_secret_name }}
            key: s3-access-key
{{ else }}
    value: {{ .Values.viaduct.s3.aws_secret_access_key  -}}
{{ end }}
  - name: AWS_PRIVATE_ACCESS_KEY_ID
    value: {{ .Values.viaduct.s3.aws_private_access_key_id }}
  - name: AWS_PRIVATE_SECRET_ACCESS_KEY
{{ if .Values.viaduct.s3.aws_private_secret_access_key_secret_name }}
    valueFrom:
        secretKeyRef:
            name: {{ .Values.viaduct.s3.aws_private_secret_access_key_secret_name }}
            key: s3-access-key
{{ else }}
    value: {{ .Values.viaduct.s3.aws_private_secret_access_key  }}
{{ end }}
  - name: MANTICORE_URL
    value: http://{{ include "viaduct.fullname" . }}-manticore:9308/json/search
{{ if .Values.viaduct.googleApiKeyExistingSecret }}
  - name: GOOGLE_API_KEY_FILE
    value: "/secrets/google/api-key.json"
{{ end }}
{{- end }}


{{/*
App volumes and volumeMounts for secret files (api keys).
*/}}
{{- define "viaduct.volumeMounts" -}}
{{- if .Values.viaduct.googleApiKeyExistingSecret }}
  - name: google-api-key-file
    mountPath: /secrets/google/
    readOnly: true
{{- end }}
{{- if .Values.viaduct.samlCertsExistingSecret }}
  - name: saml-certs
    mountPath: /app/saml/{{ .Values.viaduct.samlEnvironment }}/certs/
    readOnly: true
{{- end }}
{{- end }}

{{- define "viaduct.volumes" -}}
{{- if .Values.viaduct.googleApiKeyExistingSecret }}
  - name: google-api-key-file
    secret:
      secretName: {{ .Values.viaduct.googleApiKeyExistingSecret }}
      items:
        - key: google-api-key
          path: "api-key.json"
{{- end }}
{{- if .Values.viaduct.samlCertsExistingSecret }}
  - name: saml-certs
    secret:
      secretName: {{ .Values.viaduct.samlCertsExistingSecret }}
{{- end }}
{{- end }}

{{- define "viaduct.cookieAuthAnnotation" }}
{{- if .Values.ingress.cookie_auth_token_secret_name }}
{{- $secret := (lookup "v1" "Secret" .Release.Namespace .Values.ingress.cookie_auth_token_secret_name) }}
{{- if $secret -}}
{{- if hasKey $secret.data "token" -}}
nginx.ingress.kubernetes.io/configuration-snippet: |
    if ($cookie_auth !~ '{{- $secret.data.token | b64dec -}}') {
         return 401;
    }
{{- else }}
{{- fail (printf "Secret %s does not have key 'password'" .Values.ingress.cookie_auth_token_secret_name) -}}
{{- end -}}
{{- else -}}
{{- fail (printf "Secret %s not found in namespace %s" .Values.ingress.cookie_auth_token_secret_name .Release.Namespace) -}}
{{- end }}
{{- end }}
{{- end }}
