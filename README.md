# Viaduct

Viaduct is the codebase for the website of [study association **via**](https://svia.nl).


## Setup with GitPod

We support [GitPod](https://gitpod.io/), to quickly get started without setting up a development environment on a local machine. GitPod works with VSCode in the browser.

- Log in to GitPod with your GitLab account, linking GitPod and GitLab together.
- Open the viaduct project in GitPod.
- Wait for GitPod to finish installing the dependencies.

Changes to the codebase will automatically reload. To run tests, type `pytest`. And use `pre-commit` to check your code for style issues.

## Setup with Docker (recommended)
**Note: if you are setting up the environment for the first time, please update any errors in the documentation immediately**


### Install Docker and Docker Compose
Docker and Docker Compose are both prerequisites for running Viaduct.

Start by installing [Docker Desktop](https://docs.docker.com/desktop/) (for Mac and Windows) or
[Docker Engine](https://docs.docker.com/engine/) for Linux.

Further setup is done using the commands in the `Makefile`, run `make help` to see what
commands are available!


### Run the viaduct setup

Run:
```bash
make setup
```

Now open <http://localhost:5000> in your favourite webbrowser!

### Stop

```bash
make stop
```

If you use `docker-compose` (legacy) instead of `docker compose`, you can override the CLI syntax via the `DC_BINARY` variable. Example:
```
DC_BINARY=docker-compose make run
```

## (optional) Start background worker

The site uses a worker for background tasks, in order to start it you need to start the broker redis and a broker:

```bash
make worker
```

## Running pytest

We use [pytest](https://docs.pytest.org/en/latest/) for running our test-suite. It requires a database to create run the integration tests.
You are able to use your development postgresql connection for testing, however that will wipe all data after the test run.
A better solution is to use a specific database for the tests. To create this database in your development postgresql instance, follow the following command:

```bash
make create-test-db
```

You can now use the `docker-pytest` task from within PyCharm. If you manually want to run the test suite, you can execute pytest within the backend container using the following command:
```bash
make tests
```

### vii) Run viaduct from PyCharm (bonus)
*These instructions should translate to Intelij Ultimate*

It is possible to easily add the Docker Python interpreter directly to PyCharm, which allows you to
i) use PyCharm to check and autocomplete your imports, and ii) run Viaduct in the PyCharm debugger, so you can insert breakpoints on the fly.

#### Add the Docker Python interpreter.

1) Open the PyCharm settings (ctrl+alt+s or cmd+,) and navigate to the `project interpreter` settings.
2) Add a new interpreter by pressing gear icon and then the `add` as shown below ![alt text](docs/readme/pycharm-add-interpreter.png)
3) Click on the `Docker Compose` tab and add an interpreter with the following settings. If no Server exists, then add a Docker server with the default settings. ![alt text](docs/readme/pycharm-python-interpreter.png "Logo Title Text 1")
4) After pressing `OK`, a list with all packages viaduct pip should show. Press `Apply` or `OK` to save the settings.

#### Create a run configuration
*A run configuration called `docker-viaduct-backend` should already exist. Follow these steps if that is not the case.*

1) Open the run configuration (shift+alt+f10, then 0).
2) Open the template for python on the left and create a configuration with the following options:
    1) Module Name: `flask`
    2) Parameters: `run --host=0.0.0.0`
    3) Environment Variables: `PYTHONUNBUFFERED=1;FLASK_ENV=development`
    4) Python Interpreter: Use the one you created in the previous step.
3) Save the configuration by pressing `OK` or `Apply`.
4) After running this once, a run configuration should be available in the top right corner of PyCharm.

#### Run / Debug Viaduct

In the top right of PyCharm, the following should be visible (`docker-viaduct-backend` could also be called `run`).
To start running Viaduct press the green triangle (or shift+alt+f10), for debugging press the bug icon.

![alt text](docs/readme/pycharm-run-debug.png)

To learn more about how to optimize your development flow, read about the PyCharm debugger [here](https://www.jetbrains.com/help/pycharm/debugging-your-first-python-application.html).


## Setup without Docker
For Linux (Ubuntu LTS) and macOS.

**Note:** in the following examples, we use *user* as a default username, and *pass* as a default password. Change these as you wish.

### Setup
First, first make sure your SSH keys are set up in gitlab.com correctly. You can set them up in your [profile](https://gitlab.com/-/profile/keys).

For Linux, the directory `.local/bin` must be in your PATH. Edit the file `~/.bashrc` and add the following line to the end: `export PATH="/home/<YOUR_USER>/.local/bin:$PATH"`. After adding it to .bashrc, open a new shell to apply the new path.

#### Dependencies
For Linux systems, the following dependencies are sufficient:

`sudo apt update && sudo apt install git redis-server python3 python3-dev python3-pip redis-server build-essential pkg-config libxml2-dev libxmlsec1-dev libxmlsec1-openssl postgresql libpq-dev libjpeg-dev`

For macOS, install the dependencies with [Brew](https://brew.sh). `brew install redis python libxml2 libxmlsec1 libpq`.

#### PostgreSQL
For Linux, postgres was installed in the previous step. Start postgres with `sudo systemctl start postgresql`.

For macOS, we recommend installing [Postgress.app](https://postgresapp.com/). Install and start the postgres server. Follow the instructions on the install page of Postgress.app to add the command line tools to your PATH.

#### Redis
For Linux, start Redis with `sudo systemctl start redis-server`.

For macOS, start Redis with `brew services start redis`.

#### NodeJS
To install NodeJS for Linux, we recommend the latest LTS version of Node. The latest LTS version is listed on the [homepage of NodeJS](https://nodejs.org/en/). To install, follow the instructions [here](https://github.com/nodesource/distributions/blob/master/README.md). Make sure to install the LTS version.

For macOS, `brew install node` should be sufficient.

#### Poetry
For Linux and macOS, run `curl -sSL https://install.python-poetry.org | python3 -`.

#### Setting up viaduct

Clone the repo and enter its directory: `git clone git@gitlab.com:studieverenigingvia/viaduct.git && cd viaduct`.

##### Setting up the database

Create the postgres database role, and enter a database password (replace user as you wish, and enter a password):

Linux:
```
sudo su postgres -c "createuser --superuser --password <user>"
```

macOS:
```
createuser --superuser --password <user>
```

Create the databases for normal development and testing.

Linux:
```
sudo su postgres -c "createdb --owner <user> viaduct"
sudo su postgres -c "createdb --owner <user> viaduct_test"
psql --username <user> --dbname viaduct -c "create extension if not exists citext"
psql --username <user> --dbname viaduct_test -c "create extension if not exists citext"
```

macOS
```
createdb --owner <user> viaduct
createdb --owner <user> viaduct_test
psql --username <user> --dbname viaduct -c "create extension if not exists citext"
psql --username <user> --dbname viaduct_test -c "create extension if not exists citext"
```

##### Setting up poetry and the shell
Run `poetry install` to install all Python dependencies and `npm install` to install al node dependencies.

To activate all dependencies, and run commands like `flask`, you have to activate the Poetry shell and have the correct environment variables set. To do so, run `poetry shell`, and enter the following exports (replace user and pass to your own):
```
export FLASK_ENV=development
export BROKER_URL=redis://localhost
export BROKER_TEST_URL=redis://localhost
export SQLALCHEMY_DATABASE_URI="postgresql+psycopg2://<user>:<pass>@localhost/viaduct"
export SQLALCHEMY_DATABASE_TEST_URI="postgresql+psycopg2://<user>:<pass>@localhost/viaduct_test"
```

Next, set up a default viaduct user account.

```flask createdb```

and enter your details.

#### Running

The backend can now be run by executing `flask run`.

The frontend must also be compiled. It is useful to *watch* the changes to the frontend code to immediately compile them. Do this with `npm run dev`.

To run the worker, execute `celery --app worker:worker worker -l debug --beat`. The worker is only needed for specific tasks, such as sending emails and running long-running tasks. You often don't need it while developing.

#### Running tests
First, activate the poetry shell and set all environment variables correctly. Then, run `pytest` to run all tests.

## Testing code style and committing

We use [Pre-commit](https://pre-commit.com/) for running hooks upon commit. Install it using
the described methods in documentation. Afterwards you need to initialize it using:

```bash
pre-commit install
```

Upon commit, all the hooks will run. Some hooks are configured to automatically
fix mistakes you have made, some will require manual intervention.
A manual check without making a commit can be done by running:

```bash
pre-commit run --all-files
```

## Best practices

### Changes in the database

To make changing the database easy you can use the models to update the actual
database. There are two times you want to do this. The first one is just for
testing your changes locally.
If this is the case use these commands to upgrade your actual database.

This will create a new migration script:

```bash
make shell
flask db migrate --message 'revision message'
```

After this script is done you can view it to check if nothing weird is
going to happen when you execute it. After that, just run the server with
docker-compose and it will automatically execute the migration, or run it
manually:

Using docker:
```bash
make migrate
```

Or manually:
```bash
flask db upgrade
```

If this causes errors, something is wrong. Quite possibly the state of the
database, if you can't fix it yourself ask for help.  If not, you now have an up-to-date database.

#### Pushing migrations to develop
When pushing a new migration to develop, two scenarios can occur:

1. No new migrations were pushed to master in the meantime. This means that your
    migration is correctly the new head of database changes. You can verify this
    by running `flask db history` and check if your change is at the top, and
    the succeeding versions follow another correctly.

2. Other changes were made to the database in the meantime. This means that the
    current history is incorrect and needs to be fixed. See the section below to
    find out how.


#### Fixing migration history

Every migration has its unique `revision` hash, which is noted at the beginning
of the migration file. Note that a `down_revision` hash is also provided here.
We use `Alembic` to figure out the correct order of revisions. The current order
can be shown via `flask db history`. The current head revision is noted at the
top. In order to append your new migration to the history, change the
`down_revision` hash of your migration file to the hash of the current head.
Then check if all versions follow another correctly in the history of
`flask db history`.

*If you have any questions when merging, do ask for help!*


### Language

tl;dr For compiling the strings we have a PyCharm task that will run these
commands in sequence Just run `docker-translations` in PyCharm them.

All code shall be written in **English**, translations should be added through
Babel. After writing code with **English** strings, add their **Dutch**
translations.

Creating strings in python is done using the `(lazy_)gettext` functions.

```python
from flask_babel import _  # in views
from flask_babel import lazy_gettext as _   # in models and forms
```

For updating translation files after creating new pages, first extract the new
translatable strings from the code. Then merge the new extractions with the
existing translations:

```bash
flask translations
```

Edit the file `app/translations/nl/LC_MESSAGES/message.po` and add the Dutch
translations for the English strings. Especially look for lines marked "fuzzy",
since they won't be compiled. If the translation is correct, remove the line
marking "fuzzy" and continue.

After that compile the strings to be used in the website using the same command:

```bash
flask translations
```


### Documentation

Documentation according to Python's [Docstring Conventions]
(http://www.python.org/dev/peps/pep-0257/).


## Troubleshooting:
```
IOError: [Errno 13] Permission denied: '/home/username/.pip/pip.log'
```
This can also happen for other files. It often happens when files are created within a running Docker container. To fix, execute the following: `sudo chown $USER:$USER /home/username/.pip/pip.log`.

```
ERROR: Version in "./docker-compose.yml" is unsupported. You might be seeing
this error because you're using the wrong Compose file version. Either specify
a version of "2" (or "2.0") and place your service definitions under the `services`
key, or omit the `version` key and place your service definitions at the root of
the file to use version 1. For more on the Compose file format versions,
see https://docs.docker.com/compose/compose-file/
```

Your docker-compose version is not up-to-date, please update it to a newer version
following the instructions [here](https://docs.docker.com/compose/install/). Make
sure to remove your old version first.


## Deployment to kubernetes

The deployment to kubernetes is done using the `charts` folder. This folder contains all the necessary files to deploy the website to a kubernetes cluster.

Manticore is a bit weird, and requires a command to be run to update all the indexes. This command is run in the `manticore` container. To do this, run the following command:

```bash
su manticore
indexer --all --rotate
```

Then you have to restart the manticore container to apply the changes.
