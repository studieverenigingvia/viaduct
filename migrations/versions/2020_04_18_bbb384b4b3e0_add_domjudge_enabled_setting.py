"""Add DOMJUDGE_ENABLED setting.

Revision ID: bbb384b4b3e0
Revises: f1a08a6e2883
Create Date: 2020-04-18 13:58:36.489915

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

from sqlalchemy.orm import relationship, declarative_base

# revision identifiers, used by Alembic.
revision = "bbb384b4b3e0"
down_revision = "f1a08a6e2883"

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


settings_table = db.table(
    "setting",
    db.Column("key", db.String(128), unique=True, nullable=False),
    db.Column("value", db.String(256)),
)


def upgrade():
    create_session()

    setting = {"key": "DOMJUDGE_ENABLED", "value": "False"}
    op.bulk_insert(settings_table, [setting])


def downgrade():
    create_session()


# vim: ft=python
