"""Added sitemap boolean to Page

Revision ID: b38e89041d45
Revises: 948064b7afd3
Create Date: 2022-10-08 16:56:17.465288

"""
import sqlalchemy as sa
from alembic import op
from sqlalchemy.orm import relationship, declarative_base

# revision identifiers, used by Alembic.
revision = "b38e89041d45"
down_revision = "948064b7afd3"

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


def upgrade():
    create_session()

    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column(
        "page",
        sa.Column(
            "hide_in_sitemap",
            sa.Boolean(),
            nullable=True,
        ),
    )
    op.execute("UPDATE page SET hide_in_sitemap = false")
    op.alter_column("page", "hide_in_sitemap", nullable=False)
    # ### end Alembic commands ###


def downgrade():
    create_session()

    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column("page", "hide_in_sitemap")
    # ### end Alembic commands ###


# vim: ft=python
