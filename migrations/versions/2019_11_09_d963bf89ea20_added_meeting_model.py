"""Added meeting model.

Revision ID: d963bf89ea20
Revises: fd12cd62f2fa
Create Date: 2019-11-09 10:39:28.426482

"""
from alembic import op
import sqlalchemy as sa


from sqlalchemy.orm import relationship, declarative_base

# revision identifiers, used by Alembic.
revision = "d963bf89ea20"
down_revision = "fd12cd62f2fa"

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


def upgrade():
    create_session()

    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        "meeting",
        sa.Column("id", sa.Integer(), nullable=False),
        sa.Column("created", sa.DateTime(timezone=True), nullable=True),
        sa.Column("modified", sa.DateTime(timezone=True), nullable=True),
        sa.Column("group_id", sa.Integer(), nullable=True),
        sa.Column("name", sa.String(length=512), nullable=True),
        sa.Column("start_time", sa.DateTime(timezone=True), nullable=True),
        sa.Column("end_time", sa.DateTime(timezone=True), nullable=True),
        sa.Column("location", sa.String(length=64), nullable=True),
        sa.Column("description", sa.String(length=2048), nullable=True),
        sa.Column("cancelled", sa.Boolean(), nullable=True),
        sa.Column("sequence", sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(
            ["group_id"], ["group.id"], name=op.f("fk_meeting_group_id_group")
        ),
        sa.PrimaryKeyConstraint("id", name=op.f("pk_meeting")),
        sqlite_autoincrement=True,
    )
    # ### end Alembic commands ###


def downgrade():
    create_session()

    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table("meeting")
    # ### end Alembic commands ###


# vim: ft=python
