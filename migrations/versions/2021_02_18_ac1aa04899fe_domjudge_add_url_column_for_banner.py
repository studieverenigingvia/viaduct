"""DOMjudge: add URL column for banner.

Revision ID: ac1aa04899fe
Revises: 60b1c539436b
Create Date: 2021-02-18 15:26:28.796391

"""
from alembic import op
import sqlalchemy as sa


from sqlalchemy.orm import relationship, declarative_base

# revision identifiers, used by Alembic.
revision = "ac1aa04899fe"
down_revision = "60b1c539436b"

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


def upgrade():
    create_session()

    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column(
        "domjudge_contest_settings",
        sa.Column("banner_url", sa.String(length=512), nullable=True),
    )
    # ### end Alembic commands ###


def downgrade():
    create_session()

    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column("domjudge_contest_settings", "banner_url")
    # ### end Alembic commands ###


# vim: ft=python
