"""empty message.

Revision ID: 4db52787ccf6
Revises: ('3979d3b7fed2', '0c85618e4654')
Create Date: 2019-03-27 15:48:55.776560

"""
from alembic import op
import sqlalchemy as sa


from sqlalchemy.orm import relationship, declarative_base

# revision identifiers, used by Alembic.
revision = "4db52787ccf6"
down_revision = ("3979d3b7fed2", "0c85618e4654")

Base = declarative_base()
db = sa
db.Model = Base
db.relationship = relationship


def create_session():
    connection = op.get_bind()
    session_maker = sa.orm.sessionmaker()
    session = session_maker(bind=connection)
    db.session = session


def upgrade():
    create_session()


def downgrade():
    create_session()


# vim: ft=python
