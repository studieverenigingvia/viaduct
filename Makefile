# Self-Documented Makefile see https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html

.DEFAULT_GOAL := help

# Example to override: $ DC_BINARY=docker-compose make run
DC_BINARY ?= docker compose

.PHONY: help
help:
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.PHONY: run
run: ## Run Viaduct
	$(DC_BINARY) up db backend frontend adminer

.PHONY: run-silent
run-silent: ## Run Viaduct in verbose mode
	$(DC_BINARY) up -d db backend frontend

.PHONY: shell
shell: run-silent ## Open a shell in the backend container
	$(DC_BINARY) exec backend bash

.PHONY: create-db
create-db:
	$(DC_BINARY) up -d db adminer
	$(DC_BINARY) run --rm backend flask createdb

.PHONY: db-shell
db-shell: ## Open a shell in the db container
	$(DC_BINARY) exec db psql -U viaduct

.PHONY: translations
translations: run-silent  ## Extract translations
	$(DC_BINARY) exec backend flask translations

.PHONY: create-test-db
create-test-db:
	$(DC_BINARY) up -d db
	$(DC_BINARY) exec db bash -c "echo \"SELECT 'CREATE DATABASE viaduct_test' WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = 'viaduct_test')\gexec\" | psql -U viaduct"
	$(DC_BINARY) exec db bash -c "echo \"create extension if not exists citext\" | psql -U viaduct -d viaduct_test"

.PHONY: tests
tests: backend-tests frontend-tests ## Run all tests

.PHONY: backend-tests
backend-tests: ## Run backend unittests
	$(DC_BINARY) run --rm backend env -u S3_ENDPOINT -u USE_HTTPS -u FLASK_DEBUG pytest

.PHONY: frontend-tests
frontend-tests: ## Run frontend unittests
	$(DC_BINARY) run --rm frontend npm run test

.PHONY: migrate
migrate: ## Execute migrations on the database
	$(DC_BINARY) run --rm backend flask db upgrade

.PHONY: worker
worker: ## Start a worker container
	$(DC_BINARY) up -d redis worker

.PHONY: npm-install
npm-install:
	$(DC_BINARY) run --rm frontend npm ci

.PHONY: setup ## Setup viaduct for dev
setup: create-db create-test-db npm-install run
	$(info Open http://localhost:5000 in you favourite webbrowser!)

--update:
	$(DC_BINARY) build --pull

.PHONY: update
update: --update npm-install migrate

.PHONY: stop
stop:  ## Stop viaduct
	$(DC_BINARY) stop

.PHONY: destroy
destroy: ## Destroy viaduct
	$(DC_BINARY) down --volumes --remove-orphans
