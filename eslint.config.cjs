const eslint = require('@eslint/js');
const typescriptEslint = require('typescript-eslint');
const eslintPluginVue = require('eslint-plugin-vue');
const globals = require('globals');
const prettier = require('eslint-config-prettier');


module.exports  =  typescriptEslint.config(
  { ignores: ['*.d.ts', '**/coverage', '**/dist', '**/js_bridge.js'] },
  {
    extends: [
      eslint.configs.recommended,
      ...typescriptEslint.configs.recommended,
      ...eslintPluginVue.configs['flat/recommended'],
        prettier
    ],
    files: ['**/*.{ts,vue}'],
    languageOptions: {
      ecmaVersion: 'latest',
      sourceType: 'module',
      globals: globals.browser,
      parserOptions: {
        parser: typescriptEslint.parser,
      },
    },
    rules: {
        "vue/html-indent": ["error", 4],
        // Directly contradicts prettier, so we disable it.
        "vue/singleline-html-element-content-newline": "off",
        // This should be fixed, but too much manual work right now.
        "@typescript-eslint/no-unused-vars": "warn",
        // These are currently warnings, however I'd like to enable them.
        "@typescript-eslint/no-explicit-any": "warn",
        "vue/multi-word-component-names": "off",
        // We use mutations in the ALV components, it is easier to use them that way.
        "vue/no-mutating-props": ["off"]
    },
  },
);
